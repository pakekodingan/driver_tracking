jQuery.fn.extend({
    loading: function (param) {
        if(param == 'start'){
            // var loader = '&nbsp;&nbsp;&nbsp;&nbsp; <i class="fa fa-spinner fa-spin"></i> &nbsp;&nbsp;&nbsp;&nbsp;';
            var loader = '<i class="fa fa-spinner fa-spin"></i> Processing...';
            $(this).html(loader);
            $(this).attr("disabled",true);
        }else{
            var stop = '<i class="fa fa-save"></i> Save';
            $(this).html(stop)
            $(this).attr("disabled",false);    
        }
    },

});