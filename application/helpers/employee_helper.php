<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    
    function create_username($employee_name,$birthday) #format birthday Y-m-d
    {
        $CI =& get_instance();

        $arr_employee_name  = explode(" ", $employee_name);
        $spasi          = count($arr_employee_name);

        $arr_birthday = explode("-", $birthday);
        $year         = $arr_birthday[0];
        $month        = $arr_birthday[1];
        $day          = $arr_birthday[2];

        $arr_data[0] = $arr_employee_name[0].$day.$month;

        if($spasi >= 2)
        {
            $arr_data[1] = $arr_employee_name[1].$day.$month;
        }

        if($spasi >= 2 )
        {
            $arr_data[2] = "";
            for($i=0; $i < $spasi; $i++)
            {
                $arr_data[2] .= substr($arr_employee_name[$i],0,1);
            }
            $arr_data[2] .= $day.$month;
        }
        
        $arr_username = array();
        for($i=0; $i < count($arr_data); $i++)
        {
            $username = strtolower($arr_data[$i]);

            $check = $CI->db->get_where('user',array('username'=>$username))->num_rows();
            if($check == 0)
            {
                $arr_username[$i] = $username;
            }
        }
        return $arr_username;
    }
?>