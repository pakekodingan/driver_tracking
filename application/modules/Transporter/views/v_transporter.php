<?php $this->load->view('../../include/header'); ?>
<div class="container-fluid">
  <?php echo breadcrumb($this->uri->segment(1)); ?>
  <div class="row">
    <div class="col-md-12">
      <h1><?php echo menu_name($this->uri->segment(1)); ?></h1>
    </div> 
  </div>
  <div class="row">
    <form role="form" method="post" id="form1" enctype="multipart/form-data">
    <div class="col-md-3">
        <div class="form-group">
            <input type="file" name="file" id="file" class="form-control input-sm" required="" accept=".xls,.xlsx">
        </div>
    </div> 
    <div class="col-md-2">
        <button type="submit" class="btn file2 btn-blue btn-sm"  id="import">
            <i class="fa fa-upload"></i> 
            Import Excel
        </button>
    </div> 
    </form>
    <div class="col-md-7">
        <a href="<?php echo site_url($this->uri->segment(1).'/download_template_excel');?>" class="btn btn-success btn-sm">
            <i class="fa fa-download"></i> 
            Download Template Excel
        </a>
    </div>  
  </div>
  <br>
  <div class="row">
    <div class="col-md-12">
        <div class="card">
            <table class="table table-condensed table-bordered mtable-hover table-striped " id="table-1" >
                <thead>
                    <tr>
                        <th width="5%">No.</th>
                        <th data-hide="phone">NIK</th>
                        <th>NAMA</th>
                        <th data-hide="phone">LOKASI KERJA</th>
                        <th data-hide="phone">STATUS</th>
                        <th data-hide="phone">STATUS RUTE</th>
                        <th data-hide="phone">AREA</th>
                        <th width="6%">ACTION</th>
                    </tr>
                </thead>
                <tbody></tbody>
                <tfoot></tfoot> 
            </table>
        </div>
    </div>
   </div>
</div>
<?php $this->load->view('../../include/footer'); ?>
<script src='<?php echo base_url('application/modules/Transporter/views/transporter.js')?>'></script>
 
<!-- End of file v_transporter.php -->
<!-- Location: ./application/modules/Transporter/views/v_transporter.php -->
