<?php 
defined('BASEPATH') OR exit('No direct script access allowed'); 
 
class Transporter extends CI_Controller { 
 
	public function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('isLogin')){
			redirect('Login');
		}
		$this->load->model('M_transporter','transporter_model');
	}
 
	public function index()
	{
		history('view transporter');
		$this->load->view('v_transporter');
	}
 	
 	public function get_data() {
 		$path = $this->uri->segment(1);
		$access_menu = access_menu($path);

		$active_controller = $this->uri->segment(1);
		/* Array of database columns which should be read and sent back to DataTables. Use a space where
			         * you want to insert a non-database field (for example a counter or static image)
		*/

		$acolumns = array('employee_id', 'employee_nik', 'employee_name', 'birthday', 'username', 'status', 'position', 'status_position', 'work_location', 'rute_status', 'area', 'note');

		// DB table to use
		$stable = 'employee';
		//

		$idisplay_start = $this->input->get_post('iDisplayStart', true);
		$idisplay_length = $this->input->get_post('iDisplayLength', true);
		$isort_col_0 = $this->input->get_post('iSortCol_0', true);
		$isorting_cols = $this->input->get_post('iSortingCols', true);
		$isearch = $this->input->get_post('sSearch', true);
		$secho = $this->input->get_post('sEcho', true);

		// Paging
		if (isset($idisplay_start) && $idisplay_length != '-1') {
			$this->db->limit($this->db->escape_str($idisplay_length), $this->db->escape_str($idisplay_start));
		}

		// Ordering
		if (isset($isort_col_0)) {
			for ($i = 0; $i < intval($isorting_cols); $i++) {
				$isort_col = $this->input->get_post('iSortCol_' . $i, true);
				$bsortable = $this->input->get_post('bSortable_' . intval($isort_col), true);
				$ssort_dir = $this->input->get_post('sSortDir_' . $i, true);

				if ($bsortable == 'true') {
					$this->db->order_by($acolumns[intval($this->db->escape_str($isort_col))], $this->db->escape_str($ssort_dir));
				}
			}
		}

		/*
			         * Filtering
			         * NOTE this does not match the built-in DataTables filtering which does it
			         * word by word on any field. It's possible to do here, but concerned about efficiency
			         * on very large tables, and MySQL's regex functionality is very limited
		*/
		if (isset($isearch) && !empty($isearch)) {
			for ($i = 0; $i < count($acolumns); $i++) {
				$isearchable = $this->input->get_post('bSearchable_' . $i, true);

				// Individual column filtering
				if (isset($isearchable) && $isearchable == 'true') {
					$this->db->or_like($acolumns[$i], $this->db->escape_like_str($isearch));
				}
			}
		}

		// Select Data
		$this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $acolumns)), false);
		$this->db->from($stable);
		$this->db->where('position','TRANSPORTER');
		$rresult = $this->db->get();

		// Data set length after filtering
		$this->db->select('FOUND_ROWS() AS found_rows');
		$ifiltered_total = $this->db->get()->row()->found_rows;

		// Total data set length
		$itotal = $this->db->count_all($stable);

		// Output
		$output = array(
			'sEcho' => intval($secho),
			'iTotalRecords' => $itotal,
			'iTotalDisplayRecords' => $ifiltered_total,
			'aaData' => array(),
		);

		$no = $idisplay_start;
		foreach ($rresult->result_array() as $arow) {
			$no++;
			$row = array();

			$row[] = "<center>" . $no . ".</center>";
			$row[] = $arow['employee_nik'];
			$row[] = $arow['employee_name'];
			$row[] = $arow['work_location'];
			$row[] = $arow['status_position'];
			$row[] = $arow['rute_status'];
			$row[] = $arow['area'];
			
            $action ="";
 			$action .="<center>";
 				if($access_menu['delete']  == 'Y')
 				{
	                $action .= "<button class='btn btn-xs btn-danger' onClick='del(\"" . $arow['employee_id'] . "\",\"" . $arow['employee_name'] . "\")'><i class='fa fa-trash-o'></i></button>";
 				}
            $action .= "<center>";
			$row[] = $action;

			$output['aaData'][] = $row;
		}
		// dd( $output['aaData']);

		history('view data transporter ?search=' . $isearch);
		echo json_encode($output);
	}

	public function import()
	{
		$this->load->library('excel');
		if($_FILES['file']['name'])
		{
		 	$path = $_FILES["file"]["tmp_name"];
		    $object = PHPExcel_IOFactory::load($path);
    	  	$object->getWorksheetIterator();
    	  	$data = array();
    	  	try {
			  	$this->db->trans_begin();
				foreach($object->getWorksheetIterator() as $worksheet){
			        $highestRow = $worksheet->getHighestRow();
			        $highestColumn = $worksheet->getHighestColumn();
			        for($row=2; $row<=$highestRow; $row++){
			           	$name = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
			           	$status = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
			           	$nik = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
			           	$status_rute = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
			           	$area = $worksheet->getCellByColumnAndRow(5, $row)->getValue();
			           	$ket = $worksheet->getCellByColumnAndRow(6, $row)->getValue();
			           	$check_nik = $this->transporter_model->check_nik($nik);
			           	if($check_nik == 0)
			           	{
				           	$data = array(
				            	'employee_name'	=> $name,
				            	'employee_nik'	=> $nik,
				            	'status_position' => $status,
				            	'rute_status' => $status_rute,
				            	'area' => $area,
				            	'note' => $ket
				           	);
						    $this->transporter_model->insert_data($data);
			           	}
			           	else
			           	{
			           		$data_update = array(
				            	'employee_name'	=> $name,
				            	'status_position'	=> $status,
				            	'rute_status'	=> $status_rute,
				            	'area'	=> $area,
				            	'note'	=> $ket
				           	);
				            $where = array(	'employee_nik' => $nik);
						    $this->transporter_model->update_data($data_update,$where);
			           	}
			        }
			    }  
				$this->db->trans_commit();
				echo json_encode(array('message'=>'success'));
			}
			catch (Exception $e) {
				$this->db->trans_rollback();
				echo json_encode(array('message'=>'error','errors'=>$e));
			} 
		}
	}

	public function download_template_excel()
	{
		$this->load->library("Excel");
	    $object = new PHPExcel();

	    $style_header_row = array(
	      'alignment' => array(
	      	'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER 
	      ),
	      'borders' => array(
	        'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), 
	        'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), 
	        'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
	        'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) 
	      ),
	      'fill' => array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'color' => array('rgb' => '26186f')
	       ),
	      'font'  => array(
		        'bold'  => true,
		        'color' => array('rgb' => 'ffffff')
		    )
	  	);

	  	$style_row = array(
	      'alignment' => array(
	      	'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER 
	      ),
	      'borders' => array(
	        'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), 
	        'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), 
	        'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
	        'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) 
	      )
	  	);

	  	$object->setActiveSheetIndex(0)->setCellValue('A1', "NO"); 
	  	$object->setActiveSheetIndex(0)->setCellValue('B1', "NAMA"); 
	  	$object->setActiveSheetIndex(0)->setCellValue('C1', "STATUS"); 
	  	$object->setActiveSheetIndex(0)->setCellValue('D1', "NIK"); 
	  	$object->setActiveSheetIndex(0)->setCellValue('E1', "STATUS RUTE"); 
	  	$object->setActiveSheetIndex(0)->setCellValue('F1', "AREA"); 
	  	$object->setActiveSheetIndex(0)->setCellValue('G1', "KET");
	  	$object->getActiveSheet()->getStyle('A1')->applyFromArray($style_header_row);
	    $object->getActiveSheet()->getStyle('B1')->applyFromArray($style_header_row);
	    $object->getActiveSheet()->getStyle('C1')->applyFromArray($style_header_row);
	    $object->getActiveSheet()->getStyle('D1')->applyFromArray($style_header_row);
	    $object->getActiveSheet()->getStyle('E1')->applyFromArray($style_header_row);
	    $object->getActiveSheet()->getStyle('F1')->applyFromArray($style_header_row);
	    $object->getActiveSheet()->getStyle('G1')->applyFromArray($style_header_row);
	    $object->getActiveSheet(0)->getColumnDimension('E')->setWidth(20);
	    $object->getActiveSheet(0)->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
	    $object->getActiveSheet(0)->setTitle("TRANSPORTER");
	    $object->setActiveSheetIndex(0);
	    $write = PHPExcel_IOFactory::createWriter($object, 'Excel2007');
	    ob_end_clean();
	    
	    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	    header('Content-Disposition: attachment; filename="TEMPLATE IMPORT TRANSPORTER.xlsx"'); 
	    header('Cache-Control: max-age=0');
	    $write->save('php://output');
	}

	public function delete() {
		$id = $this->input->get('id');
		$result = $this->transporter_model->transporter_delete($id);
		$message = "failed";
		if ($result == 1) {
			$message = "success";
		}
		history('deleted transporter ?id=' . $id . ':' . $message);
		echo json_encode(array("message" => $message));
	}
}
 
/* End of file Transporter.php */
/* Location: ./application/modules/Transporter/controllers/Transporter.php */
