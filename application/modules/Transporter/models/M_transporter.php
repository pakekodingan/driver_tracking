<?php 
defined('BASEPATH') OR exit('No direct script access allowed'); 
 
class M_transporter extends CI_Model { 
 
    public function check_nik($nik)
    {
        $check = $this->db->get_where('employee',array('employee_nik'=>$nik))->num_rows();
        if($check > 0)
        {
            $result = 1;
        }
        else
        {
            $result = 0;
        }
        return $result;
    }

    public function insert_data($data)
    {
        $this->db->insert('employee',$data);
    }

    public function update_data($data,$where)
    {
        $this->db->update('employee',$data,$where);
    }
    
    public function transporter_delete($id)
    {
        $delete = $this->db->delete('employee',array('employee_id'=>$id));
        $result = ($delete == true)?1:0;
        return $result;
    }
}
 
/* End of file M_transporter.php */
/* Location: ./application/modules/Transporter/models/M_transporter.php */
