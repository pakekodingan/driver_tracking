<?php 
defined('BASEPATH') OR exit('No direct script access allowed'); 
 
class M_vehicle extends CI_Model { 
 
	public function check_plat_no($plat_no)
    {
        $check = $this->db->get_where('vehicle',array('plat_number'=>$plat_no))->num_rows();
        if($check > 0)
        {
            $result = 1;
        }
        else
        {
            $result = 0;
        }
        return $result;
    }
    
    public function insert_data($data)
    {
        $this->db->insert('vehicle',$data);
    }

    public function update_data($data,$where)
    {
        $this->db->update('vehicle',$data,$where);
    }
    
    public function vehicle_delete($id)
    {
        $delete = $this->db->delete('vehicle',array('plat_number'=>$id));
        $result = ($delete == true)?1:0;
        return $result;
    }
}
 
/* End of file M_vehicle.php */
/* Location: ./application/modules/Vehicle/models/M_vehicle.php */
