<?php 
defined('BASEPATH') OR exit('No direct script access allowed'); 
 
class Vehicle extends CI_Controller { 
 
	public function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('isLogin')){
			redirect('Login');
		}
		$this->load->model('M_vehicle','vehicle_model');
	}
 
	public function index()
	{
		$path = $this->uri->segment(1);
		$access_menu = access_menu($path);
		if($access_menu['read'] == 'Y')
		{
			$this->load->view('v_vehicle');
		}
		else
		{
			history('view vehicle :denied');
			$this->load->view('../../include/access_denied');
		}
	}
 
 	public function get_data() {
 		$path = $this->uri->segment(1);
		$access_menu = access_menu($path);

		$active_controller = $this->uri->segment(1);
		/* Array of database columns which should be read and sent back to DataTables. Use a space where
			         * you want to insert a non-database field (for example a counter or static image)
		*/

		$acolumns = array('vehicle_id', 'vehicle_code', 'plat_number', 'type', 'gw', 'ton', 'stnk', 'tax', 'kir_1', 'kir_2', 'kir_3', 'expired_kir', 'expired_stnk', 'expired_tax', 'note');

		// DB table to use
		$stable = 'vehicle';
		//

		$idisplay_start = $this->input->get_post('iDisplayStart', true);
		$idisplay_length = $this->input->get_post('iDisplayLength', true);
		$isort_col_0 = $this->input->get_post('iSortCol_0', true);
		$isorting_cols = $this->input->get_post('iSortingCols', true);
		$isearch = $this->input->get_post('sSearch', true);
		$secho = $this->input->get_post('sEcho', true);

		// Paging
		if (isset($idisplay_start) && $idisplay_length != '-1') {
			$this->db->limit($this->db->escape_str($idisplay_length), $this->db->escape_str($idisplay_start));
		}

		// Ordering
		if (isset($isort_col_0)) {
			for ($i = 0; $i < intval($isorting_cols); $i++) {
				$isort_col = $this->input->get_post('iSortCol_' . $i, true);
				$bsortable = $this->input->get_post('bSortable_' . intval($isort_col), true);
				$ssort_dir = $this->input->get_post('sSortDir_' . $i, true);

				if ($bsortable == 'true') {
					$this->db->order_by($acolumns[intval($this->db->escape_str($isort_col))], $this->db->escape_str($ssort_dir));
				}
			}
		}

		/*
			         * Filtering
			         * NOTE this does not match the built-in DataTables filtering which does it
			         * word by word on any field. It's possible to do here, but concerned about efficiency
			         * on very large tables, and MySQL's regex functionality is very limited
		*/
		if (isset($isearch) && !empty($isearch)) {
			for ($i = 0; $i < count($acolumns); $i++) {
				$isearchable = $this->input->get_post('bSearchable_' . $i, true);

				// Individual column filtering
				if (isset($isearchable) && $isearchable == 'true') {
					$this->db->or_like($acolumns[$i], $this->db->escape_like_str($isearch));
				}
			}
		}

		// Select Data
		$this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $acolumns)), false);
		$rresult = $this->db->get($stable);

		// Data set length after filtering
		$this->db->select('FOUND_ROWS() AS found_rows');
		$ifiltered_total = $this->db->get()->row()->found_rows;

		// Total data set length
		$itotal = $this->db->count_all($stable);

		// Output
		$output = array(
			'sEcho' => intval($secho),
			'iTotalRecords' => $itotal,
			'iTotalDisplayRecords' => $ifiltered_total,
			'aaData' => array(),
		);

		$no = $idisplay_start;
		foreach ($rresult->result_array() as $arow) {
			$no++;
			$row = array();
			$row[] = "<center>" . $no . ".</center>";
			$row[] = $arow['gw'];
			$row[] = $arow['ton'];
			$row[] = $arow['plat_number'];
			$row[] = $arow['type'];
			$row[] = $arow['vehicle_code'];
			$row[] = $arow['stnk'];
			$row[] = $arow['tax'];
			$row[] = $arow['kir_1'];
			$row[] = $arow['kir_2'];
			$row[] = $arow['kir_3'];
			$row[] = view_date($arow['expired_kir']);
			$row[] = view_date($arow['expired_stnk']);
			$row[] = view_date($arow['expired_tax']);

			$action ="";
 			$action .="<center>";
 				if($access_menu['delete']  == 'Y')
 				{
	                $action .= "<button class='btn btn-xs btn-danger' onClick='del(\"" . $arow['plat_number'] . "\",\"" . $arow['vehicle_code'] . "\")'><i class='fa fa-trash-o'></i></button>";
 				}
            $action .= "<center>";
			$row[] = $action;

			$output['aaData'][] = $row;
		}
		// dd( $output['aaData']);

		history('view data vehicle ?search=' . $isearch);
		echo json_encode($output);
	}

	public function import()
	{
		$this->load->library('excel');
		if($_FILES['file']['name'])
		{
		 	$path = $_FILES["file"]["tmp_name"];
		    $object = PHPExcel_IOFactory::load($path);
    	  	$object->getWorksheetIterator();
    	  	$data = array();
    	  	try {
			  	$this->db->trans_begin();
				foreach($object->getWorksheetIterator() as $worksheet){
			        $highestRow = $worksheet->getHighestRow();
			        $highestColumn = $worksheet->getHighestColumn();
			        for($row=2; $row<=$highestRow; $row++){
			           	$no = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
			           	$gw = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
			           	$ton = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
			           	$plat_no = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
			           	$jenis = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
			           	$vehicle = $worksheet->getCellByColumnAndRow(5, $row)->getValue();
			           	$stnk = $worksheet->getCellByColumnAndRow(6, $row)->getValue();
			           	$pajak = $worksheet->getCellByColumnAndRow(7, $row)->getValue();
			           	$kir1 = $worksheet->getCellByColumnAndRow(8, $row)->getValue();
			           	$kir2 = $worksheet->getCellByColumnAndRow(9, $row)->getValue();
			           	$kir3 = $worksheet->getCellByColumnAndRow(10, $row)->getValue();
			           	$berlaku_kir = $worksheet->getCellByColumnAndRow(11, $row)->getValue();
			           	$berlaku_stnk = $worksheet->getCellByColumnAndRow(12, $row)->getValue();
			           	$berlaku_pajak = $worksheet->getCellByColumnAndRow(13, $row)->getValue();
			           	$ket = $worksheet->getCellByColumnAndRow(14, $row)->getValue();
			           	$check_plat_no = $this->vehicle_model->check_plat_no($plat_no);
			           	if($check_plat_no == 0)
			           	{
				           	$data = array(
				            	'vehicle_code'	=> $vehicle,
				            	'plat_number'	=> $plat_no,
				            	'type' => $jenis,
				            	'gw' => $gw,
				            	'ton' => $ton,
				            	'stnk' => $stnk,
				            	'tax' => $pajak,
				            	'kir_1' => $kir1,
				            	'kir_2' => $kir2,
				            	'kir_3' => $kir3,
				            	'expired_kir' => save_date($berlaku_kir),
				            	'expired_stnk' => save_date($berlaku_stnk),
				            	'expired_tax' => save_date($berlaku_pajak),
				            	'note' => $ket
				           	);
						    $this->vehicle_model->insert_data($data);
			           	}
			           	else
			           	{
			           		$data_update = array(
				            	'vehicle_code'	=> $vehicle,
				            	'type' => $jenis,
				            	'gw' => $gw,
				            	'ton' => $ton,
				            	'stnk' => $stnk,
				            	'tax' => $pajak,
				            	'kir_1' => $kir1,
				            	'kir_2' => $kir2,
				            	'kir_3' => $kir3,
				            	'expired_kir' => save_date($berlaku_kir),
				            	'expired_stnk' => save_date($berlaku_stnk),
				            	'expired_tax' => save_date($berlaku_pajak),
				            	'note' => $ket
				           	);
				            $where = array(	'plat_number' => $plat_no);
						    $this->vehicle_model->update_data($data_update,$where);
			           	}
			        }
			    }  
				$this->db->trans_commit();
				echo json_encode(array('message'=>'success'));
			}
			catch (Exception $e) {
				$this->db->trans_rollback();
				echo json_encode(array('message'=>'error','errors'=>$e));
			} 
		}
	}

	public function download_template_excel()
	{
		$this->load->library("Excel");
	    $object = new PHPExcel();

	    $style_header_row = array(
	      'alignment' => array(
	      	'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER 
	      ),
	      'borders' => array(
	        'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), 
	        'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), 
	        'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
	        'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) 
	      ),
	      'fill' => array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'color' => array('rgb' => '26186f')
	       ),
	      'font'  => array(
		        'bold'  => true,
		        'color' => array('rgb' => 'ffffff')
		    )
	  	);

	  	$style_row = array(
	      'alignment' => array(
	      	'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER 
	      ),
	      'borders' => array(
	        'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), 
	        'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), 
	        'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
	        'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) 
	      )
	  	);

	  	$object->setActiveSheetIndex(0)->setCellValue('A1', "NO"); 
	  	$object->setActiveSheetIndex(0)->setCellValue('B1', "GW"); 
	  	$object->setActiveSheetIndex(0)->setCellValue('C1', "TON"); 
	  	$object->setActiveSheetIndex(0)->setCellValue('D1', "PLAT NO"); 
	  	$object->setActiveSheetIndex(0)->setCellValue('E1', "JENIS"); 
	  	$object->setActiveSheetIndex(0)->setCellValue('F1', "VEHICLE"); 
	  	$object->setActiveSheetIndex(0)->setCellValue('G1', "STNK");
	  	$object->setActiveSheetIndex(0)->setCellValue('H1', "PAJAK");
	  	$object->setActiveSheetIndex(0)->setCellValue('I1', "KIR 1");
	  	$object->setActiveSheetIndex(0)->setCellValue('J1', "KIR 2");
	  	$object->setActiveSheetIndex(0)->setCellValue('K1', "KIR 3");
	  	$object->setActiveSheetIndex(0)->setCellValue('L1', "BERLAKU KIR");
	  	$object->setActiveSheetIndex(0)->setCellValue('M1', "BERLAKU STNK");
	  	$object->setActiveSheetIndex(0)->setCellValue('N1', "BERLAKU PAJAK");
	  	$object->setActiveSheetIndex(0)->setCellValue('O1', "KET");
	  	$object->getActiveSheet()->getStyle('A1')->applyFromArray($style_header_row);
	    $object->getActiveSheet()->getStyle('B1')->applyFromArray($style_header_row);
	    $object->getActiveSheet()->getStyle('C1')->applyFromArray($style_header_row);
	    $object->getActiveSheet()->getStyle('D1')->applyFromArray($style_header_row);
	    $object->getActiveSheet()->getStyle('E1')->applyFromArray($style_header_row);
	    $object->getActiveSheet()->getStyle('F1')->applyFromArray($style_header_row);
	    $object->getActiveSheet()->getStyle('G1')->applyFromArray($style_header_row);
	    $object->getActiveSheet()->getStyle('H1')->applyFromArray($style_header_row);
	    $object->getActiveSheet()->getStyle('I1')->applyFromArray($style_header_row);
	    $object->getActiveSheet()->getStyle('J1')->applyFromArray($style_header_row);
	    $object->getActiveSheet()->getStyle('K1')->applyFromArray($style_header_row);
	    $object->getActiveSheet()->getStyle('L1')->applyFromArray($style_header_row);
	    $object->getActiveSheet()->getStyle('M1')->applyFromArray($style_header_row);
	    $object->getActiveSheet()->getStyle('N1')->applyFromArray($style_header_row);
	    $object->getActiveSheet()->getStyle('O1')->applyFromArray($style_header_row);
	    $object->getActiveSheet(0)->getColumnDimension('D')->setWidth(20);
	    $object->getActiveSheet(0)->getColumnDimension('E')->setWidth(20);
	    $object->getActiveSheet(0)->getColumnDimension('L')->setWidth(20);
	    $object->getActiveSheet(0)->getColumnDimension('M')->setWidth(20);
	    $object->getActiveSheet(0)->getColumnDimension('N')->setWidth(20);
	    $object->getActiveSheet(0)->getColumnDimension('F')->setWidth(30);
	    $object->getActiveSheet()->getStyle('L')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
	    $object->getActiveSheet()->getStyle('M')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
	    $object->getActiveSheet()->getStyle('N')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
	    $object->getActiveSheet(0)->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
	    $object->getActiveSheet(0)->setTitle("KENDARAAN");
	    $object->setActiveSheetIndex(0);
	    $write = PHPExcel_IOFactory::createWriter($object, 'Excel2007');
	    ob_end_clean();
	    
	    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	    header('Content-Disposition: attachment; filename="TEMPLATE IMPORT KENDARAAN.xlsx"'); 
	    header('Cache-Control: max-age=0');
	    $write->save('php://output');
	}

	public function delete() {
		$id = $this->input->get('id');
		$result = $this->vehicle_model->vehicle_delete($id);
		$message = "failed";
		if ($result == 1) {
			$message = "success";
		}
		history('deleted vehicle ?id=' . $id . ':' . $message);
		echo json_encode(array("message" => $message));
	}
}
 
/* End of file Vehicle.php */
/* Location: ./application/modules/Vehicle/controllers/Vehicle.php */
