jQuery(document).ready(function($)
{
    refresh_data();

    $("#save").click(function(){
        if($(".validate").valid()){
            $("#form1").submit();
        }
    });

    $("#form1").submit(function(e){
        e.preventDefault();
        $.ajax({
            url : site_url + active_controller + '/import',
            type : 'POST',
            data : new FormData(this),
            processData : false, 
            contentType : false, 
            beforeSend: function(){
                $('#import').html('<i class="fa fa-spinner rotate"></i> Processing..');
            },
            success: function(response){
                res = JSON.parse(response);
                if(res.message == 'success'){
                    swal("Success", "Thanks You.", "success");
                    $("#file").val(''); 
                    refresh_data();
                }else{
                    swal("Failed !", "Your proccess is failed \n"+res.errors, "error");
                }

                $('#import').html('<i class="fa fa-upload"></i> Import Excel');
            },
            error: function(e){
                $('#import').html('<i class="fa fa-upload"></i> Import Excel');
                  swal("Error !", "Your proccess is error \n"+e, "error");

            }
        });
    });

    $('input.icheck-1').iCheck({
        checkboxClass: 'icheckbox_minimal-orange',
        radioClass: 'iradio_minimal-orange'
    });

    $('input.icheck-2').iCheck({
        checkboxClass: 'icheckbox_minimal-blue',
        radioClass: 'iradio_minimal-blue'
    });

    $('input.icheck-3').iCheck({
        checkboxClass: 'icheckbox_minimal-green',
        radioClass: 'iradio_minimal-green'
    });

    $('input.icheck-4').iCheck({
        checkboxClass: 'icheckbox_minimal-yellow',
        radioClass: 'iradio_minimal-yellow'
    });

    $('input.icheck-5').iCheck({
        checkboxClass: 'icheckbox_minimal-red',
        radioClass: 'iradio_minimal-red'
    });

    $('input.icheck-6').iCheck({
        checkboxClass: 'icheckbox_minimal-purple',
        radioClass: 'iradio_minimal-purple'
    });

    $('input.icheck-7').iCheck({
        checkboxClass: 'icheckbox_minimal-grey',
        radioClass: 'iradio_minimal-grey'
    });
    
});

function refresh_data(){

    var responsiveHelper;
    var breakpointDefinition = {
        tablet: 1024,
        phone : 480
    };
    var tableContainer;

    tableContainer = $("#table-1");
    param1 = "";
    param2 = "";

    param ="";
    param += "?param1="+param1
    param += "&param2="+param2;

    tableContainer.dataTable({
        "sPaginationType": "bootstrap",
        "aLengthMenu": [10, 25, 50, 100,"All"],
        "aaSorting": [[0, 'asc']],
        "bDestroy": true,
        "bStateSave": true,
        "bProcessing": true,
        "bServerSide": true,
        "sServerMethod": "GET",
        "sAjaxSource": site_url+active_controller+"/get_data"+param,
        "iDisplayLength": 10,

        // Responsive Settings
        "bAutoWidth"     : false,
        fnPreDrawCallback: function () {
            // Initialize the responsive datatables helper once.
            if (!responsiveHelper) {
                responsiveHelper = new ResponsiveDatatablesHelper(tableContainer, breakpointDefinition);
            }
        },
        fnRowCallback  : function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            responsiveHelper.createExpandIcon(nRow);
        },
        fnDrawCallback : function (oSettings) {
            responsiveHelper.respond();
        }
    });

    $(".dataTables_wrapper select").select2({
        minimumResultsForSearch: -1
    });
}

function del(id,nama){
    swal({
        title: "Delete "+nama+" ?",
        text: "Click Delete. ",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Yes, Delete !",
        cancelButtonText: "No, Cancel !",
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
        closeOnCancel: false
    },
    function(isConfirm) {
        if (isConfirm) {
            $.ajax({
                  url : site_url + active_controller + "/delete?id="+id,
                  success : function(response){
                    var res = JSON.parse(response);
                    if(res.message == 'success'){
                       swal({
                                title: "Success",
                                text: "Deleted, Thanks You.",
                                type: "success",
                                showCancelButton: true,
                                showCancelButton: false
                            },
                            function(isConfirm) {
                                refresh_data();
                                // location.reload();
                        });
                    }else{
                      swal("Gagal !", "Data gagal reject", "error");
                    }
                  },
                  error : function(){
                    swal("Error !", "Failed", "error");
                  }
              });
        } else {
            swal("Cancelled", "Your data is safe.", "error");
        }
    });
}


