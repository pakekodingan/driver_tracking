<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_login extends CI_Model {

	public function check_login($data)
	{
		$result = $this->db->get_where('user',array('username'=>$data['username'],'password'=>$data['password']));
		return $result;
	}

	public function online($username)
	{
		$this->db->update('user',array('online'=>'Y'),array('username'=>$username));
	}

	public function offline($username)
	{
		$this->db->update('user',array('online'=>'N'),array('username'=>$username));
	}

	public function get_group($group_id)
	{
		$result = $this->db->get_where('group',array('group_id'=>$group_id));
		return $result;
	}
	public function get_employee($username)
	{
	   	$result = $this->db->get_where('employee',array('username'=>$username));
		return $result;
	}
	
}

/* End of file m_login.php */
/* Location: ./application/modules/login/models/m_login.php */