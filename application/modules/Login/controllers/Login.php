<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_login','login_model');
		$this->session->keep_flashdata('alert_data');
	}
	public function index()
	{
		if($this->input->post())
		{	
			$username = $this->input->post('username');
			$password = md5($this->input->post('password'));
			$check_data = array('username' => $username,'password' => $password);
			$check = $this->login_model->check_login($check_data);
			if($check->num_rows() > 0)
			{
				$val_user	= $check->row();
				$user_id = $val_user->user_id;
				$username = $val_user->username;
				$val_group = $this->login_model->get_group($val_user->group_id)->row();
				$val_employee = $this->login_model->get_employee($username)->row();
				$data = array(
								'ses_userid' 		=> $user_id,
								'ses_username' 		=> $username,
								'ses_groupid'		=> $val_user->group_id, 
								'ses_groupname'		=> $val_group->group_name,
								'ses_all_access'	=> $val_group->flag_all,
								'ses_employee_id'	=> $val_employee->employee_id,
								'ses_fullname'		=> ucwords(strtolower($val_employee->employee_name)),
								'isLogin' 			=> true
							 ); 
				$this->session->set_userdata($data);
				history('login');
				$this->login_model->online($username);
				
				echo json_encode(array('login_status' => 'success'));				
			}	
			else
			{
				echo json_encode(array('login_status' => 'invalid'));
			}
		}
		else
		{
			$this->load->view('v_login');
		}
	}

	public function logout() {
		$username = $this->session->userdata('ses_username');
		$this->login_model->offline($username);
		history('logout');
		$this->session->sess_destroy();
		redirect('Login');
	}

}

/* End of file Login.php */
/* Location: ./application/modules/Login/controllers/login.php */