<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta name="description" content="Neon Admin Panel" />
  <meta name="author" content="" />
  
  <title>Operational 520 | Login</title>
  
  <link rel="icon" href="<?php echo base_url('assets/images/520.png');?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/neon/assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css')?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/neon/assets/css/font-icons/entypo/css/entypo.css')?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/neon/http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic')?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/neon/assets/css/bootstrap.css')?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/neon/assets/css/neon-core.css')?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/neon/assets/css/neon-theme.css')?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/neon/assets/css/neon-forms.css')?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/neon/assets/css/custom.css')?>">

  
  <style type="text/css">
    body::-webkit-scrollbar {
        width: 0px;
    }
    body::-moz-scrollbar {
        width: 0px;
    }
    body::-o-scrollbar {
        width: 0px;
    }
  </style>
  
  
</head>
<body class="page-body login-page login-form-fall">

<!-- This is needed when you send requests via Ajax --> 
<script type="text/javascript">
var base_url = '<?php echo base_url(); ?>';
var site_url = '<?php echo site_url(); ?>';

  </script>

<div class="login-container">
  
  <div class="login-header login-caret">
    
    <div class="login-content">
      
      <a href="#" class="logo">
        <!-- <img src="<?php echo base_url('assets/images/logo-jnt.png'); ?>" width="150" alt="" /> -->
        <h2 style="color:white;">Operational 520</h2>
      </a>
      
      <p class="description"><?php echo application('app_name'); ?></p>
      <!-- <p class="description">Dear user, log in to access this area !</p> -->
      
      <!-- progress bar indicator -->
      <div class="login-progressbar-indicator">
        <h3>43%</h3>
        <span>logging in...</span>
      </div>
    </div>
    
  </div>
  
  <div class="login-progressbar">
    <div></div>
  </div>
  
  <div class="login-form">
    
    <div class="login-content">
      
      <div class="form-login-error" style="margin-top:-20px;">
        <h3>Invalid login</h3>
        <p>Enter <strong>Username</strong> as login and password.</p>
      </div>
      
      <form action="<?php echo site_url('login')?>"  method="post"  role="form" id="form_login">
        
        <div class="form-group">
          
          <div class="input-group">
            <div class="input-group-addon">
              <i class="entypo-user"></i>
            </div>
            
            <input type="text" class="form-control" name="uname" id="uname" placeholder="NIK" autocomplete="off" />
          </div>
          
        </div>
        
        <div class="form-group">
          
          <div class="input-group">
            <div class="input-group-addon">
              <i class="entypo-key"></i>
            </div>
            
            <input type="password" class="form-control" name="pwd" id="pwd" placeholder="Password" autocomplete="off" />
          </div>
        
        </div>
        
        <div class="form-group">
          <button type="submit" class="btn btn-primary btn-block btn-login">
            <i class="entypo-login"></i>
            Login In
          </button>
        </div>
        
            
      </form>
      
      
      <div class="login-bottom-links">
        
        <a href="extra-forgot-password.html" class="link">Forgot your password?</a>
        
        <br />
        
        <a href="#">ToS</a>  - <a href="#">Privacy Policy</a>
        
      </div>
      
    </div>
    
  </div>
  
</div>


  <!-- Bottom Scripts -->
  <script src="<?php echo base_url('assets/neon/assets/js/jquery-1.11.0.min.js');?>"></script>
  <script src="<?php echo base_url('assets/neon/assets/js/gsap/main-gsap.js');?>"></script>
  <script src="<?php echo base_url('assets/neon/assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js');?>"></script>
  <script src="<?php echo base_url('assets/neon/assets/js/bootstrap.js');?>"></script>
  <script src="<?php echo base_url('assets/neon/assets/js/joinable.js');?>"></script>
  <script src="<?php echo base_url('assets/neon/assets/js/resizeable.js');?>"></script>
  <script src="<?php echo base_url('assets/neon/assets/js/neon-api.js');?>"></script>
  <script src="<?php echo base_url('assets/neon/assets/js/jquery.validate.min.js');?>"></script>
  <script src="<?php echo base_url('assets/neon/assets/js/neon-login.js');?>"></script>
  <script src="<?php echo base_url('assets/neon/assets/js/neon-custom.js');?>"></script>
  <script src="<?php echo base_url('assets/neon/assets/js/neon-demo.js');?>"></script>

</body>
</html>