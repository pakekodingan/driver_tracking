<?php 
defined('BASEPATH') OR exit('No direct script access allowed'); 
 
class Report_takeway extends CI_Controller { 
 
	public function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('isLogin')){
			redirect('Login');
		}
		$this->load->model('M_report_takeway','report_takeway_model');
	}
 
	public function index()
	{
		$this->load->view('v_report_takeway');
	}
 
}
 
/* End of file Report_takeway.php */
/* Location: ./application/modules/Report_takeway/controllers/Report_takeway.php */
