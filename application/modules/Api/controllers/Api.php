<?php 
defined('BASEPATH') OR exit('No direct script access allowed'); 
 
class Api extends CI_Controller { 
 
	public function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('isLogin')){
			redirect('Login');
		}
		$this->load->model('M_api','api_model');
	}
 
	public function index()
	{
		$this->load->view('v_api');
	}
 
}
 
/* End of file Api.php */
/* Location: ./application/modules/Api/controllers/Api.php */
