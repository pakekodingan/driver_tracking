<div class="col-md-2">
</div> 
<div class="col-md-8"> 
    <h4>HISTORY</h4>
    <div  style="overflow-x: scroll;">
        <table class="table table-striped table-hover">
            <thead>
              <tr>
                <th>STATUS</th>
                <th>LOKASI</th>
                <th>TANGGAL</th>
                <th>WAKTU</th>
                <th>KM</th>
              </tr>
            </thead>
            <tbody>
              <?php 
                  foreach ($detail as $key => $val) {
                    echo "<tr>";
                    echo "<td>".$val['type_outgoing']."</td>";
                    echo "<td>".$val['location']."</td>";
                    echo "<td>".view_date($val['dp_date'])."</td>";
                    echo "<td>".view_time($val['dp_time'])."</td>";
                    echo "<td>".$val['km']."</td>";
                    echo "</tr>";
                  }
              ?>
            </tbody>
        </table>
    </div>
</div>
<div class="col-md-2">
</div> 