<form id="form2"  enctype="multipart/form-data">
    <input type="hidden" name="type" id="type" value="<?php echo $type; ?>">
    <input type="hidden" name="outgoing_id" value="<?php echo $outgoing_id; ?>">
    <div class="col-md-12">
        <!-- <b>Tanggal</b><br><p><?php echo $date; ?></p> -->
        <input type="hidden" name="dp_date" value="<?php echo $date; ?>">
    </div>
    <div class="col-md-12">
        <!-- <b>Waktu</b><br><p><?php echo $time; ?></p> -->
        <input type="hidden" name="dp_time" value="<?php echo $time; ?>">
    </div>
    <div class="col-md-12">
        <b>Ambil Foto</b><br>
        <input type="file" name="photo"  accept="image/*" capture="camera" id="image-source" onchange="previewImage()" class="form-control" data-label="<i class='fa fa-camera'></i> &nbsp;Ambil Foto &nbsp;&nbsp;"  required><br>
        <img id="image-preview" alt="image preview"/ width="100" style="display:none;width:100%"><br>
    </div>
    <div class="col-md-12">
        <b>Lokasi</b><br>
        <input type="text" class="form-control uppercase" name="location" id="location" value="<?php echo $standby_location_next; ?>"  readonly><br>
    </div>
    <div class="col-md-12">
        <b>KM</b><br>
        <input type="text" class="form-control just-numeric" name="km" id="km" value=""  required><br>
    </div>
    <div class="col-md-12 text-center">
        <button type="submit" class="btn btn-success" id="save_detail"><i class="fa fa-save"></i> Save</button>
    </div>
</form>
<script type="text/javascript">
    jQuery(document).ready(function($)
    {
        $("#form2").submit(function(e){
            e.preventDefault();
            $.ajax({
                url : site_url + active_controller + '/save_detail',
                type : 'POST',
                data : new FormData(this),
                processData : false, 
                contentType : false, 
                beforeSend: function(){
                    $('#save_detail').loading('start');
                },
                success: function(response){
                    res = JSON.parse(response); 
                    if(res.message == 'success'){
                        $('#modal-6').modal('hide');
                        swal({
                                title: "Success",
                                text: "Thanks You.",
                                type: "success",
                                showCancelButton: true,
                                showCancelButton: false
                            },
                            function(isConfirm) {
                                standby_next = $('#type').val();
                                standby_location_next = $('#location').val();
                                $('#standby').val(standby_next);
                                $('#standby_location').val(standby_location_next);
                                hide_show_in_out();

                                type = $('#type').val();
                                if(type == 'FINISH'){
                                    window.location = site_url + active_controller;
                                }else{
                                    history_outgoing(res.id);
                                }
                        });
                    }else{
                        error1 = res.error.replace('<p>','');
                        error = error1.replace('</p>','');
                        swal("Failed !", "Your proccess is failed \n"+error, "error");
                    }

                    $('#save_detail').loading('stop');      
                },
                error: function(){
                  $('#save_detail').loading('stop');      
                  swal("Error !", "Your proccess is error", "error");

                }
            });
        });

        $(".just-numeric").on("keypress keyup blur",function (event) {    
           $(this).val($(this).val().replace(/[^\d].+/, ""));
            if ((event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });
    });

</script>
<style type="text/css">
    .uppercase {
        text-transform: uppercase;
    }

    .file2[type="file"] {
      visibility: hidden;
    }
    .file-input-wrapper .file2[type="file"] {
      visibility: visible;
    }
</style>