<?php $this->load->view('../../include/header'); ?>
<div class="container-fluid">
  <?php echo breadcrumb($this->uri->segment(1)); ?>
  <div class="row">
    <div class="col-md-12">
      <h1><?php echo menu_name($this->uri->segment(1)); ?></h1>
    </div> 
  </div>
  <div class="row">
    <div class="col-md-12">
        <?php if($access_menu['create']){ ?>
        <button type="button" class="btn btn-blue pull-right" onclick="move_page('<?php echo site_url('Outgoing/add'); ?>')">
            <i class="fa fa-plus"></i> 
            Add
        </button>
        <?php } ?>
    </div>  
  </div>
  <br>
  <div class="row">
    <div class="col-md-12">
        <div class="card">
            <table class="table table-condensed table-bordered mtable-hover table-striped " id="table-1" style="width: 100%;">
                <thead>
                    <tr>
                        <th width="5%">No.</th>
                        <th>TANGGAL</th>
                        <th>NO.PERJALANAN</th>
                        <th data-hide="phone">RUTE</th>
                        <th data-hide="phone">KODE MOBIL</th>
                        <th data-hide="phone">TRANSPORTER</th>
                        <th data-hide="phone">START</th>
                        <th data-hide="phone">KM AWAL</th>
                        <th data-hide="phone">KM AKHIR</th>
                        <th>STATUS</th>
                        <!-- <th>ACTION</th> -->
                    </tr>
                </thead>
                <tbody></tbody>
                <tfoot></tfoot> 
            </table>
        </div>
    </div>
   </div>
   <br>
   <div class="row view-detail">
        <!-- load v_outgoing_detail -->
   </div>
</div>
<?php $this->load->view('../../include/footer'); ?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('application/modules/Outgoing/views/timeline_vertical.css')?>">
<script src='<?php echo base_url('application/modules/Outgoing/views/outgoing.js')?>'></script>

<!-- End of file v_outgoing.php -->
<!-- Location: ./application/modules/Outgoing/views/v_outgoing.php -->
