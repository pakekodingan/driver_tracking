<?php $this->load->view('../../include/header'); ?>
<!-- <?php dump($list_parent); ?> -->
<div class="container-fluid">
  <?php echo breadcrumb($this->uri->segment(1),$this->uri->segment(2)); ?>
    <div class="row">
        <div class="col-md-12">
          <h2>Edit Keberangkatan</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <button type="button" class="btn btn-primary pull-right" onclick="move_page('<?php echo site_url('Outgoing'); ?>')">
                <i class="fa fa-arrow-left"></i>
                Back
            </button>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-info">
                <div class="panel-body">
                  <div class="row">
                    <div class="col-md-12">
                      <h3>No. Perjalanan : <?php echo $header[0]['outgoing_id']; ?></h3><hr>
                      <form role="form" id="form1" method="post" class="validate">
                        <input type="hidden" name="process" value="edit">
                        <input type="hidden" name="outgoing_id" id="outgoing_id" value="<?php echo $header[0]['outgoing_id']; ?>">
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Tanggal Berangkat</label>
                              <input type="text" class="form-control" name="start_date" id="start_date" value="<?php echo view_datetime($header[0]['start_date']); ?>" data-validate="required" placeholder="Start Date"  readonly >
                            </div>             
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Transporter</label>
                              <input type="text" class="form-control" name="transporter" id="transporter" value="<?php echo $header[0]['employee_name']; ?>"  data-validate="required" placeholder="Transporter" readonly>
                              <input type="hidden" class="form-control" name="employee_id" id="employee_id" value="<?php echo $header[0]['employee_id']; ?>" data-validate="required" >
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Rute</label>
                              <select class="select2"  id="rute" name="rute">
                                <option value="">Select an option</option>
                                <?php foreach ($rute as $val): 
                                        $selected = ($header[0]['rute_id'] == $val['rute_id']) ? "selected":"";
                                        echo "<option value='$val[rute_id]' $selected> $val[rute_id]</option>";
                                      endforeach ?>
                              </select>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Kode Mobil</label>
                              <select class="select2"  id="rute" name="plat_number">
                                <option value="">Select an option</option>
                                <?php foreach ($vehicle as $val): 
                                        $selected = ($header[0]['plat_number'] == $val['vehicle_code']) ? "selected":"";
                                        echo "<option value='$val[vehicle_code]' $selected> $val[vehicle_code]</option>";
                                      endforeach ?>
                              </select>
                            </div>             
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                                <label>KM Awal</label>
                                <input type="text" class="form-control" name="km" id="km" value="<?php echo $header[0]['start_km']; ?>" data-validate="required" >
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                                <label>RIT</label>
                                <input type="text" class="form-control" name="rit"  value="<?php echo $header[0]['rit']; ?>" data-validate="required" >
                            </div>             
                          </div>
                        </div>
                        <input type="hidden" name="" id="standby" value="<?php echo $standby[0]['type_outgoing']; ?>">
                        <input type="hidden" name="" id="standby_location" value="<?php echo $standby[0]['location']; ?>">
                        <hr>
                        <br>
                        <div class="row">
                          <div class="col-md-12"> 
                            <div class="text-center">
                              <button type="button" class="btn btn-info" id="in" onclick="modal_outgoing('IN')">&nbsp;&nbsp;&nbsp;<i class="fa fa-sign-in"></i> &nbsp;IN &nbsp;&nbsp;&nbsp;</button>
                              <button type="button" class="btn btn-warning" id="out" onclick="modal_outgoing('OUT')"><i class="fa fa-sign-out"></i> OUT</button>
                              <button type="button" class="btn btn-orange" id="finish" onclick="modal_outgoing('FINISH')"><i class="fa fa-flag"></i> FINISH</button>
                            </div>
                          </div>
                        </div>
                        <br>
                        <div class="row view-history">
                          
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('../../include/footer'); ?>
<script src="<?php echo base_url('application/modules/Outgoing/views/outgoing.js')?>"></script> 