jQuery(document).ready(function($)
{
    refresh_data();
    history_outgoing($("#outgoing_id").val());

    $("#save").click(function(){
        if($(".validate").valid()){
            $("#form1").submit();
        }
    });

    $("#form1").submit(function(e){
        e.preventDefault();
        $.ajax({
            url : site_url + active_controller + '/save',
            type : 'POST',
            data : new FormData(this),
            processData : false, 
            contentType : false, 
            beforeSend: function(){
                $('#save').loading('start');
            },
            success: function(response){
                res = JSON.parse(response);
                if(res.message == 'success'){
                    swal({
                            title: "Success",
                            text: "Thanks You.",
                            type: "success",
                            showCancelButton: true,
                            showCancelButton: false
                        },
                        function(isConfirm) {
                            window.location = site_url + active_controller + "/edit/?id="+res.id;
                    });
                }else{
                    swal("Failed !", "Your proccess is failed \n"+res.error, "error");
                }
                $('#save').loading('stop');      
            },
            error: function(){
              $('#save').loading('stop');      
              swal("Error !", "Your proccess is error", "error");

            }
        });
    });

    
    $('input.icheck-1').iCheck({
        checkboxClass: 'icheckbox_minimal-orange',
        radioClass: 'iradio_minimal-orange'
    });

    $('input.icheck-2').iCheck({
        checkboxClass: 'icheckbox_minimal-blue',
        radioClass: 'iradio_minimal-blue'
    });

    $('input.icheck-3').iCheck({
        checkboxClass: 'icheckbox_minimal-green',
        radioClass: 'iradio_minimal-green'
    });

    $('input.icheck-4').iCheck({
        checkboxClass: 'icheckbox_minimal-yellow',
        radioClass: 'iradio_minimal-yellow'
    });

    $('input.icheck-5').iCheck({
        checkboxClass: 'icheckbox_minimal-red',
        radioClass: 'iradio_minimal-red'
    });

    $('input.icheck-6').iCheck({
        checkboxClass: 'icheckbox_minimal-purple',
        radioClass: 'iradio_minimal-purple'
    });

    $('input.icheck-7').iCheck({
        checkboxClass: 'icheckbox_minimal-grey',
        radioClass: 'iradio_minimal-grey'
    });
    
    $(".just-numeric").on("keypress keyup blur",function (event) {    
       $(this).val($(this).val().replace(/[^\d].+/, ""));
        if ((event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });

    hide_show_in_out();
   
});

function refresh_data(){

    var responsiveHelper;
    var breakpointDefinition = {
        tablet: 1024,
        phone : 480
    };
    var tableContainer;

    tableContainer = $("#table-1");
    param1 = "";
    param2 = "";

    param ="";
    param += "?param1="+param1
    param += "&param2="+param2;

    tableContainer.dataTable({
        "sPaginationType": "bootstrap",
        "aLengthMenu": [10, 25, 50, 100,"All"],
        "aaSorting": [[10, 'asc']],
        "bDestroy": true,
        "bStateSave": true,
        "bProcessing": true,
        "bServerSide": true,
        "sServerMethod": "GET",
        "sAjaxSource": site_url+active_controller+"/get_data"+param,
        "iDisplayLength": 10,

        // Responsive Settings
        "bAutoWidth"     : false,
        fnPreDrawCallback: function () {
            // Initialize the responsive datatables helper once.
            if (!responsiveHelper) {
                responsiveHelper = new ResponsiveDatatablesHelper(tableContainer, breakpointDefinition);
            }
        },
        fnRowCallback  : function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            responsiveHelper.createExpandIcon(nRow);
        },
        fnDrawCallback : function (oSettings) {
            responsiveHelper.respond();
        }
    });

    $(".dataTables_wrapper select").select2({
        minimumResultsForSearch: -1
    });
}

function del(id,nama){
    swal({
        title: "Delete "+nama+" ?",
        text: "Click Delete. ",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Yes, Delete !",
        cancelButtonText: "No, Cancel !",
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
        closeOnCancel: false
    },
    function(isConfirm) {
        if (isConfirm) {
            $.ajax({
                  url : site_url + active_controller + "/delete?id="+id,
                  success : function(response){
                    var res = JSON.parse(response);
                    if(res.message == 'success'){
                       swal({
                                title: "Success",
                                text: "Deleted, Thanks You.",
                                type: "success",
                                showCancelButton: true,
                                showCancelButton: false
                            },
                            function(isConfirm) {
                                refresh_data();
                                // location.reload();
                        });
                    }else{
                      swal("Gagal !", "Data gagal reject", "error");
                    }
                  },
                  error : function(){
                    swal("Error !", "Failed", "error");
                  }
              });
        } else {
            swal("Cancelled", "Your data is safe.", "error");
        }
    });
}

function detail(id){
    $.ajax({
          url : site_url + active_controller + "/detail/?id="+id,
          beforeSend : function(){
              loader = "<center><img src='"+base_url+"assets/neon/assets/images/Ripple-1s-200px.gif' width='50'></center>";
              $(".view-detail").html(loader);
          },
          success : function(response){
               $(".view-detail").html(response);
          },
          error : function(){
            swal("Error !", "Failed", "error");
            $(".view-detail").html("");
          }
    });
}

function history_outgoing(id){
    $.ajax({
          url : site_url + active_controller + "/history/?id="+id,
          beforeSend : function(){
              loader = "<center><img src='"+base_url+"assets/neon/assets/images/Ripple-1s-200px.gif' width='50'></center>";
              $(".view-history").html(loader);
          },
          success : function(response){
               $(".view-history").html(response);
          },
          error : function(){
            swal("Error !", "Failed", "error");
            $(".view-history").html("");
          }
    });
}

function hide_show_in_out(){
    standby = $('#standby').val();
    if(standby == 'IN'){
        $('#in').hide();
        $('#out').show();
        $('#finish').hide();
    }else{
        $('#out').hide();
        $('#in').show();
        $('#finish').show();
    }
}

function modal_outgoing(type){
    $('#modal-6').modal({
        backdrop: 'static',
        keyboard: false
    })
    $("#modal-6 .modal-header .modal-title").html(type);
    outgoing_id = $("#outgoing_id").val();
    standby = $("#standby").val();
    standby_location = $("#standby_location").val();
    $.ajax({
        url : site_url + active_controller + "/in",
        type : 'POST',
        data : {outgoing_id : outgoing_id, type : type, standby : standby, standby_location : standby_location},
        success : function(response){
            $("#modal-6 .modal-body").html(response);
        },
        error : function(){
            swal("Error !", "Failed", "error");
        }
    });
}

function view_photo(image){
    $('#modal-6').modal({
        backdrop: 'static',
        keyboard: false
    }) 
    $("#modal-6 .modal-header .modal-title").html('Foto');
    body = '<center><img src="'+base_url + "assets/uploads/"+image+'" style="width0%;" class="thumbnail"></center>';
    $("#modal-6 .modal-body").html(body);
}

function previewImage() {
    document.getElementById("image-preview").style.display = "block";
    var oFReader = new FileReader();
     oFReader.readAsDataURL(document.getElementById("image-source").files[0]);
 
    oFReader.onload = function(oFREvent) {
      document.getElementById("image-preview").src = oFREvent.target.result;
    };
};
