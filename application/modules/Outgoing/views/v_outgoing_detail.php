<div class="col-md-7">
    <h4>TRACKING DETAILS</h4>
    <div class="panel panel-info">
        <div class="panel-body">
            <div class="row">
                <div class="col-md-4">
                    <b>NO. PERJALANAN</b><br>
                    <?php echo $header[0]['outgoing_id']; ?>  
                </div>
                <div class="col-md-4">
                    <b>START DATE</b><br>
                    <?php echo view_datetime($header[0]['start_date']); ?>  
                </div>
                <!-- <div class="col-md-4">
                    <b>DESTINATION</b><br>
                    <?php echo $header[0]['destination']; ?>  
                </div> -->
                <div class="col-md-4">
                    <b>RUTE</b><br>
                    <?php echo $header[0]['rute_id']; ?>  
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-4">
                    <b>TRANSPORTER</b><br>
                    <?php echo $header[0]['employee_name']; ?>
                </div>
                <div class="col-md-4">
                    <b>KM AWAL</b><br>
                    <?php echo $header[0]['start_km']; ?>
                </div>
                <div class="col-md-4">
                    <b>KM AKHIR</b><br>
                    <?php echo $header[0]['end_km']; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-md-5">
    <h4>HISTORY</h4>
    <div class="panel panel-info">
        <div class="panel-body">
            <div class="timeline">
                <?php
                    $start_date_temp = ""; 
                    foreach ($detail as $key => $val) { 
                        $day ="";
                        if($start_date_temp != $val['dp_date'])
                        {
                            $day = "<h3>".nama_hari($val['dp_date'])."</h3>
                                    <p>".view_date($val['dp_date'])."</p>";
                        }
                ?>
                  <div class="entry">
                    <div class="title">
                        <?php echo $day; ?>
                    </div>
                    <div class="body">
                        <div onclick="view_photo('<?php echo $val['images']; ?>')">
                            <h4><?php echo view_time($val['dp_time']); ?></h4> 
                            <p><?php echo $val['type_outgoing']." ".$val['location'].", km : ".$val['km'];  ?></p>
                            <p><i class="fa fa-picture-o"></i></p>
                        </div>
                    </div>
                  </div>
                <?php 
                        $start_date_temp = $val['dp_date'];
                    } 
                ?>
            </div>
        </div>
    </div>
</div>