<?php $this->load->view('../../include/header'); ?>
<!-- <?php dump($list_parent); ?> -->
<div class="container-fluid">
  <?php echo breadcrumb($this->uri->segment(1),$this->uri->segment(2)); ?>
    <div class="row">
        <div class="col-md-12">
          <h2>Add Keberangkatan</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <button type="button" class="btn btn-primary pull-right" onclick="move_page('<?php echo site_url('Outgoing'); ?>')">
                <i class="fa fa-arrow-left"></i>
                Back
            </button>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-info">
                <div class="panel-body">
                  <div class="row">
                    <div class="col-md-12">
                      <form role="form" id="form1" method="post" class="validate" enctype="multipart/form-data">
                        <input type="hidden" name="process" value="add">
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Tanggal Berangkat</label>
                              <input type="text" class="form-control" name="start_date" id="start_date" value="<?php echo date('d/m/Y H:i'); ?>" data-validate="required" placeholder="Start Date"  readonly >
                            </div>             
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Transporter</label>
                              <input type="text" class="form-control" name="transporter" id="transporter" value="<?php echo $this->session->userdata('ses_fullname');?>"  data-validate="required" placeholder="Transporter" readonly>
                              <input type="hidden" class="form-control" name="employee_id" id="employee_id" value="<?php echo $this->session->userdata('ses_employee_id');?>" data-validate="required" >
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Rute</label>
                              <select class="select2"  id="rute" name="rute"  data-validate="required" >
                                <option value="">Select an option</option>
                                <?php foreach ($rute as $val): 
                                        echo "<option value='$val[rute_id]'> $val[rute_id]</option>";
                                      endforeach ?>
                              </select>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Kode Mobil</label>
                              <select class="select2"  id="vehicle_code" name="vehicle_code"  data-validate="required" >
                                <option value="">Select an option</option>
                                <?php foreach ($vehicle as $val): 
                                        echo "<option value='$val[vehicle_code]'> $val[vehicle_code]</option>";
                                      endforeach ?>
                              </select>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>KM Awal</label>
                              <input type="text" class="form-control just-numeric" name="start_km"  data-validate="required" >
                            </div>   
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>RIT</label>
                              <select class="form-control" name="rit" id="rit" data-validate="required">
                                  <?php 
                                      for($i=1;$i<=4;$i++)
                                      {
                                          echo "<option value='".$i."'>".$i."</option>";
                                      }
                                  ?>
                              </select>
                            </div>             
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <input type="file" class="form-control" name="photo" accept="image/*" capture="camera" id="image-source" onchange="previewImage()" data-validate="required" >
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-12">
                            <div class="form-group">
                              <img id="image-preview" alt="image preview" width="100" style="display:none;width:100%">
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-12"> 
                            <div class="text-center">
                              <button type="button" class="btn btn-success" id="save" ><i class="fa fa-save"></i> Save</button>
                            </div>
                          </div>
                        </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('../../include/footer'); ?>
<script src="<?php echo base_url('application/modules/Outgoing/views/outgoing.js')?>"></script> 