<?php 
defined('BASEPATH') OR exit('No direct script access allowed'); 
 
class Outgoing extends CI_Controller { 
 
	public function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('isLogin')){
			redirect('Login');
		}
		$this->load->model('M_outgoing','outgoing_model');
	}
 
	public function index()
	{
		$path = $this->uri->segment(1);
		$access_menu = access_menu($path);
		if($access_menu['read'] == 'Y')
		{
			$data['access_menu'] = $access_menu;
			history('view outgoing');
			$this->load->view('v_outgoing',$data);
		}
		else
		{
			history('view outgoing :denied');
			$this->load->view('../../include/access_denied');
		}
	}

	public function get_data() 
	{
 		$path = $this->uri->segment(1);
		$access_menu = access_menu($path);
		$ses_groupid = $this->session->userdata('ses_groupid');
		$ses_username = $this->session->userdata('ses_username');

		$active_controller = $this->uri->segment(1);
		/* Array of database columns which should be read and sent back to DataTables. Use a space where
			         * you want to insert a non-database field (for example a counter or static image)
		*/

		$acolumns = array('outgoing_header.outgoing_id', 'outgoing_header.start_date', 'outgoing_header.end_date', 'outgoing_header.rute_id','outgoing_header.destination', 'outgoing_header.plat_number', 'outgoing_header.rit', 'outgoing_header.static','outgoing_header.start_km','outgoing_header.end_km','outgoing_header.status', 'outgoing_header.employee_id', 'outgoing_header.note', 'outgoing_header.created_by','outgoing_header.status', 'employee.employee_nik', 'employee_name');

		// DB table to use
		// $stable = 'outgoing_header';
		//

		$idisplay_start = $this->input->get_post('iDisplayStart', true);
		$idisplay_length = $this->input->get_post('iDisplayLength', true);
		$isort_col_0 = $this->input->get_post('iSortCol_0', true);
		$isorting_cols = $this->input->get_post('iSortingCols', true);
		$isearch = $this->input->get_post('sSearch', true);
		$secho = $this->input->get_post('sEcho', true);

		// Paging
		if (isset($idisplay_start) && $idisplay_length != '-1') {
			$this->db->limit($this->db->escape_str($idisplay_length), $this->db->escape_str($idisplay_start));
		}

		// Ordering
		if (isset($isort_col_0)) {
			for ($i = 0; $i < intval($isorting_cols); $i++) {
				$isort_col = $this->input->get_post('iSortCol_' . $i, true);
				$bsortable = $this->input->get_post('bSortable_' . intval($isort_col), true);
				$ssort_dir = $this->input->get_post('sSortDir_' . $i, true);

				if ($bsortable == 'true') {
					$this->db->order_by($acolumns[intval($this->db->escape_str($isort_col))], $this->db->escape_str($ssort_dir));
				}
			}
		}

		/*
			         * Filtering
			         * NOTE this does not match the built-in DataTables filtering which does it
			         * word by word on any field. It's possible to do here, but concerned about efficiency
			         * on very large tables, and MySQL's regex functionality is very limited
		*/
		if (isset($isearch) && !empty($isearch)) {
			for ($i = 0; $i < count($acolumns); $i++) {
				$isearchable = $this->input->get_post('bSearchable_' . $i, true);

				// Individual column filtering
				if (isset($isearchable) && $isearchable == 'true') {
					$this->db->or_like($acolumns[$i], $this->db->escape_like_str($isearch));
				}
			}
		}

		// Select Data
		$this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $acolumns)), false);
		$this->db->from('outgoing_header');
		$this->db->join('employee','outgoing_header.employee_id = employee.employee_id');
		if($ses_groupid != 1)
		{
			$this->db->where('outgoing_header.created_by',$ses_username);
		}
		$rresult = $this->db->get(); 

		// Data set length after filtering
		$this->db->select('FOUND_ROWS() AS found_rows');
		$ifiltered_total = $this->db->get()->row()->found_rows;

		// Total data set length
		$this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $acolumns)), false);
		$this->db->from('outgoing_header');
		$this->db->join('employee','outgoing_header.employee_id = employee.employee_id');
		if($ses_groupid != 1)
		{
			$this->db->where('outgoing_header.created_by',$ses_username);
		}
		$itotal = $this->db->count_all_results();

		// Output
		$output = array(
			'sEcho' => intval($secho),
			'iTotalRecords' => $itotal,
			'iTotalDisplayRecords' => $ifiltered_total,
			'aaData' => array(),
		);

		$no = $idisplay_start;
		foreach ($rresult->result_array() as $arow) 
		{
			$status = ($arow['status'] == 'ONGOING') ? '<div class="badge badge-warning badge-roundless">ONGOING</div>' : '<div class="badge badge-default badge-roundless">FINISH</div>';
			$no++;
			$row = array();
			$row[] = "<center onclick='detail(\"".$arow['outgoing_id']."\")'>".$no.".</center>";
			$row[] = "<center onclick='detail(\"".$arow['outgoing_id']."\")'>".view_date($arow['start_date'])."</center>";
			$row[] = "<div onclick='detail(\"".$arow['outgoing_id']."\")'>".$arow['outgoing_id']."</div>";
			$row[] = "<div onclick='detail(\"".$arow['outgoing_id']."\")'>".$arow['rute_id']."</div>";
			$row[] = "<div onclick='detail(\"".$arow['outgoing_id']."\")'>".$arow['plat_number']."</div>";
			$row[] = "<div onclick='detail(\"".$arow['outgoing_id']."\")'>".$arow['employee_name']."</div>";
			$row[] = "<center onclick='detail(\"".$arow['outgoing_id']."\")'>".view_time($arow['start_date'])."</center>";
			$row[] = "<center onclick='detail(\"".$arow['outgoing_id']."\")'>".$arow['start_km']."</center>";
			$row[] = "<center onclick='detail(\"".$arow['outgoing_id']."\")'>".$arow['end_km']."</center>";
			$row[] = "<center onclick='detail(\"".$arow['outgoing_id']."\")'>".$status."</center>";
			/*
			$action ="";
 			$action .="<center>";
 				if($access_menu['update'] == 'Y' && $arow['status'] =='ONGOING')
 				{
		            $action .= "<button type='button' class='btn btn-info btn-xs' onclick='move_page(\"" . site_url($active_controller . '/edit/?id=' . encrypt($arow['outgoing_id'])) . "\")'><i class='fa fa-pencil-square-o'></i></button> ";
 				}
 				if($access_menu['delete']  == 'Y' && $arow['status'] =='ONGOING')
 				{
	                $action .= "<button class='btn btn-xs btn-danger' onClick='del(\"" . $arow['employee_id'] . "\",\"" . $arow['employee_name'] . "\")'><i class='fa fa-trash-o'></i></button>";
 				}
            $action .= "<center>";
			$row[] = $action;
			*/

			$output['aaData'][] = $row;
		}
		// dd( $output['aaData']);

		history('view data vehicle ?search=' . $isearch);
		echo json_encode($output);
	}

	public function detail() 
	{
		$id = $this->input->get('id');
		$data['header'] = $this->outgoing_model->get_haeder_outgoing($id)->result_array();
		$data['detail'] = $this->outgoing_model->get_detail_outgoing($id)->result_array();

		history('view outgoing detail');
		$this->load->view('v_outgoing_detail',$data);
	}

	public function add() 
	{
		$ses_employee_id = $this->session->userdata('ses_employee_id');
		$check_ongoing = $this->outgoing_model->check_ongoing($ses_employee_id)->result_array();
		if(empty($check_ongoing))
		{
			$data['rute'] = $this->outgoing_model->get_rute()->result_array();
			$data['destination'] = $this->outgoing_model->get_destination()->result_array();
			$data['vehicle'] = $this->outgoing_model->get_vehicle()->result_array();
			history('add outgoing');
			$this->load->view("v_outgoing_add",$data);	
		}
		else
		{
			history('edit outgoing ?id='.$id);
			redirect('Outgoing/edit/?id='.encrypt($check_ongoing[0]['outgoing_id']));
		}
	}

	public function save()
	{
		$ses_employee_id = $this->session->userdata('ses_employee_id');
		$outgoing_id = $this->outgoing_model->get_outgoing_id($ses_employee_id);
		$arr_start_date = explode(" ", $this->input->post('start_date'));
		$start_date = save_date($arr_start_date[0]);
		$start_time = $arr_start_date[1];
		$start_datetime = $start_date." ".$start_time;
		$transporter = $this->input->post('transporter');
		$employee_id = $this->input->post('employee_id');
		$rute = $this->input->post('rute');
		$destination = $this->input->post('destination');
		$vehicle_code = $this->input->post('vehicle_code');
		$start_km = $this->input->post('start_km');
		$rit = $this->input->post('rit');

		$data['outgoing_id'] = $outgoing_id;
		$data['start_date'] = $start_datetime;
		$data['rute_id'] = $rute;
		$data['destination'] = $destination;
		$data['plat_number'] = $vehicle_code;
		$data['start_km'] = $start_km;
		$data['rit'] = $rit;
		$data['employee_id'] = $employee_id;
		$data['status'] = 'ONGOING';
		$data['created_by'] = $this->session->userdata('ses_username');
		$data['created_date'] = date('Y-m-d H:i:s');
		$config['upload_path']          = './assets/uploads/';
        $config['allowed_types']        = 'jpg|png';
        $config['file_name']            = $ses_employee_id.date('Ymdhis').".jpg";
        // $config['max_size']             = 100;
        // $config['max_width']            = 1024;
        // $config['max_height']           = 768;

        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if(!$this->upload->do_upload('photo'))
        {
                $error = array('error' => $this->upload->display_errors());
                echo json_encode($error);
        }
        else
        {
                $data_upload = array('upload_data' => $this->upload->data());
                $file_name = $data_upload['upload_data']['file_name'];

				$insert_header_outgoing = $this->outgoing_model->insert_header_outgoing($data);
                if($insert_header_outgoing == 'success')
				{
		            $data_detail['outgoing_id'] = $outgoing_id;
		            $data_detail['type_outgoing'] = 'OUT';
		            $data_detail['location'] = 'START';
		            $data_detail['dp_date'] = $start_date;
		            $data_detail['dp_time'] = $start_time;
		            $data_detail['km'] = $start_km;
		            $data_detail['images'] = $file_name;
		            $insert_detail_outgoing = $this->outgoing_model->insert_detail_outgoing($data_detail);
		            $id = encrypt($outgoing_id);
		            $result = array('message'=>$insert_detail_outgoing,'id'=>$id);
				}
				else
				{
					$result = array('message'=>'failed');
				}
				echo json_encode($result);
        }
	}

	public function save_detail()
	{
		$ses_employee_id = $this->session->userdata('ses_employee_id');
		$type = $this->input->post('type');
		$outgoing_id = $this->input->post('outgoing_id');
		$dp_date = save_date($this->input->post('dp_date'));
		$dp_time = $this->input->post('dp_time');
		$location = strtoupper($this->input->post('location'));
		$km = $this->input->post('km');

		$config['upload_path']          = './assets/uploads/';
        $config['allowed_types']        = 'jpg|png';
        $config['file_name']            = $ses_employee_id.date('Ymdhis').".jpg";
        // $config['max_size']             = 100;
        // $config['max_width']            = 1024;
        // $config['max_height']           = 768;

        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if(!$this->upload->do_upload('photo'))
        {
                $error = array('error' => $this->upload->display_errors());
                echo json_encode($error);
        }
        else
        {
                $data = array('upload_data' => $this->upload->data());
                $file_name = $data['upload_data']['file_name'];

                $data_detail['outgoing_id'] = $outgoing_id;
                $data_detail['type_outgoing'] = $type;
                $data_detail['location'] = $location;
                $data_detail['dp_date'] = $dp_date;
                $data_detail['dp_time'] = $dp_time;
                $data_detail['km'] = $km;
                $data_detail['images'] = $file_name;

                if($type == 'FINISH')
                {	
                	$where = array('outgoing_id'=>$outgoing_id);
                	$data_update = array('end_date'=>$dp_date." ".$dp_time,'end_km'=>$km,'status'=>'FINISH');
                	$this->outgoing_model->update_header_outgoing($data_update,$where);
                }
               	$insert_detail_outgoing = $this->outgoing_model->insert_detail_outgoing($data_detail);
	            $result = array('message'=>$insert_detail_outgoing,'id'=>$outgoing_id);
             	echo json_encode($result);
        }
	}
 	
 	public function edit()
 	{
 		$id = decrypt($this->input->get('id'));
 		$data['header'] = $this->outgoing_model->get_haeder_outgoing($id)->result_array();
 		$data['detail'] = $this->outgoing_model->get_detail_outgoing($id)->result_array();
 		$data['standby'] = $this->outgoing_model->get_standby_outgoing($id)->result_array();
 		$data['rute'] = $this->outgoing_model->get_rute()->result_array();
		$data['destination'] = $this->outgoing_model->get_destination()->result_array();
		$data['vehicle'] = $this->outgoing_model->get_vehicle()->result_array();
 		history('edit outgoing ?id='.$id);
 		$this->load->view('v_outgoing_edit',$data);
 	}

 	public function history()
 	{
 		$id = $this->input->get('id');
 		$data['detail'] = $this->outgoing_model->get_detail_outgoing($id)->result_array();
 		history('view history outgoing ?id='.$id);
 		$this->load->view('v_outgoing_history',$data);
 	}

 	public function get_date()
 	{
 		$date = date('d/m/Y');
 		$time = date('H:i');
 		echo json_encode(array('date'=>$date,'time'=>$time));
 	}

 	public function in()
 	{
 		$standby = $this->input->post('standby');
 		if($standby == 'OUT')
 		{
 			$standby_next = 'IN';
 		}
 		else
 		{
 			$standby_next = 'OUT';	
 		}
 		$standby_location = $this->input->post('standby_location');
 		if($standby_location != 'START')
 		{
 			if($standby_next == 'OUT')
 			{
 				$standby_location_next = $standby_location;
 			}
 			elseif($this->input->post('type') == 'FINISH')
 			{
 				$standby_location_next = "END";
 			}
 			else
 			{
 				$dp_counter = substr($standby_location, -1,1) + 1; 
 				$standby_location_next = "DP".$dp_counter;
 			}
 		}
 		else
 		{
 			$standby_location_next = 'DP1';
 		}
 		$data['outgoing_id'] = $this->input->post('outgoing_id');
 		$data['standby_next'] = $standby_next;
 		$data['standby_location_next'] = $standby_location_next;
 		$data['type'] = $this->input->post('type');
 		$data['date'] = date('d/m/Y');
 		$data['time'] = date('H:i');
 		history('add detail outgoing ?id='.$data['outgoing_id']);
 		$this->load->view('v_outgoing_in',$data);
 	}

}
 
/* End of file Outgoing.php */
/* Location: ./application/modules/Outgoing/controllers/Outgoing.php */
