<?php 
defined('BASEPATH') OR exit('No direct script access allowed'); 
 
class M_outgoing extends CI_Model { 
 
	public function get_haeder_outgoing($id)
	{
        $this->db->select('*');
        $this->db->from('outgoing_header');
        $this->db->join('employee','outgoing_header.employee_id = employee.employee_id');
        $this->db->where('outgoing_header.outgoing_id',$id);
        return $this->db->get();
	}
    
    public function get_detail_outgoing($id)
    {   
        $this->db->select('*');
        $this->db->from('outgoing_detail');
        $this->db->where('outgoing_id',$id);
        $this->db->order_by('outgoing_detail_id','ASC');
        return $this->db->get();
    }

    public function get_rute()
    {
        return $this->db->get('rute');
    }

    public function get_destination()
    {
        return $this->db->get('destination');
    }

    public function get_vehicle()
    {
        return $this->db->get('vehicle');
    }

    public function check_ongoing($ses_employee_id)
    {
        return $this->db->get_where('outgoing_header',array('status'=>'ONGOING','employee_id'=>$ses_employee_id));
    }

    public function insert_header_outgoing($data)
    {
        if($this->db->insert('outgoing_header',$data))
        {
            $result = 'success';
        }
        else
        {
            $result = 'failed';
        }
        return $result;
    }

    public function insert_detail_outgoing($data)
    {
        if($this->db->insert('outgoing_detail',$data))
        {
            $result = 'success';
        }
        else
        {
            $result = 'failed';
        }
        return $result;
    }

    public function update_header_outgoing($data,$where)
    {
        if($this->db->update('outgoing_header', $data, $where))
        {
            $result = 'success';
        }
        else
        {
            $result = 'failed';
        }
        return $result;
    }

    public function get_outgoing_id($ses_employee_id)
    {
        $query = "SELECT outgoing_id,MAX(SUBSTR(outgoing_id,11,3)) AS counter FROM outgoing_header WHERE DATE(created_date) = DATE(NOW()) ";
        $exc = $this->db->query($query)->row();
        $date_code = date('ymd');
        if(!empty($exc))
        {   
            $counter_plus = intval($exc->counter) + 1; 

            if($counter_plus < 10)
            {
                $counter = '00'.$counter_plus;
            }
            elseif($counter_plus >= 10  && $counter_plus < 100)
            {
                $counter = '0'.$counter_plus;
            }
            else
            {
                $counter = $counter_plus;
            }
            $result = $date_code.$ses_employee_id.$counter;
        }
        else
        {
           $result = $date_code.$ses_employee_id.'001'; 
        }

        return $result;
    }

    public function get_standby_outgoing($id)
    {
        $this->db->select('type_outgoing,location');
        $this->db->from('outgoing_detail');
        $this->db->where('outgoing_id',$id);
        $this->db->order_by('outgoing_detail_id','DESC');
        $this->db->limit(1);
        return $this->db->get();
    }
}
 
/* End of file M_outgoing.php */
/* Location: ./application/modules/Outgoing/models/M_outgoing.php */
