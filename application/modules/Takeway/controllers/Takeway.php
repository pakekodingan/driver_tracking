<?php 
defined('BASEPATH') OR exit('No direct script access allowed'); 
 
class Takeway extends CI_Controller { 
 
	public function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('isLogin')){
			redirect('Login');
		}
		$this->load->model('M_takeway','takeway_model');
	}
 
	public function index()
	{
		$this->load->view('v_takeway');
	}
 
}
 
/* End of file Takeway.php */
/* Location: ./application/modules/Takeway/controllers/Takeway.php */
