<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_dashboard extends CI_Model {

	public function ongoing($ses_employee_id)
	{
		$this->db->select('*');
        $this->db->from('outgoing_header');
        $this->db->where('status','ONGOING');
        $this->db->where('employee_id',$ses_employee_id);
        $total = $this->db->count_all_results();
        return $total;
	}

    public function total_perjalanan_saya($ses_employee_id)
    {
        $this->db->select('*');
        $this->db->from('outgoing_header');
        $this->db->where('employee_id',$ses_employee_id);
        $total = $this->db->count_all_results();
        return $total;
    }

    public function total_ongoing()
    {
       $this->db->select('*');
        $this->db->from('outgoing_header');
        $this->db->where('status','ONGOING');
        $total = $this->db->count_all_results();
        return $total;
    }

    public function online_user()
    {
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('online','Y');
        $total = $this->db->count_all_results();
        return $total;
    }
}

/* End of file m_dashboard.php */
/* Location: ./application/modules/dashboard/models/m_dashboard.php */