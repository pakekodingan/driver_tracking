<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('isLogin')){
			redirect('Login');
		}
		$this->load->model('M_dashboard','dashboard_model');
	}

	public function index()
	{
		$ses_employee_id = $this->session->userdata('ses_employee_id');
		$data['ongoing'] = $this->dashboard_model->ongoing($ses_employee_id);
		$data['total_perjalanan_saya'] = $this->dashboard_model->total_perjalanan_saya($ses_employee_id);
		$data['total_ongoing'] = $this->dashboard_model->total_ongoing();
		$data['online_user'] = $this->dashboard_model->online_user();
		history('view dashboard');
		$this->load->view('v_dashboard', $data);
	}
}


/* End of file dashboard.php */
/* Location: ./application/modules/dashboard/controllers/dashboard.php */
