<?php 
defined('BASEPATH') OR exit('No direct script access allowed'); 
 
class M_group extends CI_Model { 
 
	public function group_insert($data)
    {
        $insert = $this->db->insert('group',$data);
        $result = ($insert == true)?1:0;
        return $result;
    }

    public function check_name($name)
    {
        $check = $this->db->get_where('group',array('group_name'=>$name));
        $result = ($check->num_rows() > 0)?1:0;
        return $result;
    }
    
    public function check_name_for_edit($id,$name)
    {
        $get = $this->db->get_where('group',array('group_id'=>$id))->row();
        $name_before = $get->group_name;
        if($name != $name_before)
        {
            $check = $this->db->get_where('group',array('group_name'=>$name));
            $result = ($check->num_rows() > 0)?1:0;
            
        }
        else
        {
            $result = 0;
        }

        return $result;
    }

    public function group_delete($id)
    {
        $delete = $this->db->delete('group',array('group_id'=>$id));
        $result = ($delete == true)?1:0;
        return $result;
    }

    public function group_by_id($id)
    {   
        $get = $this->db->get_where('group',array('group_id'=>$id));
        return $get;
    }

    public function group_update($data,$where = array())
    {
        $update = $this->db->update('group',$data,$where);

        $result = ($update == true)?1:0;
        return $result;
    }

    public function get_menu_parent()
    {
        return $this->db->get_where('menu',array('parent_id'=>'0'));
    }

    public function get_menu_child()
    {
        return $this->db->get_where('menu',array('parent_id <>'=>'0'));
    }

    public function get_group_menu($id)
    {
        return $this->db->get_where('group_menu',array('group_id'=>$id));
    }

    public function group_menu_delete($id)
    {
        $delete = $this->db->delete('group_menu',array('group_id'=>$id));
        $result = ($delete == true)?1:0;
        return $result;
    }

    public function group_menu_insert($data)
    {
        $insert = $this->db->insert('group_menu',$data);
        $result = ($insert == true)?1:0;
        return $result;
    }
}
 
/* End of file m_group.php */
/* Location: ./application/modules/group/models/m_group.php */
