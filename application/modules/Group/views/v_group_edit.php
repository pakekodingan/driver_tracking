<?php $this->load->view('../../include/header'); ?>
<div class="container-fluid">
  <?php echo breadcrumb($this->uri->segment(1),$this->uri->segment(2)); ?>
    <div class="row">
        <div class="col-md-12">
          <h2>Form Edit Group</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <button type="button" class="btn btn-primary pull-right" onclick="goBack()">
                <i class="fa fa-arrow-left"></i>
                Back
            </button>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-info">
                <div class="panel-body">
                  <div class="row">
                    <div class="col-md-12">
                      <form role="form" id="form1" method="post" class="validate">
                        <input type="hidden" name="id" value="<?php echo $data_group_by_id['group_id']; ?>">
                        <input type="hidden" name="process" value="edit">
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Name</label>
                              <input type="text" class="form-control" name="name" id="name" data-validate="required" placeholder="Name menu" value="<?php echo $data_group_by_id['group_name'] ?>">
                            </div>             
                          </div>
                          <div class="col-md-2">
                            <div class="form-group">
                              <label for="minimal-checkbox-1-3">Active Group</label><br>
                              <input type="checkbox" class="icheck-3" name="active" id="minimal-checkbox-1-3" <?php if($data_group_by_id['flag_active'] == 'Y'){echo "checked";} ?> >
                            </div>             
                          </div>
                          <div class="col-md-3">
                            <div class="form-group">
                              <label for="minimal-checkbox-1-3">All Access</label><br>
                              <input type="checkbox" class="icheck-2" name="all_access" id="minimal-checkbox-1-3" <?php if($data_group_by_id['flag_all'] == 'Y'){echo "checked";} ?> >
                            </div>             
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-12"> 
                            <div class="text-center">
                              <button type="button" class="btn btn-success" id="save" ><i class="fa fa-save"></i> Save</button>
                            </div>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('../../include/footer'); ?>
<script src="<?php echo base_url('application/modules/Group/views/group.js')?>"></script> 