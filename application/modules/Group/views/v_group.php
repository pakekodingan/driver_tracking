<?php $this->load->view('../../include/header'); ?>
<div class="container-fluid">
  <?php echo breadcrumb($this->uri->segment(1)); ?>
  <div class="row">
    <div class="col-md-12">
      <h1><?php echo menu_name($this->uri->segment(1)); ?></h1>
    </div> 
  </div>
  <div class="row">
    <div class="col-md-12">
        <button type="button" class="btn btn-blue pull-right" onclick="move_page('<?php echo site_url('Group/add'); ?>')">
            <i class="fa fa-plus"></i> 
            Add
        </button>
    </div>  
  </div>
  <br>
  <div class="row">
    <div class="col-md-12">
        <div class="card">
            <table class="table table-condensed table-bordered mtable-hover table-striped " id="table-1" >
                <thead>
                    <tr>
                        <th width="5%">No.</th>
                        <th>Group Name</th>
                        <th data-hide="phone">Access All</th>
                        <th data-hide="phone">Active</th>
                        <th width="30%">Action</th>
                    </tr>
                </thead>
                <tbody></tbody>
                <tfoot></tfoot> 
            </table>
        </div>
    </div>
   </div>
</div>
<?php $this->load->view('../../include/footer'); ?>
<script src='<?php echo base_url('application/modules/Group/views/group.js')?>'></script>
<!-- End of file v_group.php -->
<!-- Location: ./application/modules/Group/views/v_group.php -->
