<?php $this->load->view('../../include/header'); ?>
<div class="container-fluid" >
    <?php echo breadcrumb($this->uri->segment(1),'access menu'); ?>
    <div class="row">
        <div class="col-md-12">
          <h2>Access Menu</h2>
          <p>Group Name : <?php echo $data_group_by_id['group_name']; ?></p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <button type="button" class="btn btn-primary pull-right" onclick="goBack()">
                <i class="fa fa-arrow-left"></i>
                Back
            </button>
        </div>
    </div>
    <br>
    <form role="form" id="form2" method="post">
    <input type="hidden" name="group_id" value="<?php echo $data_group_by_id['group_id']; ?>">
    <div class="row">
        <div class="col-md-12" >
            <table class="table table-condensed table-bordered table-hover table-striped " id="table-2" >
                <thead>
                    <tr>
                        <th width="5%">No.</th>
                        <th>Name</th>
                        <th width="10%"><input type='checkbox' class='icheck-3' id="all-view"> View</th>
                        <th width="10%"><input type='checkbox' class='icheck-2' id="all-create"> Create</th>
                        <th width="10%"><input type='checkbox' class='icheck-1' id="all-read"> Read</th>
                        <th width="10%"><input type='checkbox' class='icheck-4' id="all-update"> Update</th>
                        <th width="10%"><input type='checkbox' class='icheck-5' id="all-delete"> Delete</th>
                        <th width="10%"><input type='checkbox' class='icheck-6' id="all-approve"> Approve</th>
                        <th width="10%"><input type='checkbox' class='icheck-7' id="all-download"> Download</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        if(!empty($data_parent_menu))
                        {   
                            $no=1;
                            foreach ($data_parent_menu as $key => $parent) {

                                $view_parent_checked = '';
                                $view_parent = 'N';
                                if(isset($data_group_menu[$parent['menu_id']]) && $data_group_menu[$parent['menu_id']]['view'] == 'Y')
                                {
                                    $view_parent_checked = 'checked';
                                    $view_parent = 'Y';
                                }

                                $read_parent_checked = '';
                                $read_parent = 'N';
                                if(isset($data_group_menu[$parent['menu_id']]) && $data_group_menu[$parent['menu_id']]['read'] == 'Y')
                                {
                                    $read_parent_checked = 'checked';
                                    $read_parent = 'Y';
                                }

                                $create_parent_checked = '';
                                $create_parent = 'N';
                                if(isset($data_group_menu[$parent['menu_id']]) && $data_group_menu[$parent['menu_id']]['create'] == 'Y')
                                {
                                    $create_parent_checked = 'checked';
                                    $create_parent = 'Y';
                                }

                                $update_parent_checked = '';
                                $update_parent = 'N';
                                if(isset($data_group_menu[$parent['menu_id']]) && $data_group_menu[$parent['menu_id']]['update'] == 'Y')
                                {
                                    $update_parent_checked = 'checked';
                                    $update_parent = 'Y';
                                }

                                $delete_parent_checked = '';
                                $delete_parent = 'N';
                                if(isset($data_group_menu[$parent['menu_id']]) && $data_group_menu[$parent['menu_id']]['delete'] == 'Y')
                                {
                                    $delete_parent_checked = 'checked';
                                    $delete_parent = 'Y';
                                }

                                $approve_parent_checked = '';
                                $approve_parent = 'N';
                                if(isset($data_group_menu[$parent['menu_id']]) && $data_group_menu[$parent['menu_id']]['approve'] == 'Y')
                                {
                                    $approve_parent_checked = 'checked';
                                    $approve_parent = 'Y';
                                }

                                $download_parent_checked = '';
                                $download_parent = 'N';
                                if(isset($data_group_menu[$parent['menu_id']]) && $data_group_menu[$parent['menu_id']]['download'] == 'Y')
                                {
                                    $download_parent_checked = 'checked';
                                    $download_parent = 'Y';
                                }

                                
                                echo "<tr>
                                        <td align='center'>".$no++.".</td>
                                        <td><b>".$parent['name']."</b> <input type='hidden' name='menu_id[]' value='".$parent['menu_id']."'></td>
                                        <td><input type='checkbox' class='icheck-3' ".$view_parent_checked."> <input type='text' style='display:none;' name='view[]' value='".$view_parent."'></td>
                                        <td><input type='checkbox' class='icheck-2' ".$create_parent_checked."> <input type='text' style='display:none;' name='create[]' value='".$create_parent."'></td>
                                        <td><input type='checkbox' class='icheck-1' ".$read_parent_checked."> <input type='text' style='display:none;' name='read[]' value='".$read_parent."'></td>
                                        <td><input type='checkbox' class='icheck-4' ".$update_parent_checked."> <input type='text' style='display:none;' name='update[]' value='".$update_parent."'></td>
                                        <td><input type='checkbox' class='icheck-5' ".$delete_parent_checked."> <input type='text' style='display:none;' name='delete[]' value='".$delete_parent."'></td>
                                        <td><input type='checkbox' class='icheck-6' ".$approve_parent_checked."> <input type='text' style='display:none;' name='approve[]' value='".$approve_parent."'></td>
                                        <td><input type='checkbox' class='icheck-7' ".$download_parent_checked."> <input type='text' style='display:none;' name='download[]' value='".$download_parent."'></td>
                                      </tr>";

                                if(isset($data_child_menu[$parent['menu_id']]))
                                {
                                    foreach ($data_child_menu[$parent['menu_id']] as $key => $child1) {
                                        
                                        $view_child1_checked = '';
                                        $view_child1 = 'N';
                                        if(isset($data_group_menu[$child1['menu_id']]) && $data_group_menu[$child1['menu_id']]['view'] == 'Y')
                                        {
                                            $view_child1_checked = 'checked';
                                            $view_child1 = 'Y';
                                        }

                                        $read_child1_checked = '';
                                        $read_child1 = 'N';
                                        if(isset($data_group_menu[$child1['menu_id']]) && $data_group_menu[$child1['menu_id']]['read'] == 'Y')
                                        {
                                            $read_child1_checked = 'checked';
                                            $read_child1 = 'Y';
                                        }

                                        $create_child1_checked = '';
                                        $create_child1 = 'N';
                                        if(isset($data_group_menu[$child1['menu_id']]) && $data_group_menu[$child1['menu_id']]['create'] == 'Y')
                                        {
                                            $create_child1_checked = 'checked';
                                            $create_child1 = 'Y';
                                        }

                                        $update_child1_checked = '';
                                        $update_child1 = 'N';
                                        if(isset($data_group_menu[$child1['menu_id']]) && $data_group_menu[$child1['menu_id']]['update'] == 'Y')
                                        {
                                            $update_child1_checked = 'checked';
                                            $update_child1 = 'Y';
                                        }

                                        $delete_child1_checked = '';
                                        $delete_child1 = 'N';
                                        if(isset($data_group_menu[$child1['menu_id']]) && $data_group_menu[$child1['menu_id']]['delete'] == 'Y')
                                        {
                                            $delete_child1_checked = 'checked';
                                            $delete_child1 = 'Y';
                                        }

                                        $approve_child1_checked = '';
                                        $approve_child1 = 'N';
                                        if(isset($data_group_menu[$child1['menu_id']]) && $data_group_menu[$child1['menu_id']]['approve'] == 'Y')
                                        {
                                            $approve_child1_checked = 'checked';
                                            $approve_child1 = 'Y';
                                        }

                                        $download_child1_checked = '';
                                        $download_child1 = 'N';
                                        if(isset($data_group_menu[$child1['menu_id']]) && $data_group_menu[$child1['menu_id']]['download'] == 'Y')
                                        {
                                            $download_child1_checked = 'checked';
                                            $download_child1 = 'Y';
                                        } 
                                        echo "<tr>
                                                <td align='center'>".$no++.".</td>
                                                <td>&nbsp;&nbsp;&nbsp;".$child1['name']." <input type='hidden' name='menu_id[]' value='".$child1['menu_id']."'></td>
                                                <td><input type='checkbox' class='icheck-3' ".$view_child1_checked."> <input type='text' style='display:none;' name='view[]' value='".$view_child1."'></td>
                                                <td><input type='checkbox' class='icheck-2' ".$create_child1_checked."> <input type='text' style='display:none;' name='create[]' value='".$create_child1."'></td>
                                                <td><input type='checkbox' class='icheck-1' ".$read_child1_checked."> <input type='text' style='display:none;' name='read[]' value='".$read_child1."'></td>
                                                <td><input type='checkbox' class='icheck-4' ".$update_child1_checked."> <input type='text' style='display:none;' name='update[]' value='".$update_child1."'></td>
                                                <td><input type='checkbox' class='icheck-5' ".$delete_child1_checked."> <input type='text' style='display:none;' name='delete[]' value='".$delete_child1."'></td>
                                                <td><input type='checkbox' class='icheck-6' ".$approve_child1_checked."> <input type='text' style='display:none;' name='approve[]' value='".$approve_child1."'></td>
                                                <td><input type='checkbox' class='icheck-7' ".$download_child1_checked."> <input type='text' style='display:none;' name='download[]' value='".$download_child1."'></td>
                                              </tr>";

                                        if(isset($data_child_menu[$child1['menu_id']]))
                                        {
                                            foreach ($data_child_menu[$child1['menu_id']] as $key => $child2) {
                                                
                                                $view_child2_checked = '';
                                                $view_child2 = 'N';
                                                if(isset($data_group_menu[$child2['menu_id']]) && $data_group_menu[$child2['menu_id']]['view'] == 'Y')
                                                {
                                                    $view_child2_checked = 'checked';
                                                    $view_child2 = 'Y';
                                                }

                                                $read_child2_checked = '';
                                                $read_child2 = 'N';
                                                if(isset($data_group_menu[$child2['menu_id']]) && $data_group_menu[$child2['menu_id']]['read'] == 'Y')
                                                {
                                                    $read_child2_checked = 'checked';
                                                    $read_child2 = 'Y';
                                                }

                                                $create_child2_checked = '';
                                                $create_child2 = 'N';
                                                if(isset($data_group_menu[$child2['menu_id']]) && $data_group_menu[$child2['menu_id']]['create'] == 'Y')
                                                {
                                                    $create_child2_checked = 'checked';
                                                    $create_child2 = 'Y';
                                                }

                                                $update_child2_checked = '';
                                                $update_child2 = 'N';
                                                if(isset($data_group_menu[$child2['menu_id']]) && $data_group_menu[$child2['menu_id']]['update'] == 'Y')
                                                {
                                                    $update_child2_checked = 'checked';
                                                    $update_child2 = 'Y';
                                                }

                                                $delete_child2_checked = '';
                                                $delete_child2 = 'N';
                                                if(isset($data_group_menu[$child2['menu_id']]) && $data_group_menu[$child2['menu_id']]['delete'] == 'Y')
                                                {
                                                    $delete_child2_checked = 'checked';
                                                    $delete_child2 = 'Y';
                                                }

                                                $approve_child2_checked = '';
                                                $approve_child2 = 'N';
                                                if(isset($data_group_menu[$child2['menu_id']]) && $data_group_menu[$child2['menu_id']]['approve'] == 'Y')
                                                {
                                                    $approve_child2_checked = 'checked';
                                                    $approve_child2 = 'Y';
                                                }

                                                $download_child2_checked = '';
                                                $download_child2 = 'N';
                                                if(isset($data_group_menu[$child2['menu_id']]) && $data_group_menu[$child2['menu_id']]['download'] == 'Y')
                                                {
                                                    $download_child2_checked = 'checked';
                                                    $download_child2 = 'Y';
                                                } 
                                                echo "<tr>
                                                        <td align='center'>".$no++.".</td>
                                                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$child2['name']." <input type='hidden' name='menu_id[]' value='".$child2['menu_id']."'></td>
                                                        <td><input type='checkbox' class='icheck-3' ".$view_child2_checked."> <input type='text' style='display:none;' name='view[]' value='".$view_child2."'></td>
                                                        <td><input type='checkbox' class='icheck-2' ".$create_child2_checked."> <input type='text' style='display:none;' name='create[]' value='".$create_child2."'></td>
                                                        <td><input type='checkbox' class='icheck-1' ".$read_child2_checked."> <input type='text' style='display:none;' name='read[]' value='".$read_child2."'></td>
                                                        <td><input type='checkbox' class='icheck-4' ".$update_child2_checked."> <input type='text' style='display:none;' name='update[]' value='".$update_child2."'></td>
                                                        <td><input type='checkbox' class='icheck-5' ".$delete_child2_checked."> <input type='text' style='display:none;' name='delete[]' value='".$delete_child2."'></td>
                                                        <td><input type='checkbox' class='icheck-6' ".$approve_child2_checked."> <input type='text' style='display:none;' name='approve[]' value='".$approve_child2."'></td>
                                                        <td><input type='checkbox' class='icheck-7' ".$download_child2_checked."> <input type='text' style='display:none;' name='download[]' value='".$download_child2."'></td>
                                                      </tr>";
                                            }
                                        }
                                    }
                                }
                            }
                        } 
                        else
                        {
                            echo "<tr><td align='center' colspan='6'>Empty Data</td></tr>";
                        }
                    ?>
                    
                </tbody>
            </table>
        </div>  
    </div>
    <div class="row">
      <div class="col-md-12"> 
        <div class="text-center">
          <button type="submit" class="btn btn-success" id="save_access_menu" ><i class="fa fa-save"></i> Save</button>
        </div>
      </div>
    </div>
    </form>
</div>
<?php $this->load->view('../../include/footer'); ?>
<script src="<?php echo base_url('application/modules/Group/views/group.js')?>"></script> 
<script src="<?php echo base_url('application/modules/Group/views/group_access_menu.js')?>"></script> 