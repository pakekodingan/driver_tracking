var responsiveHelper;
var breakpointDefinition = {
    tablet: 1024,
    phone : 480
};
var tableContainer;

jQuery(document).ready(function($)
{
    tableContainer = $("#table-2");
    
    tableContainer.dataTable({
        "sPaginationType": "bootstrap",
        "aLengthMenu": [
        [10,25, 50, 100, -1],
        [10,25, 50, 100, "All"]
        ],
        "iDisplayLength": -1,
        "bStateSave": true,

        // Responsive Settings
        "bAutoWidth"     : true,
        fnPreDrawCallback: function () {
            // Initialize the responsive datatables helper once.
            if (!responsiveHelper) {
                responsiveHelper = new ResponsiveDatatablesHelper(tableContainer, breakpointDefinition);
            }
        },
        fnRowCallback  : function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            responsiveHelper.createExpandIcon(nRow);
        },
        fnDrawCallback : function (oSettings) {
            responsiveHelper.respond();
        }
    });
    
    $(".dataTables_wrapper select").select2({
        minimumResultsForSearch: -1
    });
       
    $("#form2").submit(function(e){
        e.preventDefault();
        $.ajax({
            url : site_url + active_controller + '/save_access_menu',
            type : 'POST',
            data : new FormData(this),
            processData : false, 
            contentType : false, 
            beforeSend: function(){
                $('#save_access_menu').loading('start');
            },
            success: function(response){
                res = JSON.parse(response);
                if(res.message == 'success'){
                    swal("Success", "Thanks You.", "success");
                }else{
                    swal("Failed !", "Your proccess is failed", "error");
                }

                $('#save_access_menu').loading('stop');      
            },
            error: function(){
              $('#save_access_menu').loading('stop');      
              swal("Error !", "Your proccess is error", "error");

            }
        });    
    });   

    $('#all-view').on('ifChecked', function(event){
      $('.icheck-3').iCheck('check');
    });
    $('#all-view').on('ifUnchecked', function(event){
      $('.icheck-3').iCheck('uncheck');
    });

    $('#all-create').on('ifChecked', function(event){
      $('.icheck-2').iCheck('check');
    });
    $('#all-create').on('ifUnchecked', function(event){
      $('.icheck-2').iCheck('uncheck');
    });

    $('#all-read').on('ifChecked', function(event){
      $('.icheck-1').iCheck('check');
    });
    $('#all-read').on('ifUnchecked', function(event){
      $('.icheck-1').iCheck('uncheck');
    });

    $('#all-update').on('ifChecked', function(event){
      $('.icheck-4').iCheck('check');
    });
    $('#all-update').on('ifUnchecked', function(event){
      $('.icheck-4').iCheck('uncheck');
    });

    $('#all-delete').on('ifChecked', function(event){
      $('.icheck-5').iCheck('check');
    });
    $('#all-delete').on('ifUnchecked', function(event){
      $('.icheck-5').iCheck('uncheck');
    });

    $('#all-approve').on('ifChecked', function(event){
      $('.icheck-6').iCheck('check');
    });
    $('#all-approve').on('ifUnchecked', function(event){
      $('.icheck-6').iCheck('uncheck');
    });

    $('#all-download').on('ifChecked', function(event){
      $('.icheck-7').iCheck('check');
    });
    $('#all-download').on('ifUnchecked', function(event){
      $('.icheck-7').iCheck('uncheck');
    });

    $('.icheck-3').on('ifChecked', function(event){
      $(this).parent().parent().find("input[type=text]").val('Y');
    });
    $('.icheck-3').on('ifUnchecked', function(event){
      $(this).parent().parent().find("input[type=text]").val('N');
    });

    $('.icheck-2').on('ifChecked', function(event){
      $(this).parent().parent().find("input[type=text]").val('Y');
    });
    $('.icheck-2').on('ifUnchecked', function(event){
      $(this).parent().parent().find("input[type=text]").val('N');
    });

    $('.icheck-1').on('ifChecked', function(event){
      $(this).parent().parent().find("input[type=text]").val('Y');
    });
    $('.icheck-1').on('ifUnchecked', function(event){
      $(this).parent().parent().find("input[type=text]").val('N');
    });

    $('.icheck-4').on('ifChecked', function(event){
      $(this).parent().parent().find("input[type=text]").val('Y');
    });
    $('.icheck-4').on('ifUnchecked', function(event){
      $(this).parent().parent().find("input[type=text]").val('N');
    });

    $('.icheck-5').on('ifChecked', function(event){
      $(this).parent().parent().find("input[type=text]").val('Y');
    });
    $('.icheck-5').on('ifUnchecked', function(event){
      $(this).parent().parent().find("input[type=text]").val('N');
    });

    $('.icheck-6').on('ifChecked', function(event){
      $(this).parent().parent().find("input[type=text]").val('Y');
    });
    $('.icheck-6').on('ifUnchecked', function(event){
      $(this).parent().parent().find("input[type=text]").val('N');
    });

    $('.icheck-7').on('ifChecked', function(event){
      $(this).parent().parent().find("input[type=text]").val('Y');
    });
    $('.icheck-7').on('ifUnchecked', function(event){
      $(this).parent().parent().find("input[type=text]").val('N');
    });
});
