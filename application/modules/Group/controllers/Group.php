<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Group extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if(!$this->session->userdata('isLogin')){
			redirect('Login');
		}
		$this->load->model('M_group', 'group_model');
	}

	public function index() {
		history('view group');
		$this->load->view('v_group');
	}

	public function get_data() {
		$active_controller = $this->uri->segment(1);
		/* Array of database columns which should be read and sent back to DataTables. Use a space where
			         * you want to insert a non-database field (for example a counter or static image)
		*/

		$aColumns = array('group_id', 'group_name', 'flag_all', 'flag_active');

		// DB table to use
		$sTable = 'group';
		//

		$iDisplayStart = $this->input->get_post('iDisplayStart', true);
		$iDisplayLength = $this->input->get_post('iDisplayLength', true);
		$iSortCol_0 = $this->input->get_post('iSortCol_0', true);
		$iSortingCols = $this->input->get_post('iSortingCols', true);
		$sSearch = $this->input->get_post('sSearch', true);
		$sEcho = $this->input->get_post('sEcho', true);

		// Paging
		if (isset($iDisplayStart) && $iDisplayLength != '-1') {
			$this->db->limit($this->db->escape_str($iDisplayLength), $this->db->escape_str($iDisplayStart));
		}

		// Ordering
		if (isset($iSortCol_0)) {
			for ($i = 0; $i < intval($iSortingCols); $i++) {
				$iSortCol = $this->input->get_post('iSortCol_' . $i, true);
				$bSortable = $this->input->get_post('bSortable_' . intval($iSortCol), true);
				$sSortDir = $this->input->get_post('sSortDir_' . $i, true);

				if ($bSortable == 'true') {
					$this->db->order_by($aColumns[intval($this->db->escape_str($iSortCol))], $this->db->escape_str($sSortDir));
				}
			}
		}

		/*
			         * Filtering
			         * NOTE this does not match the built-in DataTables filtering which does it
			         * word by word on any field. It's possible to do here, but concerned about efficiency
			         * on very large tables, and MySQL's regex functionality is very limited
		*/
		if (isset($sSearch) && !empty($sSearch)) {
			for ($i = 0; $i < count($aColumns); $i++) {
				$bSearchable = $this->input->get_post('bSearchable_' . $i, true);

				// Individual column filtering
				if (isset($bSearchable) && $bSearchable == 'true') {
					$this->db->or_like($aColumns[$i], $this->db->escape_like_str($sSearch));
				}
			}
		}

		// Select Data
		$this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $aColumns)), false);
		$rResult = $this->db->get($sTable);

		// Data set length after filtering
		$this->db->select('FOUND_ROWS() AS found_rows');
		$iFilteredTotal = $this->db->get()->row()->found_rows;

		// Total data set length
		$iTotal = $this->db->count_all($sTable);

		// Output
		$output = array(
			'sEcho' => intval($sEcho),
			'iTotalRecords' => $iTotal,
			'iTotalDisplayRecords' => $iFilteredTotal,
			'aaData' => array(),
		);

		$no = $iDisplayStart;
		foreach ($rResult->result_array() as $aRow) {
			$no++;
			$row = array();
			if ($aRow['flag_all'] == 'Y') {
				$flag_all = "<span class='badge badge-warning badge-roundless'><i class='fa fa-check' ></i></span>";
			} else {
				$flag_all = "<span class='badge badge-roundless'><i class='fa fa-times' ></i></span>";
			}

			if ($aRow['flag_active'] == 'Y') {
				$flag_active = "<span class='badge badge-warning badge-roundless'><i class='fa fa-check' ></i></span>";
			} else {
				$flag_active = "<span class='badge badge-roundless'><i class='fa fa-times' ></i></span>";
			}

			$row[] = "<center>" . $no . ".</center>";
			$row[] = $aRow['group_name'];
			$row[] = $flag_all;
			$row[] = $flag_active;
			$row[] = "<center>
                        <button type='button' class='btn btn-orange btn-xs' onclick='move_page(\"" . site_url($active_controller . '/access_menu/?id=' . encrypt($aRow['group_id'])) . "\")'><i class='fa fa-key fa-rotate-90'></i></button>
                        <button type='button' class='btn btn-info btn-xs' onclick='move_page(\"" . site_url($active_controller . '/edit/?id=' . encrypt($aRow['group_id'])) . "\")'><i class='fa fa-pencil-square-o'></i></button>
                        <button class='btn btn-xs btn-danger' onClick='del(\"" . $aRow['group_id'] . "\",\"" . $aRow['group_name'] . "\")'><i class='fa fa-trash-o'></i></button>
                      <center>";

			$output['aaData'][] = $row;
		}
		// dd( $output['aaData']);

		history('view data group ?search=' . $sSearch);
		echo json_encode($output);
	}

	public function add() {
		history('add group');
		$this->load->view("v_group_add");
	}

	public function edit() {
		$id = decrypt($this->input->get('id'));
		$data['data_group_by_id'] = $this->group_model->group_by_id($id)->result_array()[0];
		history('edit group ?id=' . $id);
		$this->load->view('v_group_edit', $data);
	}

	public function save() {

		$data['group_name'] = strtoupper($this->input->post('name'));
		$data['flag_active'] = ($this->input->post('active') == 'on') ? 'Y' : 'N';
		$data['flag_all'] = ($this->input->post('all_access') == 'on') ? 'Y' : 'N';

		$errors = array();
		if ($this->input->post('process') == 'add') {
			$group_id = '';
			$check_name = $this->group_model->check_name($data['group_name']);
			if ($check_name == 0) {
				$result = $this->group_model->group_insert($data);
				$group_id = $this->db->insert_id();
				$message = "failed";
				if ($result == 1) {
					$message = "success";
				}
			} else {
				$message = "failed";
				$errors['name'] = "name already exits.";
			}
			history('created group ?id=' . $group_id . '&name=' . $data['group_name'] . ' :' . $message);
			echo json_encode(array("message" => $message, "errors" => $errors));

		} elseif ($this->input->post('process') == 'edit') {
			$group_id = $this->input->post('id');

			$check_name = $this->group_model->check_name_for_edit($group_id, $data['group_name']);
			if ($check_name == 0) {
				$result = $this->group_model->group_update($data, array('group_id' => $group_id));
				$message = "failed";
				if ($result == 1) {
					$message = "success";
				}
			} else {
				$message = "failed";
				$errors['name'] = "name already exits.";

			}
			history('modified group ?id=' . $group_id . '&name=' . $data['group_name'] . ' :' . $message);
			echo json_encode(array("message" => $message, "errors" => $errors));
		}
	}

	public function delete() {
		$id = $this->input->get('id');
		$group = $this->group_model->group_by_id($id)->row();
		$group_name = $group->group_name;
		$result = $this->group_model->group_delete($id);
		$message = "failed";
		if ($result == 1) {
			$message = "success";
		}
		history('deleted group ?id=' . $id . '&name=' . $group_name . ' :' . $message);
		echo json_encode(array("message" => $message));
	}

	public function access_menu() {
		$id = decrypt($this->input->get('id'));
		$data['data_parent_menu'] = $this->group_model->get_menu_parent()->result_array();
		$arr_child_menu = $this->group_model->get_menu_child()->result_array();
		foreach ($arr_child_menu as $key => $value) {
			$child_menu[$value['parent_id']][] = array('menu_id' => $value['menu_id'],
				'name' => $value['name'],
				'path' => $value['path'],
				'icon' => $value['icon'],
				'flag_active' => $value['flag_active']);
		}
		$data['data_child_menu'] = $child_menu;
		$data['data_group_by_id'] = $this->group_model->group_by_id($id)->result_array()[0];
		$arr_group_menu = $this->group_model->get_group_menu($id)->result_array();
		$group_menu = array();
		foreach ($arr_group_menu as $key => $value) {
			$group_menu[$value['menu_id']] = array(
				'view' => $value['view'],
				'read' => $value['read'],
				'create' => $value['create'],
				'update' => $value['update'],
				'delete' => $value['delete'],
				'approve' => $value['approve'],
				'download' => $value['download'],
			);
		}
		$data['data_group_menu'] = $group_menu;
		history('view group access menu ?id=' . $id);
		$this->load->view('v_group_access_menu', $data);
	}

	public function save_access_menu() {
		$group_id = $this->input->post('group_id');
		$menu_id = $this->input->post('menu_id');
		$view = $this->input->post('view');
		$create = $this->input->post('create');
		$read = $this->input->post('read');
		$update = $this->input->post('update');
		$delete = $this->input->post('delete');
		$approve = $this->input->post('approve');
		$download = $this->input->post('download');

		$this->group_model->group_menu_delete($group_id);
		for ($i = 0; $i < count($menu_id); $i++) {
			$data = array(
				'group_id' => $group_id,
				'menu_id' => $menu_id[$i],
				'view' => $view[$i],
				'create' => $create[$i],
				'read' => $read[$i],
				'update' => $update[$i],
				'delete' => $delete[$i],
				'approve' => $approve[$i],
				'download' => $download[$i],
				'created_by' => $this->session->userdata('ses_username'),
				'created_date' => date('Y-m-d H:i:s'),
				'modified_by' => $this->session->userdata('ses_username'),
				'modified_date' => date('Y-m-d H:i:s'),
			);

			$this->group_model->group_menu_insert($data);
		}

		$message = "success";
		history('modified group accesss menu ?id=' . $group_id . " :" . $message);
		echo json_encode(array("message" => $message));
	}
}

/* End of file Group.php */
/* Location: ./application/modules/Group/controllers/Group.php */
