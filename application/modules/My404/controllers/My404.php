<?php 
defined('BASEPATH') OR exit('No direct script access allowed'); 
 
class My404 extends CI_Controller { 
 
	public function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('isLogin')){
			redirect('login');
		}
		$this->load->model('M_my404','my404_model');
	}
 
	public function index()
	{
		history('error page 404');
       	$this->output->set_status_header('404');
		$this->load->view('v_my404');
	}
}
 
/* End of file My404.php */
/* Location: ./application/modules/My404/controllers/My404.php */
