<?php 
defined('BASEPATH') OR exit('No direct script access allowed'); 
 
class M_user extends CI_Model { 
 
	public function user_insert($data)
    {
        $insert = $this->db->insert('user',$data);
        $result = ($insert == true)?1:0;
        return $result;
    }

    public function check_username($username)
    {
        $check = $this->db->get_where('user',array('username'=>$username));
        $result = ($check->num_rows() > 0)?1:0;
        return $result;
    }

    public function user_by_id($id)
    {   
        $get = $this->db->get_where('user',array('user_id'=>$id));
        return $get;
    }
     
    public function user_delete($id)
    {
        $delete = $this->db->delete('user',array('user_id'=>$id));
        $result = ($delete == true)?1:0;
        return $result;
    }

    public function get_employee()
    {
        $result = $this->db->get('employee');
        return $result;
    }

    public function get_employee_by_id($id)
    {
        $result = $this->db->get_where('employee',array('employee_id'=>$id));
        return $result;
    }
}
 
/* End of file M_user.php */
/* Location: ./application/modules/User/models/M_user.php */
