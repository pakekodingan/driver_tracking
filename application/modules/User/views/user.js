jQuery(document).ready(function($)
{
    refresh_data();

    $("#save").click(function(){
        if($(".validate").valid()){
            $("#form1").submit();
        }
    });

    $("#form1").submit(function(e){
        e.preventDefault();
        $.ajax({
            url : site_url + active_controller + '/save',
            type : 'POST',
            data : new FormData(this),
            processData : false, 
            contentType : false, 
            beforeSend: function(){
                $('#save').loading('start');
            },
            success: function(response){
                res = JSON.parse(response);
                if(res.message == 'success'){
                    swal("Success", "Thanks You.", "success");
                }else if(res.message == 'failed' && res.errors.username != ""){
                    $("#username").parent().parent().addClass('validate-has-error');
                    $("#username").parent().parent().append('<span for="name" class="validate-has-error">'+res.errors.username+'</span>');
                }else{
                    swal("Failed !", "Your proccess is failed", "error");
                }

                $('#save').loading('stop');      
            },
            error: function(){
              $('#save').loading('stop');      
              swal("Error !", "Your proccess is error", "error");

            }
        });
    });

    $('#reset-password').click(function(){
        $.ajax({
            url : site_url + active_controller +"/reset_password",
            success : function(response){
                res = JSON.parse(response);
                $('#pwd').val(res.data);
                $('#pwd-hash').val(res.data);
            },
            error : function(e){
                alert("error "+e);
            }
        });
    });

    $('input.icheck-3').iCheck({
        checkboxClass: 'icheckbox_minimal-orange',
        radioClass: 'iradio_minimal-orange'
    });

    $('#btn-copy-password-hash').tooltip({
      trigger: 'click',
      placement: 'bottom'
    });

    var clipboard_password_hash = new ClipboardJS('#btn-copy-password-hash');
    clipboard_password_hash.on('success', function(e) {
        setTooltip('#btn-copy-password-hash','Copied!');
        hideTooltip('#btn-copy-password-hash','Copy');
    });

    $('#btn-copy-username').tooltip({
      trigger: 'click',
      placement: 'bottom'
    });

    var clipboard_username = new ClipboardJS('#btn-copy-username');
    clipboard_username.on('success', function(e) {
        setTooltip('#btn-copy-username','Copied!');
        hideTooltip('#btn-copy-username','Copy');
    });

    $('#employee').change(function(){
        var id = $(this).val();
        $.ajax({
            url : site_url + active_controller +"/create_username?id="+id,
            success : function(response){
                var res = JSON.parse(response);
                $('#username').val(res.data);
                $('#reset-password').trigger('click');
            },
            error : function(e){
                alert('error '+e);
            }
        });
    });
});


function setTooltip(element,message) {
  $(element).tooltip('hide')
    .attr('data-original-title', message)
    .tooltip('show');
}

function hideTooltip(element,message) {
  setTimeout(function() {
    $(element).tooltip('hide').attr('data-original-title', message);
  }, 1000);
}

function refresh_data(){

    var responsiveHelper;
    var breakpointDefinition = {
        tablet: 1024,
        phone : 480
    };
    var tableContainer;

    tableContainer = $("#table-1");
    param1 = "";
    param2 = "";

    param ="";
    param += "?param1="+param1
    param += "&param2="+param2;

    tableContainer.dataTable({
        "sPaginationType": "bootstrap",
        "aLengthMenu": [10, 25, 50, 100,"All"],
        "aaSorting": [[0, 'asc']],
        "bDestroy": true,
        "bStateSave": true,
        "bProcessing": true,
        "bServerSide": true,
        "sServerMethod": "GET",
        "sAjaxSource": site_url+active_controller+"/get_data"+param,
        "iDisplayLength": 10,

        // Responsive Settings
        "bAutoWidth"     : false,
        fnPreDrawCallback: function () {
            // Initialize the responsive datatables helper once.
            if (!responsiveHelper) {
                responsiveHelper = new ResponsiveDatatablesHelper(tableContainer, breakpointDefinition);
            }
        },
        fnRowCallback  : function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            responsiveHelper.createExpandIcon(nRow);
        },
        fnDrawCallback : function (oSettings) {
            responsiveHelper.respond();
        }
    });

    $(".dataTables_wrapper select").select2({
        minimumResultsForSearch: -1
    });
}

function del(id,nama){
    swal({
        title: "Delete "+nama+" ?",
        text: "Click Delete. ",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Yes, Delete !",
        cancelButtonText: "No, Cancel !",
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
        closeOnCancel: false
    },
    function(isConfirm) {
        if (isConfirm) {
            $.ajax({
                  url : site_url + active_controller + "/delete?id="+id,
                  success : function(response){
                    var res = JSON.parse(response);
                    if(res.message == 'success'){
                       swal({
                                title: "Success",
                                text: "Deleted, Thanks You.",
                                type: "success",
                                showCancelButton: true,
                                showCancelButton: false
                            },
                            function(isConfirm) {
                                refresh_data();
                        });
                    }else{
                      swal("Gagal !", "Data gagal reject", "error");
                    }
                  },
                  error : function(){
                    swal("Error !", "Failed", "error");
                  }
              });
        } else {
            swal("Cancelled", "Your data is safe.", "error");
        }
    });
}

function copy_to_clipboard() {
  var copyText = document.getElementById("pwd-hash");
  copyText.select();
  copyText.setSelectionRange(0, 99999)
  document.execCommand("copy");
  alert("Copied the text: " + copyText.value);
}

