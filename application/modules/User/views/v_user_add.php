<?php $this->load->view('../../include/header'); ?>
<!-- <?php dump($list_parent); ?> -->
<div class="container-fluid">
  <?php echo breadcrumb($this->uri->segment(1),$this->uri->segment(2)); ?>
    <div class="row">
        <div class="col-md-11">
          <h2>Form Add User</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <button type="button" class="btn btn-primary pull-right" onclick="goBack()">
                <i class="fa fa-arrow-left"></i>
                Back
            </button>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-info">
                <div class="panel-body">
                  <div class="row">
                    <div class="col-md-12">
                      <form role="form" id="form1" method="post" class="validate">
                        <input type="hidden" name="process" value="add">
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Employee</label>
                              <select class="select2"  id="employee" name="employee">
                                <option value="">Select an option</option>
                                <?php foreach ($data_employee as $val): 
                                        echo "<option value='$val[employee_id]'> $val[employee_name]</option>";
                                      endforeach ?>
                              </select>
                            </div>             
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Password</label>
                              <div class="input-group">
                                <input type="password" class="form-control" name="pwd" id="pwd" data-validate="required" placeholder="Password" readonly="" >
                                <span class="input-group-btn">
                                  <button class="btn btn-warning pointer" type="button" id="reset-password" data-toggle="tooltip" data-placement="top" title="" data-original-title="Reset"><i class="fa fa-refresh"></i></button>
                                </span>
                              </div>
                            </div>             
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                             <label>Username</label>
                              <div class="input-group">
                                <input type="text" class="form-control" name="username" id="username" data-validate="required" placeholder="Username" >
                                <span class="input-group-btn">
                                  <button class="btn btn-white pointer" type="button" id="btn-copy-username" data-clipboard-action="copy" data-clipboard-target="#username" data-toggle="tooltip" data-placement="top" title="" data-original-title="Copy"><i class="fa fa-copy"></i></button>
                                </span>
                              </div>
                            </div>             
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="">Password Hash</label>
                              <div class="input-group">
                                <input type="text" class="form-control" name="pwd_hash" id="pwd-hash" placeholder="Password hash" readonly="">
                                <span class="input-group-btn">
                                  <button class="btn btn-white pointer" type="button" id="btn-copy-password-hash" data-clipboard-action="copy" data-clipboard-target="#pwd-hash" data-toggle="tooltip" data-placement="top" title="" data-original-title="Copy"><i class="fa fa-copy"></i></button>
                                </span>
                              </div>
                            </div>             
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-12">
                            <div class="form-group">
                              <br> 
                              <label>Active</label> &nbsp;&nbsp;&nbsp;<input type="checkbox" class="icheck-3" name="active" id="minimal-checkbox-1-3"><br>
                              *) Check to actived.
                            </div>             
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-12"> 
                            <div class="text-center">
                              <button type="button" class="btn btn-success" id="save" ><i class="fa fa-save"></i> Save</button>
                            </div>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('../../include/footer'); ?>
<script src="<?php echo base_url('application/modules/User/views/user.js')?>"></script> 
