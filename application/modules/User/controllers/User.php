<?php 
defined('BASEPATH') OR exit('No direct script access allowed'); 
 
class User extends CI_Controller { 
 
	public function __construct()
	{
		parent::__construct();
		// if(!$this->session->userdata('isLogin')){
		// 	redirect('Login');
		// }
		$this->load->model('M_user','user_model');
	}
 
	public function index()
	{
		history('view user');
		$this->load->view('v_user');
	}

	public function get_data()
    {
        /* Array of database columns which should be read and sent back to DataTables. Use a space where
         * you want to insert a non-database field (for example a counter or static image)
         */

        $aColumns = array('user_id', 'username', 'flag_active', 'online');
        
        // DB table to use
        $sTable = 'user';
        //
    
        $iDisplayStart = $this->input->get_post('iDisplayStart', true);
        $iDisplayLength = $this->input->get_post('iDisplayLength', true);
        $iSortCol_0 = $this->input->get_post('iSortCol_0', true);
        $iSortingCols = $this->input->get_post('iSortingCols', true);
        $sSearch = $this->input->get_post('sSearch', true);
        $sEcho = $this->input->get_post('sEcho', true);
    
        // Paging
        if(isset($iDisplayStart) && $iDisplayLength != '-1')
        {
            $this->db->limit($this->db->escape_str($iDisplayLength), $this->db->escape_str($iDisplayStart));
        }
        
        // Ordering
        if(isset($iSortCol_0))
        {
            for($i=0; $i<intval($iSortingCols); $i++)
            {
                $iSortCol = $this->input->get_post('iSortCol_'.$i, true);
                $bSortable = $this->input->get_post('bSortable_'.intval($iSortCol), true);
                $sSortDir = $this->input->get_post('sSortDir_'.$i, true);
    
                if($bSortable == 'true')
                {
                    $this->db->order_by($aColumns[intval($this->db->escape_str($iSortCol))], $this->db->escape_str($sSortDir));
                }
            }
        }
        
        /* 
         * Filtering
         * NOTE this does not match the built-in DataTables filtering which does it
         * word by word on any field. It's possible to do here, but concerned about efficiency
         * on very large tables, and MySQL's regex functionality is very limited
         */
        if(isset($sSearch) && !empty($sSearch))
        {
            for($i=0; $i<count($aColumns); $i++)
            {
                $bSearchable = $this->input->get_post('bSearchable_'.$i, true);
                
                // Individual column filtering
                if(isset($bSearchable) && $bSearchable == 'true')
                {
                    $this->db->or_like($aColumns[$i], $this->db->escape_like_str($sSearch));
                }
            }
        }
        
        // Select Data
        $this->db->select('SQL_CALC_FOUND_ROWS '.str_replace(' , ', ' ', implode(', ', $aColumns)), false);

        // $this->db->select();
        $rResult = $this->db->get($sTable);
    
        // Data set length after filtering
        $this->db->select('FOUND_ROWS() AS found_rows');
        $iFilteredTotal = $this->db->get()->row()->found_rows;
    
        // Total data set length
        $iTotal = $this->db->count_all($sTable);
    
        // Output
        $output = array(
            'sEcho' => intval($sEcho),
            'iTotalRecords' => $iTotal,
            'iTotalDisplayRecords' => $iFilteredTotal,
            'aaData' => array()
        );
        
        $no = $iDisplayStart;
        foreach($rResult->result_array() as $aRow)
        {
        	$no++;
            $row = array();
            if($aRow['online'] == 'Y')
            {
                $online = "<span class='badge badge-warning'><i class='fa fa-dot-circle-o' ></i></span>";
            }
            else
            {
                $online = "<span class='badge '><i class='fa fa-times-circle' ></i></span>";
            }

            if($aRow['flag_active'] == 'Y')
            {
                $flag_active = "<span class='badge badge-warning badge-roundless'><i class='fa fa-check' ></i></span>";
            }
            else
            {
                $flag_active = "<span class='badge badge-roundless'><i class='fa fa-times' ></i></span>";
            }   
            
            $row[] = "<center>".$no.".</center>";
            $row[] = $aRow['username'];
            $row[] = $flag_active;
            $row[] = $online;
            $row[] = "<center>
                        <button type='button' class='btn btn-info btn-xs' onclick='move_page(\"".site_url($this->uri->segment(1).'/edit/?id='.encrypt($aRow['user_id']))."\")'><i class='fa fa-pencil-square-o'></i></button>
                        <button class='btn btn-xs btn-danger' onClick='del(\"".$aRow['user_id']."\",\"".$aRow['username']."\")'><i class='fa fa-trash-o'></i></button>
                      <center>";
 
    
            $output['aaData'][] = $row;
        }
        // dd( $output['aaData']);

        history('view data user ?search='.$sSearch);
        echo json_encode($output);
    }

    public function add()
    {
        $data['data_employee'] = $this->user_model->get_employee()->result_array();
        history('add user');
        $this->load->view("v_user_add",$data);
    }

    public function random_password()
    {
        $seed = str_split('abcdefghijklmnopqrstuvwxyz'
                         .'0123456789'); 
        shuffle($seed); 
        $rand = '';
        foreach (array_rand($seed, 6) as $k) $rand .= $seed[$k];
        return $rand;
    }

    public function reset_password()
    {
        $data = $this->random_password();
        history('reset password ?data='.$data);
        echo json_encode(array("message"=>"success","data"=>$data));
    }

    public function edit()
    {
        $id = $this->input->get('id');
        $data['data_user_by_id']    = $this->user_model->user_by_id($id)->result_array()[0];
        history('edit user ?id='.$id);
        $this->load->view('v_user_edit',$data);
    }

    public function delete()
    {
        $id = $this->input->get('id');
        $user = $this->user_model->user_by_id($id)->row();
        $username = $user->username;
        $result = $this->user_model->user_delete($id);
        $message = "failed";
        if($result == 1)
        {
            $message = "success";
        }
        history('deleted user ?id='.$id.'&name='.$username.' :'.$message);
        echo json_encode(array("message"=>$message));
    }
    
    public function create_username()
    {
        $id = $this->input->get('id');
        $employee = $this->user_model->get_employee_by_id($id)->row();
        $username = create_username($employee->employee_name,$employee->birthday);
        history('create username ?id='.$id.'&name='.$username[0].' :success');
        echo json_encode(array("message"=>"success","data"=>$username[0]));   
    }

    public function save()
    {
        $data['username'] = strtolower($this->input->post('username'));
        $data['password'] = $this->input->post('pwd');
        $data['password_hash'] = $this->input->post('pwd-hash');
        $data['group_id'] = '2';
        $data['flag_active'] = ($this->input->post('active') == 'on') ? 'Y':'N' ;

        $errors = array ();
        if($this->input->post('process') == 'add')
        {   
            $user_id = '';
            $check_username = $this->user_model->check_username($data['username']);
            if($check_username == 0)
            {
                $result = $this->user_model->user_insert($data);
                $user_id = $this->db->insert_id();
                $message = "failed";
                if($result == 1)
                {
                    $message = "success";
                }
            }
            else
            {
               $message = "failed";
               $errors['username'] = "username already exits.";
            }
            history('created user ?id='.$user_id.'&name='.$data['username'].' :'.$message);
            echo json_encode(array("message"=>$message,"errors"=>$errors));
            
        }
        elseif($this->input->post('process') == 'edit')
        {
            // $group_id    = $this->input->post('id');

            // $check_name = $this->user_model->check_name_for_edit($group_id,$data['group_name']);
            // if($check_name == 0)
            // {
            //     $result     = $this->user_model->group_update($data,array('group_id'=>$group_id));
            //     $message = "failed";
            //     if($result == 1)
            //     {
            //         $message = "success";
            //     }
            // }
            // else
            // {
            //    $message = "failed";
            //    $errors['name'] = "name already exits.";

            // }
            // history('modified group ?id='.$group_id.'&name='.$data['group_name'].' :'.$message);
            // echo json_encode(array("message"=>$message,"errors"=>$errors));
        }
    }
}
 
/* End of file User.php */
/* Location: ./application/modules/User/controllers/User.php */
