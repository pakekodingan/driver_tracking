<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('isLogin')){
			redirect('Login');
		}	
		$this->load->model('M_menu','menu_model');

	}

	public function index()
	{ 
		$path = $this->uri->segment(1);
		$access_menu = access_menu($path);
		if($access_menu['read'] == 'Y')
		{
			$data['data_parent_menu'] 	= $this->menu_model->get_menu_parent()->result_array();
			$arr_child_menu 			= $this->menu_model->get_menu_child()->result_array();
			$child_menu = array();
			foreach ($arr_child_menu as $key => $value) {
				$child_menu[$value['parent_id']][] = array( 
															'menu_id' => $value['menu_id'],
															'name' => $value['name'],
															'path' => $value['path'],
															'icon' => $value['icon'],
															'flag_active' => $value['flag_active']
														  ); 
			}
			$data['data_child_menu'] 	= $child_menu;
			$data['access_menu'] 	= $access_menu;
			history('view menu');
			$this->load->view('v_menu',$data);
		}
		else
		{
			history('view menu :denied');
			$this->load->view('../../include/access_denied');
		}
	}

	public function add()
	{
		$data['data_parent_menu']	= $this->menu_model->get_menu()->result_array();
		$data['data_icon']		= $this->menu_model->get_menu_icon()->result_array();
		history('add menu');
		$this->load->view('v_menu_add',$data);
	}

	public function edit()
	{
		$id = decrypt($this->input->get('id'));
		$data['data_parent_menu']	= $this->menu_model->get_menu()->result_array();
		$data['data_icon'] = $this->menu_model->get_menu_icon()->result_array();
		$data['data_menu_by_id'] = $this->menu_model->menu_by_id($id)->result_array()[0];
		history('edit menu ?id='.$id);
		$this->load->view('v_menu_edit',$data);
	}

	public function save()
	{	
		$data['name'] = $this->input->post('name');
		$data['parent_id'] = $this->input->post('parent');
		$data['path'] = ucwords(strtolower(str_replace(" ", "_", $this->input->post('path'))));
		$data['weight'] = $this->input->post('weight');
		$data['icon'] = $this->input->post('icon');
		$active = "N";
		if($this->input->post('active') == 'on')
		{
			$active = "Y";
		}
		$collapsed_sidebar = "N";
		if($this->input->post('collapsed_sidebar') == 'on')
		{
			$collapsed_sidebar = "Y";
		}

		$module_generate = 'N';
		if($this->input->post('module_generate') && $this->input->post('module_generate') == 'on')
		{
			$module_generate = 'Y';
		}

		$data['flag_active'] = $active;
		$data['collapsed_sidebar'] = $collapsed_sidebar;
		
		$errors = array ();
		if($this->input->post('process') == 'add')
		{
			$data['created_by'] = $this->session->userdata('ses_username');
			$data['created_date'] = date('Y-m-d H:i:s');
			$dir = $data['path'];
			$menu_id = '';
			$check_path = $this->check_path($dir);
			if($check_path == 0)
			{
				if($module_generate == 'Y')
				{
					$this->create_module($dir);
				}
				$result = $this->menu_model->menu_insert($data);
				$menu_id = $this->db->insert_id();
				$message = "failed";
				if($result == 1)
				{
					$message = "success";
				}
			}
			else
			{	
				$message = "failed";
				$errors['path'] = "path already exits.";
			}
			history('created menu ?id='.$menu_id.'&name='.$data['name'].' :'.$message);
			echo json_encode(array("message"=>$message,"errors"=>$errors));
		}
		elseif($this->input->post('process')== 'edit') 
		{
			$menu_id 	= $this->input->post('id');
			$menu = $this->menu_model->menu_by_id($menu_id)->row();
			$path_before = $menu->path;
			$dir = $data['path'];
			if($dir != $path_before)
			{
				$check_path = $this->menu_model->menu_check_path($dir);
				if($check_path == 0)
				{	
					$data['modified_by'] = $this->session->userdata('ses_username');
					$data['modified_date'] = date('Y-m-d H:i:s');
					$result 	= $this->menu_model->menu_update($data,array('menu_id'=>$menu_id));
					$message = "failed";
					if($result == 1)
					{
						$message = "success";
					}
				}
				else
				{
					$message = "failed";
					$errors['path'] = "path already exists.";
				}
			}
			else
			{	
				$data['modified_by'] = $this->session->userdata('ses_username');
				$data['modified_date'] = date('Y-m-d H:i:s');
				$result 	= $this->menu_model->menu_update($data,array('menu_id'=>$menu_id));
				$message = "failed";
				if($result == 1)
				{
					$message = "success";
				}
			}
			history('modified menu ?id='.$menu_id.'&name='.$data['name'].' :'.$message);
			echo json_encode(array("message"=>$message,"errors"=>$errors));
		}

	}

	public function delete()
	{
		$id = decrypt($this->input->get('id'));
        $menu = $this->menu_model->menu_by_id($id)->row();
        $menu_name = $menu->name;
		$result = $this->menu_model->menu_delete($id);
		$message = "failed";
		if($result == 1)
		{
			$message = "success";
		}
		history('deleted menu ?id='.$id.'&name='.$menu_name.' :'.$message);
		echo json_encode(array("message"=>$message));
	}

	public function check_path($dir)
	{
		$dir = ucwords(strtolower($dir));
		$path_module = './application/modules/'.$dir.'/';
		$path_menu = $this->db->get_where('menu',array('path'=>$dir,'path <>'=>'javascript:void(0)'))->num_rows();
		$result = 1;
		if(!is_dir($path_module) && $path_menu == 0)
		{
			$result = 0; 
		}
		return $result;
	}

	public function create_module($dir)
	{	
		$dir = ucwords(strtolower(str_replace(" ","_",$dir)));
		$path_module 		= './application/modules/'.$dir.'/';
		$path_controller	= './application/modules/'.$dir.'/controllers/';
		$path_model 		= './application/modules/'.$dir.'/models/';
		$path_view 		 	= './application/modules/'.$dir.'/views/';

		if(!is_dir('application/modules/'.$dir))
		{
			mkdir($path_module,0777, TRUE);
		}
		
		if(!is_dir($path_controller))
		{
			mkdir($path_controller);
			$script_controller = "<?php \n";
			$script_controller .= "defined('BASEPATH') OR exit('No direct script access allowed'); \n \n";
			$script_controller .= "class ".$dir." extends CI_Controller { \n \n";
			$script_controller .= "\tpublic function __construct()\n";
			$script_controller .= "\t{\n";
			$script_controller .= "\t\tparent::__construct();\n";
			$script_controller .= "\t\tif(!\$this->session->userdata('isLogin')){\n";
			$script_controller .= "\t\t\tredirect('Login');\n";
			$script_controller .= "\t\t}\n";
			$script_controller .= "\t\t\$this->load->model('M_".strtolower($dir)."','".strtolower($dir)."_model');\n";
			$script_controller .= "\t}\n \n";
			$script_controller .= "\tpublic function index()\n";
			$script_controller .= "\t{\n";
			$script_controller .= "\t\t\$this->load->view('v_".strtolower($dir)."');\n";
			$script_controller .= "\t}\n \n";
			$script_controller .= "}\n \n";
			$script_controller .= "/* End of file ".$dir.".php */\n";
			$script_controller .= "/* Location: ./application/modules/".$dir."/controllers/".$dir.".php */\n";
			write_file($path_controller.$dir.'.php',$script_controller);
		}
		
		if(!is_dir($path_model))
		{
			mkdir($path_model,0777, TRUE);
			$model_name = "M_".strtolower($dir);
			$script_model = "<?php \n";
			$script_model .= "defined('BASEPATH') OR exit('No direct script access allowed'); \n \n";
			$script_model .= "class ".$model_name." extends CI_Model { \n \n";
			$script_model .= "\tpublic function rename_here()\n";
			$script_model .= "\t{\n \n";
			$script_model .= "\t}\n \n";
			$script_model .= "}\n \n";
			$script_model .= "/* End of file ".$model_name.".php */\n";
			$script_model .= "/* Location: ./application/modules/".$dir."/models/".$model_name.".php */\n";
			write_file($path_model.$model_name.'.php',$script_model);		
		}
		
		if(!is_dir($path_view))
		{
			mkdir($path_view,0777, TRUE);
			$view_name = "v_".strtolower($dir);
			$script_view = "<?php \$this->load->view('../../include/header'); ?>\n";
			$script_view .='<div class="container-fluid">
							  <?php echo breadcrumb($this->uri->segment(1)); ?>
							  <div class="row">
							    <div class="col-md-12">
							      <h1><?php echo menu_name($this->uri->segment(1)); ?></h1>
							    </div>  
							  </div>
							  <br>
							  <div class="row">
						        <div class="col-md-12">
						            <div class="panel panel-info">
						                <div class="panel-body">
						                Content Here ....
						                </div>
					                </div>
				                </div>
				               </div>
							</div>';
			$script_view .= "\n";
			$script_view .= "<?php \$this->load->view('../../include/footer'); ?>\n";
			$script_view .= "<script src='<?php echo base_url('application/modules/".$dir."/views/".strtolower($dir).".js')?>'></script>";
			$script_view .= "\n \n";
			$script_view .= "<!-- End of file ".$view_name.".php -->\n";
			$script_view .= "<!-- Location: ./application/modules/".$dir."/views/".$view_name.".php -->\n";
 
			write_file($path_view.$view_name.'.php',$script_view);

			$script_view_js = "";
			write_file($path_view.strtolower($dir).'.js',$script_view_js);
		}
		history('create module ?path='.$path_module);
		
	}

	public function insert_test_menu($total){
		for($i=0;$i<$total;$i++)
		{
			$this->db->query("INSERT INTO menu (`name`, `path`, parent_id, weight, icon, flag_active)
								SELECT `name`, `path`, parent_id, weight, icon, flag_active FROM menu WHERE menu_id=1;");
		}
	}

}

/* End of file Menu.php */
/* Location: ./application/modules/Menu/controllers/Menu.php */