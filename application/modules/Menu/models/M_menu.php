<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_menu extends CI_Model {

    public function get_menu()
    {
        return $this->db->get_where('menu',array('flag_active'=>'Y'));
    }

    public function get_menu_parent()
    {
        $this->db->select('*');
        $this->db->from('menu');
        $this->db->where('parent_id','0');
        $this->db->order_by('weight','ASC');
        return $this->db->get();
    }

    public function get_menu_child()
    {
        $this->db->select('*');
        $this->db->from('menu');
        $this->db->where('parent_id <>','0');
        $this->db->order_by('weight','ASC');
        return $this->db->get();
    }

    public function get_menu_icon()
	{
		$get = $this->db->get('icon');
		return $get;
	}

	public function menu_insert($data)
	{
		$insert = $this->db->insert('menu',$data);

		$result = ($insert == true)?1:0;
        return $result;
	}

    public function menu_check_path($path)
    {
        $check = $this->db->get_where('menu',array('path'=>$path,'path <>'=>'javascript:void(0)'));

        $result = ($check->num_rows() > 0)?1:0;
        return $result;
    }

    public function menu_by_id($id)
    {   
        $get = $this->db->get_where('menu',array('menu_id'=>$id));
        return $get;
    }

    public function menu_update($data,$where = array())
    {
        $update = $this->db->update('menu',$data,$where);

        $result = ($update == true)?1:0;
        return $result;
    }

    public function menu_delete($id)
    {
        $delete = $this->db->delete('menu',array('menu_id'=>$id));

        $result = ($delete == true)?1:0;
        return $result;
    }

}

/* End of file M_menu.php */
/* Location: ./application/modules/Menu/models/M_menu.php */