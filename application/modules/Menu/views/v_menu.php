<?php $this->load->view('../../include/header'); ?>
<div class="container-fluid" >
    <?php echo breadcrumb($this->uri->segment(1)); ?>
    <div class="row">
        <div class="col-md-12">
          <h2><?php echo menu_name($this->uri->segment(1)); ?></h2>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <button type="button" class="btn btn-blue pull-right" onclick="move_page('<?php echo site_url('Menu/add'); ?>')">
                <i class="fa fa-plus"></i> 
                Add
            </button>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-12" >
            <table class="table table-condensed table-bordered table-hover table-striped " id="table-1" >
                <thead>
                    <tr>
                        <th width="5%">No.</th>
                        <th>Name</th>
                        <th data-hide="phone">Path</th>
                        <th data-hide="phone">Icon</th>
                        <th data-hide="phone">Active</th>
                        <th width="10%">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        if(!empty($data_parent_menu))
                        {   
                            $no=1;
                            foreach ($data_parent_menu as $key => $parent) {

                                if($parent['flag_active'] == 'Y')
                                {
                                    $status_parent = "<span class='badge badge-warning badge-roundless tooltip-primary' data-toggle='tooltip' data-placement='right' title='' data-original-title='Active'><i class='fa fa-check' ></i></span>";
                                }
                                else
                                {
                                    $status_parent = "<span class='badge badge-roundless tooltip-primary' data-toggle='tooltip' data-placement='right' title='' data-original-title='Not Active'><i class='fa fa-check' ></i></span>";
                                }

                                echo "<tr>
                                        <td align='center'>".$no++.".</td>
                                        <td><b>".$parent['name']."</b></td>
                                        <td>".$parent['path']."</td>
                                        <td><i class='fa ".$parent['icon']."'></i> ".$parent['icon']."</td>
                                        <td>".$status_parent."</td>
                                        <td  align='center'>";
                                        if($access_menu['update'] == 'Y')
                                        {
                                            echo "<button type='button' class='btn btn-info btn-xs tooltip-primary' data-toggle='tooltip' data-placement='left' title='' data-original-title='Edit' onclick='move_page(\"".site_url($this->uri->segment(1).'/edit/?id='.encrypt($parent['menu_id']))."\")'><i class='fa fa-pencil-square-o'></i></button> ";
                                        }
                                        if($access_menu['delete'] == 'Y')
                                        {
                                            echo "<button type='button' class='btn btn-danger btn-xs tooltip-primary' data-toggle='tooltip' data-placement='right' title='' data-original-title='Delete' onclick='del(\"".encrypt($parent['menu_id'])."\",\"".$parent['name']."\")'><i class='fa fa-trash-o'></i></button> ";
                                        }
                                    echo "</td>
                                      </tr>";
                                if(isset($data_child_menu[$parent['menu_id']]))
                                {
                                    foreach ($data_child_menu[$parent['menu_id']] as $key => $child1) 
                                    {
                                        
                                        if($child1['flag_active'] == 'Y')
                                        {
                                            $status_child1 = "<span class='badge badge-warning badge-roundless tooltip-primary' data-toggle='tooltip' data-placement='right' title='' data-original-title='Active'><i class='fa fa-check' ></i></span>";
                                        }
                                        else
                                        {
                                            $status_child1 = "<span class='badge badge-roundless tooltip-primary' data-toggle='tooltip' data-placement='right' title='' data-original-title='Not Active'><i class='fa fa-check' ></i></span>";
                                        }
                                    echo "<tr>
                                            <td align='center'>".$no++.".</td>
                                            <td>&nbsp;&nbsp;&nbsp;".$child1['name']."</td>
                                            <td>".$child1['path']."</td>
                                            <td><i class='fa ".$child1['icon']."'></i> ".$child1['icon']."</td>
                                            <td>".$status_child1."</td>
                                            <td  align='center'>";
                                            if($access_menu['update'] == 'Y')
                                            {
                                                echo "<button type='button' class='btn btn-info btn-xs tooltip-primary' data-toggle='tooltip' data-placement='left' title='' data-original-title='Edit' onclick='move_page(\"".site_url($this->uri->segment(1).'/edit/?id='.encrypt($child1['menu_id']))."\")'><i class='fa fa-pencil-square-o'></i></button> ";
                                            }

                                            if($access_menu['delete'] == 'Y')
                                            {
                                                echo "<button type='button' class='btn btn-danger btn-xs tooltip-primary' data-toggle='tooltip' data-placement='right' title='' data-original-title='Delete' onclick='del(\"".encrypt($child1['menu_id'])."\",\"".$child1['name']."\")'><i class='fa fa-trash-o'></i></button> ";
                                            }
                                        echo "</td>
                                          </tr>";

                                        if(isset($data_child_menu[$child1['menu_id']]))
                                        {
                                            foreach ($data_child_menu[$child1['menu_id']] as $key => $child2) 
                                            {
                                                
                                                if($child2['flag_active'] == 'Y')
                                                {
                                                    $status_child2 = "<span class='badge badge-warning  badge-roundless tooltip-primary' data-toggle='tooltip' data-placement='right' title='' data-original-title='Active'><i class='fa fa-check' ></i></span>";
                                                }
                                                else
                                                {
                                                    $status_child2 = "<span class='badge badge-roundless tooltip-primary' data-toggle='tooltip' data-placement='right' title='' data-original-title='Not Active'><i class='fa fa-times'></i></span>";
                                                }
                                            echo "<tr>
                                                    <td align='center'>".$no++.".</td>
                                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$child2['name']."</td>
                                                    <td>".$child2['path']."</td>
                                                    <td><i class='fa ".$child2['icon']."'></i> ".$child2['icon']."</td>
                                                    <td>".$status_child2."</td>
                                                    <td  align='center'>";
                                                    if($access_menu['update'] == 'Y')
                                                    {
                                                        echo "<button type='button' class='btn btn-info btn-xs tooltip-primary' data-toggle='tooltip' data-placement='left' title='' data-original-title='Edit' onclick='move_page(\"".site_url($this->uri->segment(1).'/edit?id='.encrypt($child2['menu_id']))."\")'><i class='fa fa-pencil-square-o'></i></button> ";
                                                    }

                                                    if($access_menu['delete'] == 'Y')
                                                    {
                                                        echo "<button type='button' class='btn btn-danger btn-xs tooltip-primary' data-toggle='tooltip' data-placement='right' title='' data-original-title='Delete' onclick='del(\"".encrypt($child2['menu_id'])."\",\"".$child2['name']."\")'><i class='fa fa-trash-o'></i></button> ";
                                                    }
                                                echo "</td>
                                                  </tr>";
                                            }
                                        }
                                    }
                                }
                            }
                        } 
                        else
                        {
                            echo "<tr><td align='center' colspan='6'>Empty Data</td></tr>";
                        }
                    ?>
                    
                </tbody>
            </table>
        </div>  
    </div>
</div>
<?php $this->load->view('../../include/footer'); ?>
<script src="<?php echo base_url('application/modules/Menu/views/menu.js')?>"></script> 
<!-- End of file v_menu.php -->
<!-- Location: ./application/modules/Menu/views/v_menu.php -->