var responsiveHelper;
var breakpointDefinition = {
    tablet: 1024,
    phone : 480
};
var tableContainer;

jQuery(document).ready(function($)
{
    tableContainer = $("#table-1");
    
    tableContainer.dataTable({
        "sPaginationType": "bootstrap",
        "aLengthMenu": [
        [10,25, 50, 100, -1],
        [10,25, 50, 100, "All"]
        ],
        "iDisplayLength": -1,
        "bStateSave": true,

        // Responsive Settings
        "bAutoWidth"     : true,
        fnPreDrawCallback: function () {
            // Initialize the responsive datatables helper once.
            if (!responsiveHelper) {
                responsiveHelper = new ResponsiveDatatablesHelper(tableContainer, breakpointDefinition);
            }
        },
        fnRowCallback  : function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            responsiveHelper.createExpandIcon(nRow);
        },
        fnDrawCallback : function (oSettings) {
            responsiveHelper.respond();
        }
    });
    
    $(".dataTables_wrapper select").select2({
        minimumResultsForSearch: -1
    });

    $("#save").click(function(){
        if($(".validate").valid()){
            $("#form1").submit();
        }
    });

    $("#form1").submit(function(e){
        e.preventDefault();
        $.ajax({
            url : site_url + active_controller + '/save',
            type : 'POST',
            data : new FormData(this),
            processData : false, 
            contentType : false, 
            beforeSend: function(){
                $('#save').loading('start');
            },
            success: function(response){
                res = JSON.parse(response);
                if(res.message == 'success'){
                    swal("Success", "Thanks You.", "success");
                }else if(res.message == 'failed' && res.errors.path != ""){
                    $("#path").parent().parent().addClass('validate-has-error');
                    $("#path").parent().parent().append('<span for="name" class="validate-has-error">'+res.errors.path+'</span>');
                }else{
                    swal("Failed !", "Your proccess is failed", "error");
                }

                $('#save').loading('stop');      
            },
            error: function(){
              $('#save').loading('stop');      
              swal("Error !", "Your proccess is error", "error");

            }
        });
    });

    $('input.icheck-3').iCheck({
        checkboxClass: 'icheckbox_minimal-green',
        radioClass: 'iradio_minimal-green'
    });

    $('input.icheck-1').iCheck({
        checkboxClass: 'icheckbox_minimal-blue',
        radioClass: 'iradio_minimal-blue'
    });

    // $("#icon").change(function(){
    //     $("#icon-view").addClass($(this).val());
    // });

    
});

function del(id,nama){
    swal({
        title: "Delete "+nama+" ?",
        text: "Click Delete. ",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Yes, Delete !",
        cancelButtonText: "No, Cancel !",
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
        closeOnCancel: false
    },
    function(isConfirm) {
        if (isConfirm) {
            $.ajax({
                  url : site_url + active_controller + "/delete?id="+id,
                  success : function(response){
                    var res = JSON.parse(response);
                    if(res.message == 'success'){
                       swal({
                                title: "Success",
                                text: "Deleted, Thanks You.",
                                type: "success",
                                showCancelButton: true,
                                showCancelButton: false
                            },
                            function(isConfirm) {
                                location.reload();
                        });
                    }else{
                      swal("Failed !", "Deleting data", "error");
                    }
                  },
                  error : function(){
                    swal("Error !", "Failed", "error");
                  }
            });
        } else {
            swal("Cancelled", "Your data is safe.", "error");
        }
    });
}

