<?php $this->load->view('../../include/header'); ?>
<div class="container-fluid">
  <?php echo breadcrumb($this->uri->segment(1),$this->uri->segment(2)); ?>
    <div class="row">
        <div class="col-md-12">
          <h2>Form Edit Menu</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <button type="button" class="btn btn-primary pull-right" onclick="goBack()">
                <i class="fa fa-arrow-left"></i>
                Back
            </button>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-info">
                <div class="panel-body">
                  <div class="row">
                    <div class="col-md-12">
                      <form role="form" id="form1" method="post" class="validate">
                        <input type="hidden" name="id" value="<?php echo $data_menu_by_id['menu_id']; ?>">
                        <input type="hidden" name="process" value="edit">
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Name</label>
                              <input type="text" class="form-control" name="name" id="name" value="<?php echo $data_menu_by_id['name']; ?>" data-validate="required" placeholder="Name menu" >
                            </div>             
                          </div> 
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Parent Menu</label>
                              <select name="parent" id="parent" class="select2" data-allow-clear="true">
                                <option value="0">Select an option</option>
                                <?php foreach ($data_parent_menu as $value):
                                        $selected = ($data_menu_by_id['parent_id'] == $value['menu_id']) ? "selected":"" ; 
                                        echo "<option value='".$value['menu_id']."' ".$selected.">".$value['name']."</option>";
                                      endforeach; ?>
                              </select>
                            </div>
                          </div>           
                        </div>
                        <div class="row"> 
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Path</label>
                              <div class="input-group">
                                <input type="text" class="form-control" name="path" id="path" value="<?php echo $data_menu_by_id['path']; ?>" data-validate="required"  placeholder="example: dashboard">
                                <span class="input-group-addon" id="void" style="cursor: pointer;">void(0)</span>
                                <script type="text/javascript">
                                  $("#void").click(function(){$("#path").val('javascript:void(0)')});
                                </script>
                              </div>
                            </div>             
                          </div> 
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Icon Menu</label> &nbsp;&nbsp; { &nbsp;<i class="fa <?php echo $data_menu_by_id['icon'];?>" id="icon-view"></i>&nbsp; }
                              <select class="select2"  id="icon" name="icon">
                                <option value="">Select an option</option>
                                <?php foreach ($data_icon as $val): 
                                        $selected = ($data_menu_by_id['icon'] == $val['icon_nm']) ? "selected":"";
                                        echo "<option value='$val[icon_nm]' ".$selected."> $val[icon_nm]</option>";
                                      endforeach ?>
                              </select>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                                <br>
                                <label for="minimal-checkbox-1-3">Active Menu</label> &nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="checkbox" class="icheck-3" name="active" <?php if($data_menu_by_id['flag_active'] == 'Y'){echo "checked";} ?> id="minimal-checkbox-1-3">
                                <p>*) Check to actived.</p>
                            </div>             
                          </div>
                          <div class="col-md-2">
                            <div class="form-group">
                                <label>Weight</label>
                                <select class="select2"  id="weight" name="weight">
                                  <?php 
                                      for($i=1;$i<100;$i++)
                                      {
                                          $selected = ($data_menu_by_id['weight'] == $i) ? "selected":"";
                                          echo "<option value='".$i."' ".$selected.">".$i."</option>";
                                      }
                                  ?>
                                </select>
                            </div>             
                          </div> 
                          <div class="col-md-2">
                            <div class="form-group">
                                <br>
                                <label for="minimal-checkbox-1-1">Collapsed Menu</label> &nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="checkbox" class="icheck-1" name="collapsed_sidebar" <?php if($data_menu_by_id['collapsed_sidebar'] == 'Y'){echo "checked";} ?> id="minimal-checkbox-1-1">
                                <p>*) Check to actived.</p>
                            </div>             
                          </div> 
                        </div>
                        <div class="row">
                          <div class="col-md-12"> 
                            <div class="text-center">
                              <button type="button" class="btn btn-success" id="save" ><i class="fa fa-save"></i> Save</button>
                            </div>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
          <blockquote class="blockquote-default">
            <p>
              <small><strong>Created By </strong>: <?php echo $data_menu_by_id['created_by']." - ".view_datetime($data_menu_by_id['created_date']); ?></small>
            </p>
            <p>
              <small><strong>Modified By</strong> : <?php echo $data_menu_by_id['modified_by']." - ".view_datetime($data_menu_by_id['modified_date']); ?></small>
            </p>
          </blockquote>
        </div>
    </div>
</div>
<?php $this->load->view('../../include/footer'); ?>
<script src="<?php echo base_url('application/modules/Menu/views/menu.js')?>"></script> 