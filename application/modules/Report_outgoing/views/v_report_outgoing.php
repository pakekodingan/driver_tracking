<?php $this->load->view('../../include/header'); ?>
<div class="container-fluid">
  <?php echo breadcrumb($this->uri->segment(1)); ?>
  <div class="row">
    <div class="col-md-12">
      <h1><?php echo menu_name($this->uri->segment(1)); ?></h1>
    </div> 
  </div>
  <br>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <input type="text" name="" class="form-control input-sm daterange" id="search-date" placeholder="Tanggal">
        </div>
    </div>
    <div class="col-md-1">
        <div class="form-group">
            <label>&nbsp;</label>
            <button class="btn btn-sm btn-blue" onclick="refresh_data()"><i class="fa fa-search"></i> Cari</button>
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group">
            <label>&nbsp;</label>
            <button class="btn btn-sm btn-success " id="btn-export-excel"><i class="fa fa-download"></i> Export to excel</button>
        </div>
    </div>
</div>
  <div class="row">
    <div class="col-md-12">
        <div class="card" style="overflow-x: scroll;">
            <table class="table table-condensed table-bordered mtable-hover table-striped " id="table-1" style="width: 100%;font-size: 8px;">
                <thead>
                    <tr>
                        <th width="1%">No.</th>
                        <th>NO. PERJALANAN</th>
                        <th>TANGGAL</th>
                        <th>RUTE</th>
                        <th>PLAT NO</th>
                        <th>TRANSPORTER</th>
                        <th>START BERANGKAT</th>
                        <th style="border-right: 2px solid blue;">KM</th>
                        <th>IN DP1</th>
                        <th>KM</th>
                        <th style="border-right: 2px solid blue;">OUT DP1</th>
                        <th>IN DP2</th>
                        <th>KM</th>
                        <th style="border-right: 2px solid blue;">OUT DP2</th>
                        <th>IN DP3</th>
                        <th>KM</th>
                        <th style="border-right: 2px solid blue;">OUT DP3</th>
                        <th>IN DP4</th>
                        <th>KM</th>
                        <th style="border-right: 2px solid blue;">OUT DP4</th>
                        <th>IN DP5</th>
                        <th>KM</th>
                        <th style="border-right: 2px solid blue;">OUT DP5</th>
                        <th>IN DP6</th>
                        <th>KM</th>
                        <th style="border-right: 2px solid blue;">OUT DP6</th>
                        <th>FINISH</th>
                        <th>KM</th>
                    </tr>
                </thead>
                <tbody></tbody>
                <tfoot></tfoot> 
            </table>
        </div>
    </div>
   </div>
</div>
<?php $this->load->view('../../include/footer'); ?>
<script src='<?php echo base_url('application/modules/Report_outgoing/views/report_outgoing.js')?>'></script>
 
<!-- End of file v_report_outgoing.php -->
<!-- Location: ./application/modules/Report_outgoing/views/v_report_outgoing.php -->
