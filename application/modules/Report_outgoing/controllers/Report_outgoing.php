<?php 
defined('BASEPATH') OR exit('No direct script access allowed'); 
 
class Report_outgoing extends CI_Controller { 
 
	public function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('isLogin')){
			redirect('Login');
		}
		$this->load->model('M_report_outgoing','report_outgoing_model');
	}
 
	public function index()
	{
		$path = $this->uri->segment(1);
		$access_menu = access_menu($path);
		if($access_menu['read'] == 'Y')
		{
			history('view report outgoing');
			$this->load->view('v_report_outgoing');
		}
		else
		{
			history('view report outgoing :denied');
			$this->load->view('../../include/access_denied');
		}
	}
 	
 	public function get_data() {
 		if(!empty($this->input->get('param1')))
 		{
 			$search_date = explode(' - ',$this->input->get('param1'));
	 		$start_date = save_date($search_date[0]);
	 		$end_date = save_date($search_date[1]);
 		}
 		else
 		{
 			$start_date = "";
	 		$end_date = "";
 		}

 		$path = $this->uri->segment(1);
		$access_menu = access_menu($path);

		$active_controller = $this->uri->segment(1);
		/* Array of database columns which should be read and sent back to DataTables. Use a space where
			         * you want to insert a non-database field (for example a counter or static image)
		*/

		$acolumns = array('outgoing_header.outgoing_id', 'outgoing_header.start_date', 'outgoing_header.end_date', 'outgoing_header.rute_id','outgoing_header.destination', 'outgoing_header.plat_number', 'outgoing_header.rit', 'outgoing_header.static','outgoing_header.start_km','outgoing_header.end_km', 'outgoing_header.employee_id', 'outgoing_header.note', 'employee.employee_nik', 'employee_name');

		// DB table to use
		$stable = 'outgoing_header';
		//

		$idisplay_start = $this->input->get_post('iDisplayStart', true);
		$idisplay_length = $this->input->get_post('iDisplayLength', true);
		$isort_col_0 = $this->input->get_post('iSortCol_0', true);
		$isorting_cols = $this->input->get_post('iSortingCols', true);
		$isearch = $this->input->get_post('sSearch', true);
		$secho = $this->input->get_post('sEcho', true);

		// Paging
		if (isset($idisplay_start) && $idisplay_length != '-1') {
			$this->db->limit($this->db->escape_str($idisplay_length), $this->db->escape_str($idisplay_start));
		}

		// Ordering
		if (isset($isort_col_0)) {
			for ($i = 0; $i < intval($isorting_cols); $i++) {
				$isort_col = $this->input->get_post('iSortCol_' . $i, true);
				$bsortable = $this->input->get_post('bSortable_' . intval($isort_col), true);
				$ssort_dir = $this->input->get_post('sSortDir_' . $i, true);

				if ($bsortable == 'true') {
					$this->db->order_by($acolumns[intval($this->db->escape_str($isort_col))], $this->db->escape_str($ssort_dir));
				}
			}
		}

		/*
			         * Filtering
			         * NOTE this does not match the built-in DataTables filtering which does it
			         * word by word on any field. It's possible to do here, but concerned about efficiency
			         * on very large tables, and MySQL's regex functionality is very limited
		*/
		if (isset($isearch) && !empty($isearch)) {
			for ($i = 0; $i < count($acolumns); $i++) {
				$isearchable = $this->input->get_post('bSearchable_' . $i, true);

				// Individual column filtering
				if (isset($isearchable) && $isearchable == 'true') {
					$this->db->or_like($acolumns[$i], $this->db->escape_like_str($isearch));
				}
			}
		}

		// Select Data
		$this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $acolumns)), false);
		$this->db->from('outgoing_header');
		$this->db->join('employee','outgoing_header.employee_id = employee.employee_id');
		if(!empty($start_date) && !empty($end_date))
		{
			$this->db->where('outgoing_header.created_date >=',$start_date);
			$this->db->where('outgoing_header.created_date <=',$end_date);
		} 
		$rresult = $this->db->get();

		// Data set length after filtering
		$this->db->select('FOUND_ROWS() AS found_rows');
		$ifiltered_total = $this->db->get()->row()->found_rows;

		// Total data set length
		$this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $acolumns)), false);
		$this->db->from('outgoing_header');
		$this->db->join('employee','outgoing_header.employee_id = employee.employee_id');
		if(!empty($start_date) && !empty($end_date))
		{
			$this->db->where('outgoing_header.created_date >=',$start_date);
			$this->db->where('outgoing_header.created_date <=',$end_date);
		}
		$itotal = $this->db->count_all_results();

		// Output
		$output = array(
			'sEcho' => intval($secho),
			'iTotalRecords' => $itotal,
			'iTotalDisplayRecords' => $ifiltered_total,
			'aaData' => array(),
		);

		$this->db->select('*');
		$this->db->from('outgoing_detail');
		$this->db->order_by('outgoing_id','ASC');
		$this->db->order_by('outgoing_detail_id','ASC');
		$query_outgoing_detail  = $this->db->get()->result_array();

		$arr_data = array();
		foreach ($query_outgoing_detail as $key => $value) 
		{
			$arr_data['outgoing_id'][$value['outgoing_id']][$value['type_outgoing']][$value['location']] = $value['outgoing_id'];
			$arr_data['type_outgoing'][$value['outgoing_id']][$value['type_outgoing']][$value['location']] = $value['type_outgoing'];
			$arr_data['dp_date'][$value['outgoing_id']][$value['type_outgoing']][$value['location']] = $value['dp_date'];
			$arr_data['dp_time'][$value['outgoing_id']][$value['type_outgoing']][$value['location']] = $value['dp_time'];
			$arr_data['km'][$value['outgoing_id']][$value['type_outgoing']][$value['location']] = $value['km'];
		}

		// dd($arr_data);

		$no = $idisplay_start;
		foreach ($rresult->result_array() as $arow) 
		{
			$no++;
			$row = array();
			$row[] = "<center>" . $no . ".</center>";
			$row[] = $arow['outgoing_id'];
			$row[] = view_date($arow['start_date']);
			$row[] = $arow['rute_id'];
			$row[] = $arow['plat_number'];
			$row[] = $arow['employee_name'];
			$row[] = "<center>".view_time($arow['start_date'])."</center>";
			$row[] = $arow['start_km'];

			$row[] = (isset($arr_data['dp_time'][$arow['outgoing_id']]['IN']['DP1'])) ? "<center>".view_time($arr_data['dp_time'][$arow['outgoing_id']]['IN']['DP1'])."</center>" : "";
			$row[] = (isset($arr_data['km'][$arow['outgoing_id']]['IN']['DP1'])) ? "<center><u>".$arr_data['km'][$arow['outgoing_id']]['IN']['DP1']."</u></center>" : "";
			$row[] = (isset($arr_data['dp_time'][$arow['outgoing_id']]['OUT']['DP1'])) ? "<center>".view_time($arr_data['dp_time'][$arow['outgoing_id']]['OUT']['DP1'])."</center>" : "";

			$row[] = (isset($arr_data['dp_time'][$arow['outgoing_id']]['IN']['DP2'])) ? "<center>".view_time($arr_data['dp_time'][$arow['outgoing_id']]['IN']['DP2'])."</center>" : "";
			$row[] = (isset($arr_data['km'][$arow['outgoing_id']]['IN']['DP2'])) ? "<center><u>".$arr_data['km'][$arow['outgoing_id']]['IN']['DP2']."</u></center>" : "";
			$row[] = (isset($arr_data['dp_time'][$arow['outgoing_id']]['OUT']['DP2'])) ? "<center>".view_time($arr_data['dp_time'][$arow['outgoing_id']]['OUT']['DP2'])."</center>" : "";

			$row[] = (isset($arr_data['dp_time'][$arow['outgoing_id']]['IN']['DP3'])) ? "<center>".view_time($arr_data['dp_time'][$arow['outgoing_id']]['IN']['DP3'])."</center>" : "";
			$row[] = (isset($arr_data['km'][$arow['outgoing_id']]['IN']['DP3'])) ? "<center><u>".$arr_data['km'][$arow['outgoing_id']]['IN']['DP3']."</u></center>" : "";
			$row[] = (isset($arr_data['dp_time'][$arow['outgoing_id']]['OUT']['DP3'])) ? "<center>".view_time($arr_data['dp_time'][$arow['outgoing_id']]['OUT']['DP3'])."</center>" : "";

			$row[] = (isset($arr_data['dp_time'][$arow['outgoing_id']]['IN']['DP4'])) ? "<center>".view_time($arr_data['dp_time'][$arow['outgoing_id']]['IN']['DP4'])."</center>" : "";
			$row[] = (isset($arr_data['km'][$arow['outgoing_id']]['IN']['DP4'])) ? "<center><u>".$arr_data['km'][$arow['outgoing_id']]['IN']['DP4']."</u></center>" : "";
			$row[] = (isset($arr_data['dp_time'][$arow['outgoing_id']]['OUT']['DP4'])) ? "<center>".view_time($arr_data['dp_time'][$arow['outgoing_id']]['OUT']['DP4'])."</center>" : "";

			$row[] = (isset($arr_data['dp_time'][$arow['outgoing_id']]['IN']['DP5'])) ? "<center>".view_time($arr_data['dp_time'][$arow['outgoing_id']]['IN']['DP5'])."</center>" : "";
			$row[] = (isset($arr_data['km'][$arow['outgoing_id']]['IN']['DP5'])) ? "<center><u>".$arr_data['km'][$arow['outgoing_id']]['IN']['DP5']."</u></center>" : "";
			$row[] = (isset($arr_data['dp_time'][$arow['outgoing_id']]['OUT']['DP5'])) ? "<center>".view_time($arr_data['dp_time'][$arow['outgoing_id']]['OUT']['DP5'])."</center>" : "";

			$row[] = (isset($arr_data['dp_time'][$arow['outgoing_id']]['IN']['DP6'])) ? "<center>".view_time($arr_data['dp_time'][$arow['outgoing_id']]['IN']['DP6'])."</center>" : "";
			$row[] = (isset($arr_data['km'][$arow['outgoing_id']]['IN']['DP6'])) ? "<center><u>".$arr_data['km'][$arow['outgoing_id']]['IN']['DP6']."<u/></center>" : "";
			$row[] = (isset($arr_data['dp_time'][$arow['outgoing_id']]['OUT']['DP6'])) ? "<center>".view_time($arr_data['dp_time'][$arow['outgoing_id']]['OUT']['DP6'])."</center>" : "";

			
			$row[] = (isset($arr_data['dp_time'][$arow['outgoing_id']]['FINISH']['END'])) ? "<center>".view_time($arr_data['dp_time'][$arow['outgoing_id']]['FINISH']['END'])."</center>" : "";
			$row[] = (isset($arr_data['km'][$arow['outgoing_id']]['FINISH']['END'])) ? "<center><u>".$arr_data['km'][$arow['outgoing_id']]['FINISH']['END']."</u></center>" : "";

			$output['aaData'][] = $row;
		}
		// dd( $output['aaData']);

		history('view data vehicle ?search=' . $isearch);
		echo json_encode($output);
	}

	public function export()
	{
		$this->load->library("Excel");
	    $object = new PHPExcel();
		
	    $style_col = array(
	      'font' => array('bold' => true), 
	      'alignment' => array(
	        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER 
	      ),
	      'borders' => array(
	        'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), 
	        'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), 
	        'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), 
	        'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN)
	      )
	  	);
		
	    $style_row = array(
	      'alignment' => array(
	      	'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER 
	      ),
	      'borders' => array(
	        'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), 
	        'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), 
	        'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
	        'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) 
	      )
	  	);

	  	$style_row2 = array(
	      'alignment' => array(
	      	'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER 
	      ),
	      'borders' => array(
	        'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), 
	        'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), 
	        'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
	        'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) 
	      ),
	      'fill' => array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'color' => array('rgb' => 'f8f8f8')
	       )
	  	);

	  	$object->setActiveSheetIndex(0)->setCellValue('A1', "REPORT KEBERANGKATAN"); 
	    $object->getActiveSheet()->mergeCells('A1:E1'); 
	    $object->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); 
	    $object->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); 
	    $object->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	    $object->setActiveSheetIndex(0)->setCellValue('A3', "NO. PERJALANAN"); 
	    $object->setActiveSheetIndex(0)->setCellValue('B3', "TANGGAL");
	    $object->setActiveSheetIndex(0)->setCellValue('C3', "RUTE");
	    $object->setActiveSheetIndex(0)->setCellValue('D3', "PLAT NO");
	    $object->setActiveSheetIndex(0)->setCellValue('E3', "TRANSPORTER");
	    $object->setActiveSheetIndex(0)->setCellValue('F3', "START");
	    $object->setActiveSheetIndex(0)->setCellValue('G3', "KM");
	    $object->setActiveSheetIndex(0)->setCellValue('H3', "IN DP1");
	    $object->setActiveSheetIndex(0)->setCellValue('I3', "KM");
	    $object->setActiveSheetIndex(0)->setCellValue('J3', "OUT DP1");
	    $object->setActiveSheetIndex(0)->setCellValue('K3', "IN DP2");
	    $object->setActiveSheetIndex(0)->setCellValue('L3', "KM");
	    $object->setActiveSheetIndex(0)->setCellValue('M3', "OUT DP2");
	    $object->setActiveSheetIndex(0)->setCellValue('N3', "IN DP3");
	    $object->setActiveSheetIndex(0)->setCellValue('O3', "KM");
	    $object->setActiveSheetIndex(0)->setCellValue('P3', "OUT DP3");
	    $object->setActiveSheetIndex(0)->setCellValue('Q3', "IN DP4");
	    $object->setActiveSheetIndex(0)->setCellValue('R3', "KM");
	    $object->setActiveSheetIndex(0)->setCellValue('S3', "OUT DP4");
	    $object->setActiveSheetIndex(0)->setCellValue('T3', "IN DP5");
	    $object->setActiveSheetIndex(0)->setCellValue('U3', "KM");
	    $object->setActiveSheetIndex(0)->setCellValue('V3', "OUT DP5");
	    $object->setActiveSheetIndex(0)->setCellValue('W3', "IN DP6");
	    $object->setActiveSheetIndex(0)->setCellValue('X3', "KM");
	    $object->setActiveSheetIndex(0)->setCellValue('Y3', "OUT DP6");
	    $object->setActiveSheetIndex(0)->setCellValue('Z3', "FINISH");
	    $object->setActiveSheetIndex(0)->setCellValue('AA3', "KM");
	    $object->getActiveSheet()->getStyle('A3')->applyFromArray($style_col);
	    $object->getActiveSheet()->getStyle('B3')->applyFromArray($style_col);
	    $object->getActiveSheet()->getStyle('C3')->applyFromArray($style_col);
	    $object->getActiveSheet()->getStyle('D3')->applyFromArray($style_col);
	    $object->getActiveSheet()->getStyle('E3')->applyFromArray($style_col);
	    $object->getActiveSheet()->getStyle('F3')->applyFromArray($style_col);
	    $object->getActiveSheet()->getStyle('G3')->applyFromArray($style_col);
	    $object->getActiveSheet()->getStyle('H3')->applyFromArray($style_col);
	    $object->getActiveSheet()->getStyle('I3')->applyFromArray($style_col);
	    $object->getActiveSheet()->getStyle('J3')->applyFromArray($style_col);
	    $object->getActiveSheet()->getStyle('K3')->applyFromArray($style_col);
	    $object->getActiveSheet()->getStyle('L3')->applyFromArray($style_col);
	    $object->getActiveSheet()->getStyle('M3')->applyFromArray($style_col);
	    $object->getActiveSheet()->getStyle('N3')->applyFromArray($style_col);
	    $object->getActiveSheet()->getStyle('O3')->applyFromArray($style_col);
	    $object->getActiveSheet()->getStyle('P3')->applyFromArray($style_col);
	    $object->getActiveSheet()->getStyle('Q3')->applyFromArray($style_col);
	    $object->getActiveSheet()->getStyle('R3')->applyFromArray($style_col);
	    $object->getActiveSheet()->getStyle('S3')->applyFromArray($style_col);
	    $object->getActiveSheet()->getStyle('T3')->applyFromArray($style_col);
	    $object->getActiveSheet()->getStyle('U3')->applyFromArray($style_col);
	    $object->getActiveSheet()->getStyle('V3')->applyFromArray($style_col);
	    $object->getActiveSheet()->getStyle('W3')->applyFromArray($style_col);
	    $object->getActiveSheet()->getStyle('X3')->applyFromArray($style_col);
	    $object->getActiveSheet()->getStyle('Y3')->applyFromArray($style_col);
	    $object->getActiveSheet()->getStyle('Z3')->applyFromArray($style_col);
	    $object->getActiveSheet()->getStyle('AA3')->applyFromArray($style_col);


		$this->db->select('*');
		$this->db->from('outgoing_detail');
		$this->db->order_by('outgoing_id','ASC');
		$this->db->order_by('outgoing_detail_id','ASC');
		$query_outgoing_detail  = $this->db->get()->result_array();

		$arr_data = array();
		foreach ($query_outgoing_detail as $key => $value) 
		{
			$arr_data['outgoing_id'][$value['outgoing_id']][$value['type_outgoing']][$value['location']] = $value['outgoing_id'];
			$arr_data['type_outgoing'][$value['outgoing_id']][$value['type_outgoing']][$value['location']] = $value['type_outgoing'];
			$arr_data['dp_date'][$value['outgoing_id']][$value['type_outgoing']][$value['location']] = $value['dp_date'];
			$arr_data['dp_time'][$value['outgoing_id']][$value['type_outgoing']][$value['location']] = $value['dp_time'];
			$arr_data['km'][$value['outgoing_id']][$value['type_outgoing']][$value['location']] = $value['km'];
		}

		// dd($arr_data);

	    $acolumns = array('outgoing_header.outgoing_id', 'outgoing_header.start_date', 'outgoing_header.end_date', 'outgoing_header.rute_id','outgoing_header.destination', 'outgoing_header.plat_number', 'outgoing_header.rit', 'outgoing_header.static','outgoing_header.start_km','outgoing_header.end_km', 'outgoing_header.employee_id', 'outgoing_header.note', 'employee.employee_nik', 'employee_name');
		$this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $acolumns)), false);
		$this->db->from('outgoing_header');
		$this->db->join('employee','outgoing_header.employee_id = employee.employee_id');
		$rresult = $this->db->get()->result_array();
	 	$no = 1; 
	    $numrow = 4;
	    foreach($rresult as $arow){ 
	      $object->setActiveSheetIndex(0)->setCellValue('A'.$numrow, $arow['outgoing_id']);
	      $object->setActiveSheetIndex(0)->setCellValue('B'.$numrow, view_date($arow['start_date']));
	      $object->setActiveSheetIndex(0)->setCellValue('C'.$numrow, $arow['rute_id']);
	      $object->setActiveSheetIndex(0)->setCellValue('D'.$numrow, $arow['plat_number']);
	      $object->setActiveSheetIndex(0)->setCellValue('E'.$numrow, $arow['employee_name']);
	      $object->setActiveSheetIndex(0)->setCellValue('F'.$numrow, view_time($arow['start_date']));
	      $object->setActiveSheetIndex(0)->setCellValue('G'.$numrow, $arow['start_km']);

	      $in_dp1 = (isset($arr_data['dp_time'][$arow['outgoing_id']]['IN']['DP1'])) ? view_time($arr_data['dp_time'][$arow['outgoing_id']]['IN']['DP1']) :"";
	      $km_dp1 = (isset($arr_data['km'][$arow['outgoing_id']]['IN']['DP1'])) ? $arr_data['km'][$arow['outgoing_id']]['IN']['DP1'] : "";
	      $out_dp1 = (isset($arr_data['dp_time'][$arow['outgoing_id']]['OUT']['DP1'])) ? view_time($arr_data['dp_time'][$arow['outgoing_id']]['OUT']['DP1']) : "";
	      $object->setActiveSheetIndex(0)->setCellValue('H'.$numrow, $in_dp1);
	      $object->setActiveSheetIndex(0)->setCellValue('I'.$numrow, $km_dp1);
	      $object->setActiveSheetIndex(0)->setCellValue('J'.$numrow, $out_dp1);

	      $in_dp2 = (isset($arr_data['dp_time'][$arow['outgoing_id']]['IN']['DP2'])) ? view_time($arr_data['dp_time'][$arow['outgoing_id']]['IN']['DP2']) :"";
	      $km_dp2 = (isset($arr_data['km'][$arow['outgoing_id']]['IN']['DP2'])) ? $arr_data['km'][$arow['outgoing_id']]['IN']['DP2'] : "";
	      $out_dp2 = (isset($arr_data['dp_time'][$arow['outgoing_id']]['OUT']['DP2'])) ? view_time($arr_data['dp_time'][$arow['outgoing_id']]['OUT']['DP2']) : "";
	      $object->setActiveSheetIndex(0)->setCellValue('K'.$numrow, $in_dp2);
	      $object->setActiveSheetIndex(0)->setCellValue('L'.$numrow, $km_dp2);
	      $object->setActiveSheetIndex(0)->setCellValue('M'.$numrow, $out_dp2);

	      $in_dp3 = (isset($arr_data['dp_time'][$arow['outgoing_id']]['IN']['DP3'])) ? view_time($arr_data['dp_time'][$arow['outgoing_id']]['IN']['DP3']) :"";
	      $km_dp3 = (isset($arr_data['km'][$arow['outgoing_id']]['IN']['DP3'])) ? $arr_data['km'][$arow['outgoing_id']]['IN']['DP3'] : "";
	      $out_dp3 = (isset($arr_data['dp_time'][$arow['outgoing_id']]['OUT']['DP3'])) ? view_time($arr_data['dp_time'][$arow['outgoing_id']]['OUT']['DP3']) : "";
	      $object->setActiveSheetIndex(0)->setCellValue('N'.$numrow, $in_dp3);
	      $object->setActiveSheetIndex(0)->setCellValue('O'.$numrow, $km_dp3);
	      $object->setActiveSheetIndex(0)->setCellValue('P'.$numrow, $out_dp3);

	      $in_dp4 = (isset($arr_data['dp_time'][$arow['outgoing_id']]['IN']['DP4'])) ? view_time($arr_data['dp_time'][$arow['outgoing_id']]['IN']['DP4']) :"";
	      $km_dp4 = (isset($arr_data['km'][$arow['outgoing_id']]['IN']['DP4'])) ? $arr_data['km'][$arow['outgoing_id']]['IN']['DP4'] : "";
	      $out_dp4 = (isset($arr_data['dp_time'][$arow['outgoing_id']]['OUT']['DP4'])) ? view_time($arr_data['dp_time'][$arow['outgoing_id']]['OUT']['DP4']) : "";
	      $object->setActiveSheetIndex(0)->setCellValue('Q'.$numrow, $in_dp4);
	      $object->setActiveSheetIndex(0)->setCellValue('R'.$numrow, $km_dp4);
	      $object->setActiveSheetIndex(0)->setCellValue('S'.$numrow, $out_dp4);

	      $in_dp5 = (isset($arr_data['dp_time'][$arow['outgoing_id']]['IN']['DP5'])) ? view_time($arr_data['dp_time'][$arow['outgoing_id']]['IN']['DP5']) :"";
	      $km_dp5 = (isset($arr_data['km'][$arow['outgoing_id']]['IN']['DP5'])) ? $arr_data['km'][$arow['outgoing_id']]['IN']['DP5'] : "";
	      $out_dp5 = (isset($arr_data['dp_time'][$arow['outgoing_id']]['OUT']['DP5'])) ? view_time($arr_data['dp_time'][$arow['outgoing_id']]['OUT']['DP5']) : "";
	      $object->setActiveSheetIndex(0)->setCellValue('T'.$numrow, $in_dp5);
	      $object->setActiveSheetIndex(0)->setCellValue('U'.$numrow, $km_dp5);
	      $object->setActiveSheetIndex(0)->setCellValue('V'.$numrow, $out_dp5);

	      $in_dp6 = (isset($arr_data['dp_time'][$arow['outgoing_id']]['IN']['DP6'])) ? view_time($arr_data['dp_time'][$arow['outgoing_id']]['IN']['DP6']) :"";
	      $km_dp6 = (isset($arr_data['km'][$arow['outgoing_id']]['IN']['DP6'])) ? $arr_data['km'][$arow['outgoing_id']]['IN']['DP6'] : "";
	      $out_dp6 = (isset($arr_data['dp_time'][$arow['outgoing_id']]['OUT']['DP6'])) ? view_time($arr_data['dp_time'][$arow['outgoing_id']]['OUT']['DP6']) : "";
	      $object->setActiveSheetIndex(0)->setCellValue('W'.$numrow, $in_dp6);
	      $object->setActiveSheetIndex(0)->setCellValue('X'.$numrow, $km_dp6);
	      $object->setActiveSheetIndex(0)->setCellValue('Y'.$numrow, $out_dp6);

	      $finish_time = (isset($arr_data['dp_time'][$arow['outgoing_id']]['FINISH']['END'])) ? view_time($arr_data['dp_time'][$arow['outgoing_id']]['FINISH']['END']) : "";
		  $finish_km = (isset($arr_data['km'][$arow['outgoing_id']]['FINISH']['END'])) ? $arr_data['km'][$arow['outgoing_id']]['FINISH']['END'] : "";
	      $object->setActiveSheetIndex(0)->setCellValue('Z'.$numrow, $finish_time);
	      $object->setActiveSheetIndex(0)->setCellValue('AA'.$numrow, $finish_km);
	      
	      $object->getActiveSheet()->getStyle('A'.$numrow)->applyFromArray($style_row);
	      $object->getActiveSheet()->getStyle('A'.$numrow)->getNumberFormat()->setFormatCode('0');
	      $object->getActiveSheet()->getStyle('B'.$numrow)->applyFromArray($style_row);
	      $object->getActiveSheet()->getStyle('C'.$numrow)->applyFromArray($style_row);
	      $object->getActiveSheet()->getStyle('D'.$numrow)->applyFromArray($style_row);
	      $object->getActiveSheet()->getStyle('E'.$numrow)->applyFromArray($style_row);
	      $object->getActiveSheet()->getStyle('F'.$numrow)->applyFromArray($style_row);
	      $object->getActiveSheet()->getStyle('G'.$numrow)->applyFromArray($style_row);
	      $object->getActiveSheet()->getStyle('H'.$numrow)->applyFromArray($style_row2);
	      $object->getActiveSheet()->getStyle('I'.$numrow)->applyFromArray($style_row2);
	      $object->getActiveSheet()->getStyle('J'.$numrow)->applyFromArray($style_row2);
	      $object->getActiveSheet()->getStyle('K'.$numrow)->applyFromArray($style_row);
	      $object->getActiveSheet()->getStyle('L'.$numrow)->applyFromArray($style_row);
	      $object->getActiveSheet()->getStyle('M'.$numrow)->applyFromArray($style_row);
	      $object->getActiveSheet()->getStyle('N'.$numrow)->applyFromArray($style_row2);
	      $object->getActiveSheet()->getStyle('O'.$numrow)->applyFromArray($style_row2);
	      $object->getActiveSheet()->getStyle('P'.$numrow)->applyFromArray($style_row2);
	      $object->getActiveSheet()->getStyle('Q'.$numrow)->applyFromArray($style_row);
	      $object->getActiveSheet()->getStyle('R'.$numrow)->applyFromArray($style_row);
	      $object->getActiveSheet()->getStyle('S'.$numrow)->applyFromArray($style_row);
	      $object->getActiveSheet()->getStyle('T'.$numrow)->applyFromArray($style_row2);
	      $object->getActiveSheet()->getStyle('U'.$numrow)->applyFromArray($style_row2);
	      $object->getActiveSheet()->getStyle('V'.$numrow)->applyFromArray($style_row2);
	      $object->getActiveSheet()->getStyle('W'.$numrow)->applyFromArray($style_row);
	      $object->getActiveSheet()->getStyle('X'.$numrow)->applyFromArray($style_row);
	      $object->getActiveSheet()->getStyle('Y'.$numrow)->applyFromArray($style_row);
	      $object->getActiveSheet()->getStyle('Z'.$numrow)->applyFromArray($style_row2);
	      $object->getActiveSheet()->getStyle('AA'.$numrow)->applyFromArray($style_row2);
	      $no++; 
	      $numrow++;
	    }
	    $object->getActiveSheet(0)->freezePane('F3');
	    $object->getActiveSheet(0)->getColumnDimension('A')->setWidth(20); 
	    $object->getActiveSheet(0)->getColumnDimension('B')->setWidth(15);
	    $object->getActiveSheet(0)->getColumnDimension('C')->setWidth(35);
	    $object->getActiveSheet(0)->getColumnDimension('D')->setWidth(20);
	    $object->getActiveSheet(0)->getColumnDimension('E')->setWidth(25);
	    $object->getActiveSheet(0)->getDefaultRowDimension()->setRowHeight(-1);
	    $object->getActiveSheet(0)->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
	    $object->getActiveSheet(0)->setTitle("REPORT KEBERANGKATAN");
	    $object->setActiveSheetIndex(0);
	    $write = PHPExcel_IOFactory::createWriter($object, 'Excel2007');
	    ob_end_clean();
	    
	    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	    header('Content-Disposition: attachment; filename="REPORT KEBERANGKATAN.xlsx"'); 
	    header('Cache-Control: max-age=0');
	    $write->save('php://output');
	}
	
}
 
/* End of file Report_outgoing.php */
/* Location: ./application/modules/Report_outgoing/controllers/Report_outgoing.php */
