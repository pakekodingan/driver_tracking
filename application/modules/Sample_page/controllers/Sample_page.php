<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sample_page extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_sample_page','sample_page_model');
		if(!$this->session->userdata('isLogin')){
			// redirect('login');
		}	
	}

	public function index()
	{
		$this->load->view('v_sample_page');
	}
}
