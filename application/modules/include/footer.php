    <div class="totop"><i class="fa fa-angle-up"></i></div>
    <!-- Footer -->
    <footer class="main">
      &copy; <?php echo date('Y',strtotime(application('build_date'))); ?> <strong><?php echo application('app_name'); ?> - <?php echo application('version'); ?></strong> by <a href="#" target="_blank"><?php echo application('app_detail'); ?></a>
    </footer>
  </div>


<!-- Modal 6 (Long Modal)-->
<div class="modal fade" id="modal-6">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        
      </div>
    </div>
  </div>
</div>
<!---
<div id="chat" class="fixed" data-current-user="Art Ramadani" data-order-by-status="1" data-max-chat-history="25">
	<div class="chat-inner">
		<h2 class="chat-header">
			<a href="#" class="chat-close" data-animate="1"><i class="entypo-cancel"></i></a>
			<i class="entypo-users"></i>
			Chat
			<span class="badge badge-success is-hidden">0</span>
		</h2>
		<div class="chat-group" id="group-1">
			<strong>Favorites</strong>
			
			<a href="#" id="sample-user-123" data-conversation-history="#sample_history"><span class="user-status is-online"></span> <em>Catherine J. Watkins</em></a>
			<a href="#"><span class="user-status is-online"></span> <em>Nicholas R. Walker</em></a>
			<a href="#"><span class="user-status is-busy"></span> <em>Susan J. Best</em></a>
			<a href="#"><span class="user-status is-offline"></span> <em>Brandon S. Young</em></a>
			<a href="#"><span class="user-status is-idle"></span> <em>Fernando G. Olson</em></a>
		</div>
		<div class="chat-group" id="group-2">
			<strong>Work</strong>
			<a href="#"><span class="user-status is-offline"></span> <em>Robert J. Garcia</em></a>
			<a href="#" data-conversation-history="#sample_history_2"><span class="user-status is-offline"></span> <em>Daniel A. Pena</em></a>
			<a href="#"><span class="user-status is-busy"></span> <em>Rodrigo E. Lozano</em></a>
		</div>
		<div class="chat-group" id="group-3">
			<strong>Social</strong>
			<a href="#"><span class="user-status is-busy"></span> <em>Velma G. Pearson</em></a>
			<a href="#"><span class="user-status is-offline"></span> <em>Margaret R. Dedmon</em></a>
			<a href="#"><span class="user-status is-online"></span> <em>Kathleen M. Canales</em></a>
			<a href="#"><span class="user-status is-offline"></span> <em>Tracy J. Rodriguez</em></a>
		</div>
	</div>
-->
	<!-- conversation template -->
<!--
	<div class="chat-conversation">
		<div class="conversation-header">
			<a href="#" class="conversation-close"><i class="entypo-cancel"></i></a>
			
			<span class="user-status"></span>
			<span class="display-name"></span> 
			<small></small>
		</div>
		<ul class="conversation-body">	
		</ul>
		<div class="chat-textarea">
			<textarea class="form-control autogrow" placeholder="Type your message"></textarea>
		</div>
	</div>
</div>
-->
<!-- Chat Histories -->
<!--
<ul class="chat-history" id="sample_history">
	<li>
		<span class="user">Art Ramadani</span>
		<p>Are you here?</p>
		<span class="time">09:00</span>
	</li>
	<li class="opponent">
		<span class="user">Catherine J. Watkins</span>
		<p>This message is pre-queued.</p>
		<span class="time">09:25</span>
	</li>
	<li class="opponent">
		<span class="user">Catherine J. Watkins</span>
		<p>Whohoo!</p>
		<span class="time">09:26</span>
	</li>
	
	<li class="opponent unread">
		<span class="user">Catherine J. Watkins</span>
		<p>Do you like it?</p>
		<span class="time">09:27</span>
	</li>
</ul>
-->
<!-- Chat Histories -->
<!--
<ul class="chat-history" id="sample_history_2">
	<li class="opponent unread">
		<span class="user">Daniel A. Pena</span>
		<p>I am going out.</p>
		<span class="time">08:21</span>
	</li>
	
	<li class="opponent unread">
		<span class="user">Daniel A. Pena</span>
		<p>Call me when you see this message.</p>
		<span class="time">08:27</span>
	</li>
</ul>	
-->
<script type="text/javascript">
      var base_url = "<?php echo base_url(); ?>";
      var site_url = "<?php echo site_url(); ?>";
      var active_controller = "<?php echo $this->uri->segment(1); ?>";

      function move_page(link){
        window.location.href = link;
      }

     function goBack() {
      window.history.back();
     }
</script>
<!-- Bottom Scripts -->
<script src="<?php echo base_url('assets/neon/assets/js/totop/totop.min.js')?>"></script>
<script src="<?php echo base_url('assets/neon/assets/js/gsap/main-gsap.js')?>"></script>
<script src="<?php echo base_url('assets/neon/assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js')?>"></script>
<script src="<?php echo base_url('assets/neon/assets/js/bootstrap.js')?>"></script>
<script src="<?php echo base_url('assets/neon/assets/js/joinable.js')?>"></script>
<script src="<?php echo base_url('assets/neon/assets/js/resizeable.js')?>"></script>
<script src="<?php echo base_url('assets/neon/assets/js/neon-api.js')?>"></script>
<script src="<?php echo base_url('assets/neon/assets/js/jquery.validate.min.js')?>"></script>
<script src="<?php echo base_url('assets/neon/assets/js/jvectormap/jquery-jvectormap-1.2.2.min.js')?>"></script>
<script src="<?php echo base_url('assets/neon/assets/js/jvectormap/jquery-jvectormap-europe-merc-en.js')?>"></script>
<script src="<?php echo base_url('assets/neon/assets/js/jquery.sparkline.min.js')?>"></script>
<script src="<?php echo base_url('assets/neon/assets/js/rickshaw/vendor/d3.v3.js')?>"></script>
<script src="<?php echo base_url('assets/neon/assets/js/rickshaw/rickshaw.min.js')?>"></script>
<script src="<?php echo base_url('assets/neon/assets/js/raphael-min.js')?>"></script>
<script src="<?php echo base_url('assets/neon/assets/js/morris.min.js')?>"></script>
<script src="<?php echo base_url('assets/neon/assets/js/toastr.js')?>"></script>
<script src="<?php echo base_url('assets/neon/assets/js/jquery.multi-select.js')?>"></script>
<script src="<?php echo base_url('assets/neon/assets/js/icheck/icheck.min.js')?>"></script>
<script src="<?php echo base_url('assets/neon/assets/js/bootstrap-tagsinput.min.js')?>"></script>
<script src="<?php echo base_url('assets/neon/assets/js/typeahead.min.js')?>"></script>
<script src="<?php echo base_url('assets/neon/assets/js/selectboxit/jquery.selectBoxIt.min.js')?>"></script>
<script src="<?php echo base_url('assets/neon/assets/js/bootstrap-datepicker.js')?>"></script>
<script src="<?php echo base_url('assets/neon/assets/js/bootstrap-timepicker.min.js')?>"></script>
<script src="<?php echo base_url('assets/neon/assets/js/bootstrap-colorpicker.min.js')?>"></script>
<script src="<?php echo base_url('assets/neon/assets/js/daterangepicker/moment.min.js')?>"></script>
<script src="<?php echo base_url('assets/neon/assets/js/daterangepicker/daterangepicker.js')?>"></script>
<script src="<?php echo base_url('assets/neon/assets/js/neon-chat.js')?>"></script>
<script src="<?php echo base_url('assets/neon/assets/js/neon-custom.js')?>"></script>
<script src="<?php echo base_url('assets/neon/assets/js/neon-demo.js')?>"></script>
<script src="<?php echo base_url('assets/neon/assets/js/button-loader.js')?>"></script>

<!-- Bottom Scripts -->
<script src="<?php echo base_url('assets/neon/assets/js/jquery.dataTables.min.js')?>"></script>
<script src="<?php echo base_url('assets/neon/assets/js/datatables/TableTools.min.js')?>"></script>
<script src="<?php echo base_url('assets/neon/assets/js/dataTables.bootstrap.js')?>"></script>
<script src="<?php echo base_url('assets/neon/assets/js/datatables/jquery.dataTables.columnFilter.js')?>"></script>
<script src="<?php echo base_url('assets/neon/assets/js/datatables/lodash.min.js')?>"></script>
<script src="<?php echo base_url('assets/neon/assets/js/datatables/responsive/js/datatables.responsive.js')?>"></script>
<script src="<?php echo base_url('assets/neon/assets/js/select2/select2.min.js')?>"></script>
<script src="<?php echo base_url('assets/neon/assets/js/jquery-confirm/js/jquery-confirm.js')?>"></script>
<script src="<?php echo base_url('assets/neon/assets/js/sweetalert/js/sweetalert.min.js')?>"></script>
<script src="<?php echo base_url('assets/neon/assets/js/clipboard/clipboard.min.js')?>"></script>
<script src="<?php echo base_url('assets/neon/assets/js/easy-autocomplete/jquery.easy-autocomplete.js')?>"></script>
<script src="<?php echo base_url('assets/neon/assets/js/easy-autocomplete/jquery.easy-autocomplete.min.js')?>"></script>
<script type="text/javascript">
	var options = {

	  url: function(phrase) {
	    return "<?php echo site_url(); ?>assets/menuSearch.php";
	  },

	  getValue: function(element) {
	    return element.name;
	  },
	  ajaxSettings: {
	    dataType: "json",
	    method: "POST",
	    data: {
	      dataType: "json"
	    }
	  },
	  template: {
	        type: "links",
	        fields: {
	            link: "website-link"
	        }
	  },
	  preparePostData: function(data) {
	    data.phrase = $("#search-input").val();
	    return data;
	  },

	  requestDelay: 400
	};
	$("#search-input").easyAutocomplete(options);
</script>
</body>
</html>