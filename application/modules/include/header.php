<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="Neon Admin Panel" />
	<meta name="author" content="" />
	
	<title>Operational 520 | <?php echo menu_name($this->uri->segment(1)); ?></title>

  <link rel="icon" href="<?php echo base_url('assets/images/520.png');?>">

  <link rel="stylesheet" href="<?php echo base_url('assets/neon/assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css')?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/neon/assets/css/font-icons/entypo/css/entypo.css')?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/neon/assets/css/font-icons/font-awesome/css/font-awesome.min.css')?>">

  <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
  <link rel="stylesheet" href="<?php echo base_url('assets/neon/assets/css/bootstrap.css')?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/neon/assets/css/neon-core.css')?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/neon/assets/css/neon-theme.css')?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/neon/assets/css/neon-forms.css')?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/neon/assets/css/custom.css')?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/neon/assets/css/scrollbar.css')?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/neon/assets/js/totop/totop.css')?>">
  

  <link rel="stylesheet" href="<?php echo base_url('assets/neon/assets/js/jvectormap/jquery-jvectormap-1.2.2.css')?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/neon/assets/js/rickshaw/rickshaw.min.css')?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/neon/assets/js/datatables/responsive/css/datatables.responsive.css')?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/neon/assets/js/select2/select2-bootstrap.css')?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/neon/assets/js/select2/select2.css')?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/neon/assets/js/selectboxit/jquery.selectBoxIt.css')?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/neon/assets/js/daterangepicker/daterangepicker-bs3.css')?>">
  
  <link rel="stylesheet" href="<?php echo base_url('assets/neon/assets/js/icheck/skins/minimal/_all.css')?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/neon/assets/js/icheck/skins/square/_all.css')?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/neon/assets/js/icheck/skins/flat/_all.css')?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/neon/assets/js/icheck/skins/futurico/futurico.css')?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/neon/assets/js/icheck/skins/polaris/polaris.css')?>">

  <link rel="stylesheet" href="<?php echo base_url('assets/neon/assets/js/jquery-confirm/css/jquery-confirm.css')?>">
  <!-- <link rel="stylesheet" href="<?php echo base_url('assets/neon/assets/js/jquery-confirm/css/jquery-confirm.less')?>"> -->
  <link rel="stylesheet" href="<?php echo base_url('assets/neon/assets/js/sweetalert/css/sweetalert.css')?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/neon/assets/js/easy-autocomplete/easy-autocomplete.css')?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/neon/assets/js/easy-autocomplete/easy-autocomplete.min.css')?>">

  <!-- jQuery min -->
  <script src="<?php echo base_url('assets/neon/assets/js/jquery-1.11.0.min.js')?>"></script>
	
	
</head>
<body class="page-body page-fade-only" ><!-- page-fade-only -->
<?php
	$collapsed_sidebar = collapsed_sidebar();
	$collapsed = '';
	if($collapsed_sidebar == 'Y')
	{
		$collapsed = 'sidebar-collapsed';
	}
?>
<div class="page-container <?php echo $collapsed; ?>"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->	
	<div class="sidebar-menu">
		<header class="logo-env">
			<!-- logo -->
			<div class="logo">
				<a href="index.html">
					<!-- <img src="<?php echo base_url('assets/images/logo-jnt.png'); ?>" width="100" alt="" /> -->
					<p style="font-size: 14px;color: white;"><b>Operational 520</b></p>
				</a>
			</div>
			<!-- logo collapse icon -->
			<div class="sidebar-collapse">
				<a href="#" class="sidebar-collapse-icon with-animation"><!-- add class "with-animation" if you want sidebar to have animation during expanding/collapsing transition -->
					<i class="entypo-menu"></i>
				</a>
			</div>
			<!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
			<div class="sidebar-mobile-menu visible-xs">
				<a href="#" class="with-animation"><!-- add class "with-animation" to support animation -->
					<i class="entypo-menu"></i>
				</a>
			</div>
		</header>
		<!-- main menu -->
		<?php echo side_menu(); ?>
	</div>	
	<div class="main-content">
	<div class="row">
		<!-- Profile Info and Notifications -->
		<div class="col-md-6 col-sm-8 clearfix">
			<ul class="user-info pull-left pull-none-xsm">
				<!-- Profile Info -->
				<li class="profile-info dropdown"><!-- add class "pull-right" if you want to place this from right -->
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<img src="<?php echo base_url('assets/neon/assets/images/thumb-1@2x.png'); ?>" alt="" class="img-circle" width="44" />
						<?php echo $this->session->userdata('ses_fullname'); ?>
					</a>
					<ul class="dropdown-menu">
						<!-- Reverse Caret -->
						<li class="caret"></li>
						<!-- Profile sub-links -->
						<li>
							<a href="extra-timeline.html">
								<i class="entypo-user"></i>
								Edit Profile
							</a>
						</li>
						<li>
							<a href="mailbox.html">
								<i class="entypo-mail"></i>
								Inbox
							</a>
						</li>
						<li>
							<a href="extra-calendar.html">
								<i class="entypo-calendar"></i>
								Calendar
							</a>
						</li>
						<li>
							<a href="#">
								<i class="entypo-clipboard"></i>
								Tasks
							</a>
						</li>
					</ul>
				</li>
			</ul>
			<ul class="user-info pull-left pull-right-xs pull-none-xsm">
				
			<!-- Message Notifications -->
			<li class="notifications dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
						<i class="entypo-mail"></i>
						<!-- <span class="badge badge-secondary">0</span> -->
					</a>
					
				</li>
			</ul>
		</div>
		<!-- Raw Links -->
		<div class="col-md-6 col-sm-4 clearfix hidden-xs">
			<ul class="list-inline links-list pull-right">
				<li class="sep"></li>
				<li>
					<a href="<?php echo site_url('Login/logout'); ?>">
						Log Out <i class="entypo-logout right"></i>
					</a>
				</li>
			</ul>
		</div>
	</div>
	<hr />