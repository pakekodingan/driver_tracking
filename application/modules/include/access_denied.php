<?php $this->load->view('header'); ?>
<div class="container-fluid">
  <?php echo breadcrumb($this->uri->segment(1)); ?>
  <div class="row">
    <div class="col-md-12">
      <h2><?php echo menu_name($this->uri->segment(1)); ?></h2>
      <br>
    </div>  
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="alert alert-danger"><i class="fa fa-warning"></i> &nbsp;&nbsp;<strong>Opps Sorry!</strong> You don't have access this menu.</div>
    </div>  
  </div>
</div>
<?php $this->load->view('footer'); ?>