<?php 
defined('BASEPATH') OR exit('No direct script access allowed'); 
 
class Create_module extends CI_Controller { 
 
	public function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('isLogin')){
			redirect('Login');
		}
		$this->load->model('M_create_module','create_module_model');
	}
 
	public function index()
	{
		$this->load->view('v_create_module');
	}

	public function new(){
		
		$environment = application('environment');

		if($environment == 'production')
		{
			echo "Opps, Sorry can't create module in environment production.";
			die();
		}

		$dir = $this->uri->segment(3);

		$dir = ucwords(strtolower(str_replace(" ","_",$dir)));
		$path_module 		= './application/modules/'.$dir.'/';
		$path_controller	= './application/modules/'.$dir.'/controllers/';
		$path_model 		= './application/modules/'.$dir.'/models/';
		$path_view 		 	= './application/modules/'.$dir.'/views/';

		if(!is_dir('application/modules/'.$dir))
		{
			mkdir($path_module,0777, TRUE);
		}else{
			die('module already exits !');
		}
		
		if(!is_dir($path_controller))
		{
			mkdir($path_controller);
			$script_controller = "<?php \n";
			$script_controller .= "defined('BASEPATH') OR exit('No direct script access allowed'); \n \n";
			$script_controller .= "class ".$dir." extends CI_Controller { \n \n";
			$script_controller .= "\tpublic function __construct()\n";
			$script_controller .= "\t{\n";
			$script_controller .= "\t\tparent::__construct();\n";
			$script_controller .= "\t\tif(!\$this->session->userdata('isLogin')){\n";
			$script_controller .= "\t\t\tredirect('Login');\n";
			$script_controller .= "\t\t}\n";
			$script_controller .= "\t\t\$this->load->model('M_".strtolower($dir)."','".strtolower($dir)."_model');\n";
			$script_controller .= "\t}\n \n";
			$script_controller .= "\tpublic function index()\n";
			$script_controller .= "\t{\n";
			$script_controller .= "\t\t\$this->load->view('v_".strtolower($dir)."');\n";
			$script_controller .= "\t}\n \n";
			$script_controller .= "}\n \n";
			$script_controller .= "/* End of file ".$dir.".php */\n";
			$script_controller .= "/* Location: ./application/modules/".$dir."/controllers/".$dir.".php */\n";
			write_file($path_controller.$dir.'.php',$script_controller);
		}
		
		if(!is_dir($path_model))
		{
			mkdir($path_model,0777, TRUE);
			$model_name = "M_".strtolower($dir);
			$script_model = "<?php \n";
			$script_model .= "defined('BASEPATH') OR exit('No direct script access allowed'); \n \n";
			$script_model .= "class ".$model_name." extends CI_Model { \n \n";
			$script_model .= "\tpublic function rename_here()\n";
			$script_model .= "\t{\n \n";
			$script_model .= "\t}\n \n";
			$script_model .= "}\n \n";
			$script_model .= "/* End of file ".$model_name.".php */\n";
			$script_model .= "/* Location: ./application/modules/".$dir."/models/".$model_name.".php */\n";
			write_file($path_model.$model_name.'.php',$script_model);		
		}
		
		if(!is_dir($path_view))
		{
			mkdir($path_view,0777, TRUE);
			$view_name = "v_".strtolower($dir);
			$script_view = "<?php \$this->load->view('../../include/header'); ?>\n";
			$script_view .='<div class="container-fluid">
							  <?php echo breadcrumb($this->uri->segment(1)); ?>
							  <div class="row">
							    <div class="col-md-12">
							      <h1><?php echo menu_name($this->uri->segment(1)); ?></h1>
							    </div>  
							  </div>
							  <br>
							  <div class="row">
						        <div class="col-md-12">
						            <div class="panel panel-info">
						                <div class="panel-body">
						                Content Here ....
						                </div>
					                </div>
				                </div>
				               </div>
							</div>';
			$script_view .= "\n";
			$script_view .= "<?php \$this->load->view('../../include/footer'); ?>\n";
			$script_view .= "<script src='<?php echo base_url('application/modules/".$dir."/views/".strtolower($dir).".js')?>'></script>";
			$script_view .= "\n \n";
			$script_view .= "<!-- End of file ".$view_name.".php -->\n";
			$script_view .= "<!-- Location: ./application/modules/".$dir."/views/".$view_name.".php -->\n";
 
			write_file($path_view.$view_name.'.php',$script_view);

			$script_view_js = "";
			write_file($path_view.strtolower($dir).'.js',$script_view_js);
		}
		history('create module ?path='.$path_module);
		echo "succses created module.";
	}
 
}
 
/* End of file Create_module.php */
/* Location: ./application/modules/Create_module/controllers/Create_module.php */
