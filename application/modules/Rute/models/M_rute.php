<?php 
defined('BASEPATH') OR exit('No direct script access allowed'); 
 
class M_rute extends CI_Model { 
 
	public function check_rute($kode_rute)
    {
        $check = $this->db->get_where('rute',array('rute_id'=>$kode_rute))->num_rows();
        if($check > 0)
        {
            $result = 1;
        }
        else
        {
            $result = 0;
        }
        return $result;
    }

    public function insert_data($data)
    {
        $this->db->insert('rute',$data);
    }

    public function update_data($data,$where)
    {
        $this->db->update('rute',$data,$where);
    }
    
    public function rute_delete($id)
    {
        $delete = $this->db->delete('rute',array('rute_id'=>$id));
        $result = ($delete == true)?1:0;
        return $result;
    }
 
}
 
/* End of file M_rute.php */
/* Location: ./application/modules/Rute/models/M_rute.php */
